# arpa2common
#
# This Makefile is just a stub: it invokes CMake, which in turn
# generates Makefiles, and then uses those to make the project.
#
# Useful Make parameters at this level are:
#	PREFIX=/usr/local
#
# For anything else, do this:
#
#	make configure                 # Basic configure, remember PREFIX!
#	( cd cbuild ; ccmake )         # CMake GUI for build configuration
#	( cd cbuild ; make install )   # Build and install
#

#
# Redistribution and use is allowed without limitation.
#    SPDX-License-Identifier: CC0-1.0
#    SPDX-FileCopyrightText: no
#


PREFIX ?= /usr/local

all: compile

build-dir:
	@mkdir -p cbuild

configure: _configure build-dir cbuild/CMakeCache.txt

_configure:
	@rm -f cbuild/CMakeCache.txt

cbuild/CMakeCache.txt:
	( cd cbuild && cmake .. -DCMAKE_INSTALL_PREFIX=$(PREFIX) -DCMAKE_PREFIX_PATH=$(PREFIX) )

compile: build-dir cbuild/CMakeCache.txt
	( cd cbuild && $(MAKE) )

install: build-dir
	( cd cbuild && $(MAKE) install )

test: build-dir
	( cd cbuild && $(MAKE) test )

uninstall: build-dir
	( cd cbuild && $(MAKE) uninstall )

clean:
	rm -rf cbuild/

package: compile
	( cd cbuild && $(MAKE) package )

