+ Should match sel, but NOT AS ID, for bakker+@orvelte.nep
	+ confusion with signatures
	+ aliases set before backtracking
+ Remove sigflags, expireday from the _sign and _verify API
+ Instead, document its move into the a2id_t structure
+ Write an INSTALL.MD
+ Introduce error tables a2id and a2xs, for use with com_err
+ Report error codes in errno for a2id
+ Report error codes in errno for a2xs
+ Trouble iterating on local-part
+ Trouble iterating on patterns that start as "@." (like that one)
+ Seeing _END (and, presumably, _BUFSIZE) drop out when iterating domains
+ Trouble removing signature (because offsets get out of order)
+ Consider a light-weight iterator that delivers input for "%.*s@%s"
+ Distinctly parse remote identifiers / selectors
+ Parse ACL text: store variables, collect access_rights, make callbacks
+ Iterate over Remote Selector and parse what is found
+ Derive database keys in steps that supports gradual release of key control
+ Interact with LMDB backend to retrieve ACL text
+ a2id_sign() always includes the sigexp value; it should depend on sigflg
- Check if LMDB integer keys are not "too portable" and get reversed
- Check if LMDB integer keys are really more compact (no size per key)
+ Check if the text shifter in identity.rl clears memory at the end
- Consider a NULL element for a Selector (no need for NULL pointers, lattice order)
+ Introduce UTF-8 character codes as an added constraint
+ Introduce NAI constraints on double dots and/or initial/trailing dots
+ Where do identity signatures get their key from?
2.0.0: - Add and subtract the key's dayzero during a2id_sign() and a2id_verify()
2.0.0: - Also check the expireday against the current day
+ Make a utility to generate keys in /var/lib/arpa2/identity/xxx.key
+ Introduce an algorithm code in the keys
- Insert a "trunk" or "Access Group" in the database; struct ruledb default is 0
- Key derivation: function names; keylen; separate from "production flow" functions
- Document pattern and a few Access Types: attrs, rights, triggers
- Comms: a=alias_requirement, o=alias_override, s=signature_flags_minimum/numeric
+ Align identifiers; we have a2xs_ acldb_ a2acl_ a2aclc_ a2aclr_
+ Count abstraction steps (using Quick Iteration) and return false for unreachable.
2.0.0: - Check that a2id_verify() approves of unsigned addresses (and test it, too)
+ Document libarpa2identity.so
- Document libarpa2access.so
+ Rename library from liba2acl.so to liba2xs.so (became libarpa2access.so)

2.0.0: - How do we parse "+@." selectors?  -->  doing poorly!
        DEBUG: OK - +@.
        DEBUG:    = +@.
        DEBUG:    : ||||||||+||||||@|.||
  unlike the intention as a service, in line with
        DEBUG: OK - +imap+store@.
        DEBUG:    = +imap+store@.
        DEBUG:    : |+|imap|+|store||||||||||@|.||


- Have a generic function _like_ the one for Communication, including
  iteration over the Remote Selector and using an xskey instead of a
  local identity.  Keep the rights in/out and perhaps allow the same
  "nesting" protocol as used for rules, so the dbhandle may be NULL.
  Make a2xs_communication() a wrapper around this generic function.

	bool a2xs_communication (void *dbhandle,
                        a2id_t *local, a2sel_t *remote,
                        uint32_t required_rights, uint32_t desired_rights,
                        uint32_t *actual_rights);

  Perhaps like this?  Note that key preparation has now been done and
  only the xskey is still needed.

	bool a2xs_getrights (void *nested_dbhandle,
			rules_dbkey xstoken, char *xskey,
			a2sel_t *remote,
                        uint32_t required_rights, uint32_t desired_rights,
                        uint32_t *actual_rights);

  Looking back at communication, a wrapper for prepkey may be used
  to incorporate its specific UUID in going from prepkey1 to prepkey2.
  And it would derive xskey from the userid / servicename in local.
  (Or, there may be two levels of communication calls for two prepkeys.)

- Support a simple algebra for Access Rights.  For instance, Apache calls
  for a notation like

	AccessRights RW+CD/RWCDKP

   To say "need RW, wish for CD, change nothing but RWCDKP rights".
   These strings might be parsed by the ACL library, and used to modify
   two `(uint32_t *)` holding the required and desired Access Rights.

