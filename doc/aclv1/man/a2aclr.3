.\" Copyright (c) 2019 Tim Kuijsten
.\"
.\" Permission to use, copy, modify, and/or distribute this software for any
.\" purpose with or without fee is hereby granted, provided that the above
.\" copyright notice and this permission notice appear in all copies.
.\"
.\" THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
.\" WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
.\" MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
.\" ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
.\" WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
.\" ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
.\" OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
.\"
.Dd $Mdocdate: December 17 2019 $
.Dt A2ACLR 3
.Os
.Sh NAME
.Nm a2aclr_count ,
.Nm a2aclr_dbclose ,
.Nm a2aclr_dbopen ,
.Nm a2aclr_delrights ,
.Nm a2aclr_importpolicyfile ,
.Nm a2aclr_hasright ,
.Nm a2aclr_hasrights ,
.Nm a2aclr_printdb ,
.Nm a2aclr_putrights ,
.Nm a2aclr_whichrights
.Nd library to work with ARPA2 Resource Access Control Lists
.Sh SYNOPSIS
.In arpa2/a2aclr.h
.Ft int
.Fo a2aclr_count
.Fa "void *ctx"
.Fa "size_t *count"
.Fc
.Ft int
.Fo a2aclr_dbclose
.Fa "void *ctx"
.Fc
.Ft void *
.Fo a2aclr_dbopen
.Fa "const char *path"
.Fc
.Ft int
.Fo a2aclr_delrights
.Fa "void *ctx"
.Fa "const char *realm"
.Fa "size_t realmsize"
.Fa "const char *selector"
.Fa "size_t selectorsize"
.Fa "const char *resource"
.Fa "size_t resourcesize"
.Fa "const char *instance"
.Fa "size_t instancesize"
.Fa "const char *rights"
.Fa "size_t rightssize"
.Fc
.Ft ssize_t
.Fo a2aclr_importpolicyfile
.Fa "void *ctx"
.Fa "const char *policyfile"
.Fa "char *errstr"
.Fa "size_t errstrsize"
.Fc
.Ft int
.Fo a2aclr_hasright
.Fa "void *ctx"
.Fa "const char reqright"
.Fa "const char *realm"
.Fa "size_t realmsize"
.Fa "const char *id"
.Fa "size_t idsize"
.Fa "const char *resource"
.Fa "size_t resourcesize"
.Fa "const char *instance"
.Fa "size_t instancesize"
.Fc
.Ft int
.Fo a2aclr_hasrights
.Fa "void *ctx"
.Fa "const char *reqrights"
.Fa "size_t reqrightssize"
.Fa "const char *realm"
.Fa "size_t realmsize"
.Fa "const char *id"
.Fa "size_t idsize"
.Fa "const char *resource"
.Fa "size_t resourcesize"
.Fa "const char *instance"
.Fa "size_t instancesize"
.Fc
.Ft int
.Fo a2aclr_printdb
.Fa "void *ctx"
.Fa "int descriptor"
.Fc
.Ft int
.Fo a2aclr_putrights
.Fa "void *ctx"
.Fa "const char *realm"
.Fa "size_t realmsize"
.Fa "const char *selector"
.Fa "size_t selectorsize"
.Fa "const char *resource"
.Fa "size_t resourcesize"
.Fa "const char *instance"
.Fa "size_t instancesize"
.Fa "const char *rights"
.Fa "size_t rightssize"
.Fc
.Ft int
.Fo a2aclr_whichrights
.Fa "void *ctx"
.Fa "char *rights"
.Fa "size_t *rightssize"
.Fa "const char *realm"
.Fa "size_t realmsize"
.Fa "const char *id"
.Fa "size_t idsize"
.Fa "const char *resource"
.Fa "size_t resourcesize"
.Fa "const char *instance"
.Fa "size_t instancesize"
.Fc
.Sh DESCRIPTION
The
.Fn a2aclr_count
function sets
.Fa count
to the number of policy rules in the database
.Fa ctx .
.Pp
The
.Fn a2aclr_dbclose
function closes and free's the database context
.Fa ctx .
.Pp
The
.Fn a2aclr_dbopen
function opens a database.
.Fa path
is where the database is stored.
The type of database backend is decided at compile time.
There are currently two supported implementations:
.Dq dbmem
which is a simple
memory based key-value store and
.Dq dblmdb
which uses the LMDB database backend.
Only the LMDB database backend requires a
.Fa path
argument.
The caller is responsible for freeing
.Ar ctx
with
.Fn a2aclr_dbclose .
.Pp
The
.Fn a2aclr_delrights
function removes
.Fa rights
under the
.Fa realm ,
.Fa selector ,
.Fa resource ,
and
.Fa instance
from database
.Fa ctx .
.Ar rights
must be a string of one or more characters from
.Sx DEFINED RIGHTS
and is case-insensitive.
.Pp
The
.Fn a2aclr_importpolicyfile
function imports a resource policy from a text file specified by
.Fa policyfile
into the database
.Fa ctx .
See
.Sx RESOURCE POLICY IMPORT FILE FORMAT
for a description of the syntax of a resource policy file.
.Pp
The
.Fn a2aclr_hasright
function determines if
.Fa id
within
.Fa realm
has the
.Fa reqright
on
.Fa resource
and
.Fa instance .
.Fa ctx
must be the context that was returned while opening the database.
.Ar reqright
must be a character from
.Sx DEFINED RIGHTS .
.Pp
The
.Fn a2aclr_hasrights
function determines if
.Fa id
within
.Fa realm
has all
.Fa reqrights
on
.Fa resource
and
.Fa instance .
.Fa ctx
must be the context that was returned while opening the database.
.Ar reqrights
must be a string of one or more characters from
.Sx DEFINED RIGHTS .
.Pp
The
.Fn a2aclr_printdb
function prints the contents of the database
.Fa ctx .
.Pp
The
.Fn a2aclr_putrights
function stores
.Fa rights
into the database
.Fa ctx .
If an entry for the combination of
.Fa realm ,
.Fa selector ,
.Fa resource ,
.Fa instance
already exists it is ensured that every new right in
.Fa rights
is added to the existing set of rights.
.Ar rights
must be a string of one or more characters from
.Sx DEFINED RIGHTS
and is case-insensitive.
.Pp
The
.Fn a2aclr_whichrights
function determines which rights
.Fa id
within
.Fa realm
has on
.Fa resource
and optionally
.Fa instance .
The result is a string of characters written to
.Fa rights .
See
.Sx DEFINED RIGHTS
for a complete list of the rights that can be returned.
The returned rights are unordered, uppercase and never contain duplicates.
.Pp
If no rights are found then
.Fa rightssize
is set to 0 and
.Fa rights
is left untouched.
If
.Fa rights
is >=
.Dv MAXRIGHTSSIZE
it is guaranteed that the rights of any matched policy
always fit.
.Pp
.Fa id
will be generalized until an ACL rule is found or until it equals the most
general selector
.Dq @.
which can not be further generalized.
.Fa ctx
must be the context that was returned while opening the database.
.Ss DEFINED RIGHTS
The following rights are defined, each consisting of one character:
.Bl -tag -width 6n -offset indent
.It A
administration
.It S
services
.It D
resource deleters
.It C
resource creators
.It W
writing to an existing resource
.It R
reading of a resource
.It P
proof about information without reveiling the information
.It K
know about existence
.It O
owner of a resource
.It V
visitors, who may only receive limited facilitation
.El
.Ss RESOURCE POLICY IMPORT FILE FORMAT
Each line in a resource policy file must consist of the following columns
.D1 <realm selector rights resource instance>
the
.Dq instance
column is optional.
Each column should be separated from the other by exactly one horizontal tab.
Empty lines or lines starting with a hash
.Sq #
are ignored.
The complete per-line grammar is as follows:
.Bd -literal
policyline = realm SEP selector SEP rights SEP resource [ SEP [ instance ] ]
SEP        = horizontal tab (0x09)
realm      = 1*graph
selector   = 2*graph
rights     = 1*(A / S / D / C / W / R / P / K / O / V)
resource   = uuid
instance   = 1*print
uuid       = 36graph
graph      = %x21-7e
print      = %x20-7e
.Ed
.Sh RETURN VALUES
The
.Fn a2aclr_count
function returns the value 0 if successful; otherwise the value -1 is returned.
.Pp
The
.Fn a2aclr_dbclose
function returns the value 0 if successful; otherwise the value -1 is returned.
.Pp
The
.Fn a2aclr_dbopen
function returns a newly created database context if successful; otherwise the
value
.Dv NULL
is returned.
.Pp
The
.Fn a2aclr_delrights
function returns the value 0 if successful; otherwise the value -1 is returned.
.Pp
The
.Fn a2aclr_importpolicyfile
function returns the number of processed rules if successful; otherwise the
value -1 is returned and the global variable errno is set to indicate the error.
Furthermore, if
.Fa errstr
is not
.Dv NULL ,
then
.Fa errstr
is updated with a descriptive error of at most
.Fa errstrsize
bytes, including a terminating nul.
.Pp
The
.Fn a2aclr_hasright
function returns a value greater than 0 if
.Fa reqright
is permitted; otherwise the value 0 is returned.
.Pp
The
.Fn a2aclr_hasrights
function returns a value greater than 0 if all rights in
.Fa reqrights
are permitted; otherwise the value 0 is returned.
.Pp
The
.Fn a2aclr_printdb
function returns the value 0 if successful; otherwise the value -1 is returned.
.Pp
The
.Fn a2aclr_putrights
function returns the value 0 if successful; otherwise the value -1 is returned.
.Pp
The
.Fn a2aclr_whichrights
function returns the value 0 if successful and updates
.Fa rights
with the applicable characters.
.Fa rightssize
is updated to reflect the number of characters written into
.Fa rights .
This will be 0 in case no policy is matched.
If an error occurred or a policy is matched but the rights do not fit in
.Fa rights ,
then the value -1 is returned.
If
.Fa rights
is >=
.Dv MAXRIGHTSSIZE
then it is guaranteed that the rights of any matched policy
always fit.
Note that
.Fa rights
is never terminated with a nul byte.
.Sh EXAMPLES
Determine the rights of joe@example.com on the resource instance
7a35d76d-a754-35a6-abe7-757c161f0263 /var/www/private.
.Pp
The file policy.txt used in this example contains the following two lines:
.Bd -literal -offset indent
example.com	joe@example.com	CRWK	7a35d76d-a754-35a6-abe7-757c161f0263	/var/www/private
example.org	@example.org	V	7a35d76d-a754-35a6-abe7-757c161f0263	/var/www/htdocs/index.html
.Ed
.Pp
The first line grants
.Dq CRWK
on the resource 7a35d76d-a754-35a6-abe7-757c161f0263 instance /var/www/private
to the user joe@example.com in the realm example.com.
The second line grants only
.Dq V
to all users from example.org within the realm example.org on resource
7a35d76d-a754-35a6-abe7-757c161f0263 instance /var/www/htdocs/index.html.
.Pp
The following code imports policy.txt into policy.db (when LMDB is used). Then
it parses the A2ID joe@example.com and looks-up permissions for this user on
7a35d76d-a754-35a6-abe7-757c161f0263 /var/www/private.
.Bd -literal -offset indent

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <arpa2/a2aclr.h>

const char *realm, *id, *resource, *instance;
char rights[MAXRIGHTSSIZE];
char errstr[100];
void *ctx;
size_t rightssize;
ssize_t imported;

if ((ctx = a2aclr_dbopen("policy.db")) == NULL)
	exit(1);

imported = a2aclr_importpolicyfile(ctx, "policy.txt", errstr,
    sizeof(errstr));

if (imported == -1) {
	fprintf(stderr, "import error: %s\\n", errstr);
	exit(1);
} else {
	fprintf(stdout, "imported rules: %zd\\n", imported);
}

realm    = "example.com";
id       = "joe@example.com";
resource = "7a35d76d-a754-35a6-abe7-757c161f0263";
instance = "/var/www/private";

rightssize = sizeof(rights);
if (a2aclr_whichrights(ctx, rights, &rightssize, realm, strlen(realm),
    id, strlen(id), resource, strlen(resource), instance,
    strlen(instance)) == -1) {
	fprintf(stderr, "lookup error\\n");
	exit(1);
}

printf("%.*s\\n", (int)rightssize, rights);

if (a2aclr_dbclose(ctx) == -1)
	exit(1);
.Ed
.Pp
The output of this code is:
.Bd -literal -offset indent
imported rules: 2
CRWK
.Ed
.Sh SEE ALSO
.Xr a2id_fromstr 3
.Sh AUTHORS
.An -nosplit
.An Tim Kuijsten
.Sh CAVEATS
The used database backend for
.Fn a2aclr_dbopen
is currently decided at compile time.
Whenever a system-wide lmdb library can be found the LMDB backend is used,
otherwise the memory based backend is used.
It would be better to let the caller pick a backend at run time.
