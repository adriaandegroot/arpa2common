/** @defgroup arpa2identity ARPA2 Identity and Selector functions
 * @{
 * Our blog discusses much background in the
 * [Identity series](http://internetwide.org/tag/identity.html).
 *
 * These functions provide precodition tests for the
 * format of ARPA2 Identity and ARPA2 Selector strings.
 * Comparisons may additionally be applied, for instance
 * to check rights in comparison to application-specific
 * access control configurations.
 *
 * The design of this API is light-weight, raher than an
 * opaque structure with function calls to operate on it.
 * You are expected to unerstand the data structures, but
 * are saved from having to understand a pile of functions.
 * Some functions are general in nature, but have a simpler
 * counterparts defined as macro or inline function.
 *
 * There is good support for stack allocation to reduce
 * risk of mistakes.  Also, functions return a boolean
 * when they succeed, to enable a programming style like
 *
 *	bool ok = true;
 *	ok = ok && a2id_something (...);
 *	bool got_resource = ok;
 *	ok = ok && a2id_someother (...);
 *	if (got_resource) {
 *		a2id_cleanup (...);
 *	}
 *	return ok;
 *
 * SPDX-FileCopyrightText: Copyright 2020 Rick van Rein <rick@openfortress.nl>
 * SPDX-License-Identifier: BSD-2-Clause
 */


#ifndef ARPA2_IDENTITY_H
#define ARPA2_IDENTITY_H


#include <stdbool.h>
#include <stdint.h>

#include <time.h>



/** @brief Initialise the ARPA2 Identity system.
 *
 * This has a mirror image for cleaup in a2id_fini().
 */
void a2id_init (void);


/** @brief Finalise the ARPA2 Identity system.
 *
 * This may do cleanup operations.
 */
void a2id_fini (void);


/* Maximum identity length is:
 *  - local-part in 64 bytes
 *  - one byte AT symbol
 *  - domain in 255 Punycode chars
 *  - one Punycode char yields up to 4 bytes
 * Buffers add one for a trailing NUL.
 */
#define A2ID_MAXLEN   ( 64 + 1 + 4*255     )
#define A2ID_BUFSIZE  ( 64 + 1 + 4*255 + 1 )
//
#define A2SEL_MAXLEN  ( 64 + 1 + 4*255     )
#define A2SEL_BUFSIZE ( 64 + 1 + 4*255 + 1 )


/** @brief Offsets into parsed identity strings.
 *
 *After parsing, a number of offsets into the string buffer are derived.
 * They are stored in an array, in their order of appearance.  The first
 * is 0 and the last is the string length.
 *
 * Identity manipulation involves looking at ranges between offsets, be
 * they subsequent or not, but at least used in order of appearance.
 * Absense of a part of the ARPA2 Identity or ARPA2 Selector is marked
 * as an empty string between two subsequent offsets, rathen than as a
 * NULL pointer, so a range spans 0 characters if its field is absent.
 *
 * Several of the markers are included, such as _PLUS_SIG before _SIG.
 * When these successive values are equal, then there would not be a
 * separator and, as a result, no signature.  In this particular case,
 * the additional use of _SIGPLUS is the plus sign after the _SIG, the
 * actual marker being parsed to detect a signature, but it is also
 * shown by marking the preceding _PLUS_SIG as such.  Note that the
 * value doubles as the end marker for aliases, or of the userid.
 *
 * Values such as _AT_DOMAIN include the marker to enable comparison
 * of a full domain name, rather then comparison of a trailing bit,
 * such as for a Selector @.domain which would use _DOMAIN instead.
 *
 * There are two variants for the local-part.  An initial plus is for
 * service names, and the "aliases" are really parameters.  There
 * can be a signature on those addresses too, signified by a trailing
 * plus in the local-part.  The two sequences parse into different
 * offsets; A2ID_USERID, _PLUS_ALIASES and _ALIASES all point at the
 * optional signature and @domain for services.  For services, the
 * A2ID_OFS_PLUS_SERVICE, _SERVICE, _PLUS_SVCARGS, _SVCARGS all point
 * at the begining, so at 0, as does A2ID_OFS_USERID in that case.
 *
 * The FLGEXPSIG follows a grammar of its own.  Encoded in BASE32,
 * the FLG and EXP parts signal continuation in their highest bits,
 * so they are self-terminating.  The SIG part than runs to consume
 * the remaining characters at 5 bits each.
 *
 * All this is mostly of interest for domains that are known to adhere
 * to the ARPA2 Identity forms.  Others fit in the framework with the
 * local part, possibly not even cast to lowercase, entirely between
 * the A2ID_OFS_USERID and A2ID_OFS_PLUS_ALIASES.  Anything before it
 * equalys A2ID_OFS_PLUS_SERVICE (normally 0) and anthing after it,
 * up to and including the A2ID_OFS_AT_DOMAIN, are equal and point to
 * the '@' separator.  It may be a matter of taste whether to err on
 * the unsafe side, in the interest of consistency and usability and
 * with only mild impact on the real-life security level.
 */
typedef enum {
		/* Option #0 is a local-part "+svccmd+svcargs[+FLGEXPSIG+]"        */
	A2ID_OFS_PLUS_SERVICE,		/* "[+svccmd+svcargs][+FLGEXPSIG+]@domain" */
	A2ID_OFS_SERVICE,		/*  "[svccmd+svcargs][+FLGEXPSIG+]@domain" */
	A2ID_OFS_PLUS_SVCARGS,		/*        "[+svcargs][+FLGEXPSIG+]@domain" */
	A2ID_OFS_SVCARGS,		/*         "[svcargs][+FLGEXPSIG+]@domain" */
		/* Option #1 is a local-part  "userid+aliases[+FLGEXPSIG+]"        */
	A2ID_OFS_USERID,		/*  "[userid+aliases][+FLGEXPSIG+]@domain" */
	A2ID_OFS_PLUS_ALIASES,		/*        "[+aliases][+FLGEXPSIG+]@domain" */
	A2ID_OFS_ALIASES,		/*         "[aliases][+FLGEXPSIG+]@domain" */
		/* Selectors may have an open ended '+'                            */
	A2ID_OFS_OPEN_END,		/*                 "+[+FLGEXPSIG+]"        */
		/* Both the local-part grammars fallback to "[+FLGEXPSIG+]"        */
	A2ID_OFS_PLUS_SIG,		/*                  "[+FLGEXPSIG+]@domain" */
	A2ID_OFS_SIGFLAGS,		/*                   "[FLGEXPSIG+]@domain" */
	A2ID_OFS_SIGEXPIRE,		/*                      "[EXPSIG+]@domain" */
	A2ID_OFS_SIGVALUE,		/*                         "[SIG+]@domain" */
	A2ID_OFS_SIGPLUS,		/*                            "[+]@domain" */
	A2ID_OFS_AT_DOMAIN,		/*                               "@domain" */
		/* Selectors can place a dot before the domain                     */
	A2ID_OFS_DOT_DOMAIN,		/*                             "[.]domain" */
	A2ID_OFS_DOMAIN,		/*                                "domain" */
	A2ID_OFS_END,               	/*                                      "" */
	A2ID_OFS_BUFSIZE,		/* Buffersize used, with a terminating NUL */
	//
	A2ID_OFS_COUNT
} a2id_offset_t;


/** @brief ARPA2 Selector.
 *
 * This type represents a parsed and normalised ARPA2 Selector.
 * It has been validated and normalised while parsing the structure
 * into offsets.  It is stored with a NUL terminator, so the txt
 * field can actually be printed as a normal C-string, also after
 * editing operations are performed on it.
 */
typedef struct {
	uint32_t sigflags, expireday;
	uint16_t ofs [A2ID_OFS_COUNT];
	char txt [A2SEL_BUFSIZE];
	time_t expiration;
} a2sel_t;


/** @brief ARPA2 Identity.
 *
 *This type represents a parsed and normalised ARPA2 Identity.
 * Note that this is a special/constrained form of ARPA2 Selector.
 * It has been validated and normalised whie parsing the structure
 * into offsets.  It is stored with a NUL terminator, so the txt
 * field can actually be printed as a normal C-string, also after
 * editing operations are performed on it.
 */
typedef a2sel_t a2id_t;


/** @brief Parse and normalise a string into an ARPA2 Identity.
 *
 * Return true if the input follows the correct grammar.
 * After this, offsets are setup in the .ofs field and
 * the normalised string, including NUL terminator, is
 * written into the .txt field.
 *
 * Normalised identities are in lowercase, except for
 * an optional +SIG+ signature part that will be in
 * upper case.  When we normalise such things beyond
 * ASCII, this is where c16n mappings should go.
 *
 * When inlen is set to 0, its value will be computed
 * with strlen() -- if it is higher than 0 then the
 * in string does not need NUL-termination and may be
 * part of a larger context.  The selector is copied
 * into the out value.
 */
bool a2id_parse (a2id_t *out, const char *in, unsigned inlen);


/** @brief Parse and normalise a string into a remote ARPA2 Identity.
 *
 * Return true if the input follows the correct grammar, where
 * the userid is not interpreted as for a local ARPA2 Identity.
 * The whole part before '@' ends up in the userid string.
 * The corresponding offsets are setup in the .ofs field and
 * the normalised string, including NUL terminator, is
 * written into the .txt field.
 *
 * Local parts are not case-normalised and no signatures can be
 * assumed to be part of the name.  The only parts that are
 * handled cleverly are the domain names.
 *
 * When inlen is set to 0, its value will be computed
 * with strlen() -- if it is higher than 0 then the
 * in string does not need NUL-termination and may be
 * part of a larger context.  The selector is copied
 * into the out value.
 */
bool a2id_parse_remote (a2id_t *out, const char *in, unsigned inlen);


/** @brief Parse and normalise a string into an ARPA2 Selector.
 *
 * Return true if the input follows the correct grammar.
 * After this, offsets are setup in the .ofs field and
 * the normalised string, including NUL terminator, is
 * written into the .txt field.
 *
 * Normalised identities are in lowercase, except for
 * an optional +SIG+ signature flags that will be in
 * upper case.  When we normalise such things beyond
 * ASCII, this is where c16n mappings should go.  As
 * part of signature validation, the normalisation
 * trims off anything beyond the signature _flags_
 * by looking at its self-termination marker.  This
 * can be used to compare against ARPA2 Identity with
 * a signature, to select the signature on strength.
 *
 * When inlen is set to 0, its value will be computed
 * with strlen() -- if it is higher than 0 then the
 * in string does not need NUL-termination and may be
 * part of a larger context.  The selector is copied
 * into the out value.
 */
bool a2sel_parse (a2sel_t *out, const char *in, unsigned inlen);


/** @brief Test if two ARPA2 Identity values are the same.
 *
 * Return true on yes, false on no.
 *
 * This quick test on equality can help to weed out
 * special cases that might arise during use of the
 * more advanced comparisons.
 */
#define a2id_equal(a,b) (0 == strcmp ((a)->txt, (b)->txt))


/** @brief Test if two ARPA2 Selector values are the same.
 *
 * Return true on yes, false on no.
 *
 * This quick test on equality can help to weed out
 * special cases that might arise during use of the
 * more advanced comparisons.
 */
#define a2sel_equal(a,b) (0 == strcmp ((a)->txt, (b)->txt))


/** @brief Test if an ARPA2 Identity matches an ARPA2 Selector.
 *
 * One might think of an ARPA2 Identity as being a member
 * of the set permissible under an ARPA2 Selector.  In
 * this line of thought, this function is a "member-of"
 * test.
 *
 * Note that every ARPA2 Identity is also a valid
 * ARPA2 Selector, and so the function is a special
 * use of a subset-or-equals test between Selectors.
 */
// bool a2id_match (a2id_t *id, a2sel_t *sel);
//
#define a2id_match(id,sel) (a2sel_subseteq ((id),(sel)))
//
#define a2id_member(id,sel) (a2id_match ((id),(sel)))


/** @brief Test if the left ARPA2 Selector is a specialised form of
 * the right ARPA2 Selector.
 *
 * One might read this as a subseteq operation placed as
 * a testing condition between the (sets of identities
 * that would match) the ARPA2 Selectors.
 *
 * Note that ARPA2 Selectors form partially ordered
 * subsetting relations; if the left is not more specific
 * than the right than it is not automatically true that
 * the right is more specific than the left.  They might
 * simply have incomplete overlap in both tests.
 *
 * Also note that the overlap can be perfect in both
 * tests when the two ARPA2 Selectors are identical.
 *
 * Return true when the first is more specific than
 * the second.  Return false if this is not the case.
 */
bool a2sel_special (const a2sel_t *special, const a2sel_t *general);
//
#define a2sel_subseteq(s,g) a2sel_special ((s), (g))



/** @brief Shift text in an ARPA2 Selector.
 *
 * The A2ID_OFS_xxx index into
 * the internal offset array is given, along with the value to
 * write to that offset.  The text will be shifted forward or
 * backward as required, so text is removed and/or a known size
 * is available to write from an earlier starting pointing.
 * Offsets are updated; later ones receive an offset and when
 * earlier ones are overwritten they will be squeezed down.
 * Use the before flag to indicate that the work should be
 * considered to be done before the indexed offset.
 */
bool a2sel_textshift (a2id_t *shifty, a2id_offset_t ofsidx,
			uint16_t newofs, bool before);



/** @defgroup arpa2identity_iter Iteration of ARPA2 Selectors
 * @{
 */


/** @brief Iterate an ARPA2 Selector or ARPA2 Identity.
 *
 * This turns it into an ever more abstract structure, but
 * the first bet is always the original structure.
 *
 * Signatures are always removed during iteration.
 * Just asking for presence of a signature adds
 * nothing.  And the signature flags are unsuitable
 * for iteration because that would call for the
 * (expensive) iteration over all possible subsets.
 * You can use the a2id_match() and variants to set
 * a lower bound on signatures.
 *
 * Domains are iterated by removing one label at a
 * time and starting with the dot after it.  The
 * final domain is "." or the most abstract one.
 *
 * Users for which we rely on ARPA2 semantics can
 * have aliases, so we can iterate by removing
 * one of these in turns, but leaving the + that
 * precedes it as an "open end".  The last of the
 * user patterns mentions just the user, and no
 * user at all.
 *
 * Services are always represented with their
 * name, but the arguments may be stripped one
 * by one, as for user aliases, and the + is
 * left as an "open end" in that case too.
 *
 * The outer loop of iteration (slow changes) are
 * for domain abstraction.  The inner loop (fast
 * changes) are for user/service iteration.  In
 * every iteration, the most abstract form is the
 * last and will be the "@." selector.
 *
 * The iterated value is stored in the cursor,
 * and updated on every _next invocation.  The
 * routines can be used like this (example for
 * an a2id_t, but a2sel_t works the same):
 *
 *	a2id_t me;
 *	a2sel_t crs;
 *	if (a2id_iterate_init (&me, &crs)) do {
 *		...process (crs)...
 *	} while (a2id_iterate_next (&me, &crs));
 *
 */
bool a2sel_iterate_init (const a2sel_t *start, a2sel_t *cursor);

/** @brief Progress to the next Iterator value.
 */
bool a2sel_iterate_next (const a2sel_t *start, a2sel_t *cursor);

/** @brief Start iterating over an ARPA2 Identity.
 */
#define a2id_iterate_init(in,cursor) a2sel_iterate_init ((const a2sel_t *) (start), (cursor))

/** @brief Progress to the next Iterator value.
 */
#define a2id_iterate_next(in,cursor) a2sel_iterate_next ((const a2sel_t *) (start), (cursor))


/** @brief Storage structure for Quick Iteration.
 */
typedef struct {
	const a2sel_t *src;
	const char *uid;
	int uidlen;
	const char *dom;
} a2sel_quickiter;

/** @brief Lightweight / Lightning-fast ARPA2 Selector and Identity
 * iteration.
 *
 * This uses a simple structure with a pointer
 * to the userid and domain, with a length field for the
 * former and NUL termination for the latter.  This means
 * that a little more work is done by the user, but there
 * is less pressure on the iterator in terms of strings
 * being copied and offsets of the a2sel_t being kept up
 * to date.
 *
 * This mechanism is perfect for fast implementations, and
 * is specifically interesting for database lookups such as
 * the ACL logic of <arpa2/access.h>.
 *
 * You are supposed to fill the src value and ensure its
 * stability througout iteration.  The use is like:
 *
 *	a2sel_quickiter qiter;
 *	qiter.src = ...ref...;
 *	if (a2sel_quickiter_init (&qiter)) do {
 *		...printf ("%.*s@%s", qiter.uidlen, qiter.uid, qiter.dom)...
 *	} while (a2sel_quickiter_next (&qiter));
 */
bool a2sel_quickiter_init (a2sel_quickiter *iter);

/** @brief Iterate to the next Quick Iterator value.
 */
bool a2sel_quickiter_next (a2sel_quickiter *iter);


/** @brief Count the number of abstractions to go from one ARPA2 Selector (or Identity)
 * to another ARPA2 Selector.
 *
 * This is done as fast as possible, and no other
 * information is derived.  The return value is false when it is impossible
 * to make these steps, so it is another mechanism to test specialisation.
 *
 * Counting is not linear; to ensure that username abstractions count as less
 * influential, they are greatly outnumbered by domain name abstractions.
 * You can exploit this to infer the number of abstraction steps for the
 * domain and username, by passing a2sel_abstractions_domain() and _steps()
 * over the steps output here.
 *
 * The purpose of this function is to handle multiple matches, and learn
 * which is the closest match.  This can be practical when comparing an
 * Access Rule, such as one loaded from LDAP or in an application that defines
 * Access Rules in a static configuration.
 *
 * On success, the function returns true.  On failure, it returns false and
 * sets errno to a com_err code.  In either case, it sets steps to a value,
 * but failure will set the highest possible value, which is unreachable in
 * any other way.
 */
bool a2sel_abstractions (const a2sel_t *specific, const a2sel_t *generic, uint16_t *steps);
//
#define a2sel_abstractions_domain(steps)   ((steps) >> 7)
#define a2sel_abstractions_username(steps) ((steps) & 0x007f)



/** @} */



/** @defgroup arpa2identity_sign Signatures on ARPA2 Identities
 * @{
 */


/** @brief Add a key to be used for signed identities.
 *
 * The last
 * key added will be used for signatures.  Keys added
 * before it only serve as a validation option.  Call
 * this function as part of your initialisation, then
 * try to drop privileges to reach the key material.
 *
 * The key is read from a file descriptor, and should
 * not exceed PIPE_BUF from <limits.h> in size.  This
 * construct allows you to load from a pipe or fifo,
 * which can usually be read only once.  Keys should
 * fit easily into even the minimum PIPE_BUF in POSIX
 * specifications.
 *
 * Keys are obtained in a single read, and should end
 * in a newline (a simple completeness check).  Small
 * sizes are considered an error.  The key starts as
 * "arpa2id", comma, 3-level semantic version for the
 * first software to define the signature computation,
 * comma, algorithm name, comma, and finally the key's
 * entropy in any desired form desired.  We like to
 * use BASE32 but are mindful of case sensitivity of
 * this input.
 *
 * The file descriptor is not closed in this function;
 * you may continue to use it for other purposes.
 */
bool a2id_addkey (int keyfd);


/** @brief Wipe all keys clean.
 *
 * Do not return the memory to the pool,
 * but on a recycling list so those zeroes seem really important
 * to the compiler.
 */
void a2id_dropkeys (void);


/** @brief Signature data types.
 *
 * These are managed in a
 * repository, but since signatures are validated on the
 * same place as they are made it does not really matter
 * whether this is followed.  Only for reasons of software
 * compatibility is it _quite_ useful to have a standard.
 *
 * The registry is located at
 * http://a2id.arpa2.org/sigflags.html
 */
typedef enum {
	//
	// char #0, bits 0..3, index values 0..3
	A2ID_SIGDATA_EXPIRATION,	/* Include expiration timer (EXP), in days since key creation */
	A2ID_SIGDATA_REMOTE_DOMAIN,	/* UTF-8 String, from _DOT_DOMAIN to _END */
	A2ID_SIGDATA_REMOTE_USERID,	/* UTF-8 String, up to _AT_DOMAIN, may be case-sensitive!!! */
	A2ID_SIGDATA_LOCAL_ALIASES,	/* UTF-8 String from _PLUS_ALIASES to _PLUS_SIG
					   or, if that is empty, _PLUS_SVCARGS to _PLUS_SIG */
	//
	// char #1, bits 0..3, index values 4..7
	A2ID_SIGDATA_SESSIONID,		/* Machine-generated, protocol-specific session identity */
	A2ID_SIGDATA_SUBJECT,		/* Human-entered, protocol-specific, aim for a canonical form */
	A2ID_SIGDATA_TOPIC,		/* Automation-friendly, protocol-specific, like [TOPIC] in subject */
	A2ID_SIGDATA_CRYPTLOCAL,	/* Encrypted string from A2ID_OFS_USERID to A2ID_OFS_PLUS_SIG */
	//
	// No futher assigments yet
	A2ID_SIGDATA_COUNT
} a2id_sigdata_t;
//
#define A2ID_SIGFLAG_EXPIRATION		( 1 << A2ID_SIGDATA_EXPIRATION    )
#define A2ID_SIGFLAG_REMOTE_DOMAIN	( 1 << A2ID_SIGDATA_REMOTE_DOMAIN )
#define A2ID_SIGFLAG_REMOTE_USERID	( 1 << A2ID_SIGDATA_REMOTE_USERID )
#define A2ID_SIGFLAG_LOCAL_ALIASES	( 1 << A2ID_SIGDATA_LOCAL_ALIASES )
#define A2ID_SIGFLAG_SESSIONID		( 1 << A2ID_SIGDATA_SESSIONID     )
#define A2ID_SIGFLAG_SUBJECT		( 1 << A2ID_SIGDATA_SUBJECT       )
#define A2ID_SIGFLAG_TOPIC		( 1 << A2ID_SIGDATA_TOPIC         )
#define A2ID_SIGFLAG_CRYPTLOCAL		( 1 << A2ID_SIGDATA_CRYPTLOCAL    )


/** @brief Callback function prototype to retrieve signature data.
 *
 * It receives both the identity structure and the
 * signature requester's cbdata pointer to retrieve
 * data.  A sigdata code indicates what data to pass.
 *
 * No more bytes than the original *buflen are written
 * to buf, and if buf is NULL nothing must be written.
 *
 * In all cases, *buflen is updated to the actual data
 * size.  This includes:
 *  - when buf was supplied as NULL
 *  - when not all data fitted into buf
 *  - regardless of the original *buflen value
 *  - when the data is unavailable, set *buflen to 0
 *
 * In all cases, the function returns true when it has
 * been able to supply the data, and is happy that it
 * held enough bytes.  The new value of *buflen leave
 * the signature routines the choice to call again to
 * fill a larger buffer instead.  If it always wants
 * to take this approach, the signer might first
 * probe for the data size to use by calling with buf
 * set to NULL.
 *
 * The remaining cases are not so happy, and false is
 * returned.  This includes the cases where
 *  - the value of buf was NULL
 *  - the requested data could not be found
 *  - the original value of *buflen was too low
 *
 * The signature algorithm can choose how to handle
 * a false return value; it may try again, decide
 * that the data is not important enough or break
 * off the signature computation.
 */
typedef bool a2id_sigdata_cb (a2id_sigdata_t sd,
			void *cbdata, const a2id_t *id,
			uint8_t *buf, uint16_t *buflen);


/** @brief Basic callback function for a signed ARPA2 Identity.
 *
 * The code of this function doubles as example code for an
 * application-defined callback function; these may wrap
 * around this function to not have to replicate these
 * basic callback functions.
 *
 * This function behaves as may be expected from a
 * a2id_sigdata_cb typed function and can indeed be
 * provided as such a callback to a2id_sign() and
 * a2id_verify() functions.  More elaborate callback
 * functions may also use the code provided herein
 * to derive the intended signature data.
 *
 * This function expects the remote as an a2id_t in
 * its cbdata, and it receives the local ARPA2 Identity
 * from the a2id_sign() or a2id_verify() operation,
 * and with that it can provide this sigdata:
 *
 *  - A2ID_SIGDATA_REMOTE_DOMAIN
 *  - A2ID_SIGDATA_REMOTE_USERID
 *  - A2ID_SIGDATA_LOCAL_ALIASES
 */
bool a2id_sigdata_base (a2id_sigdata_t sd,
			void *cbdata_rid, const a2id_t *lid,
			uint8_t *buf, uint16_t *buflen);


/** @brief Possibly attach a signature to an ARPA2 Identity,
 * by adding extra characters to its username.
 *
 * This silently succeeds when
 * there no key was loaded, and/or when the identity has no
 * signature flags requesting for data to be incorporated.
 *
 * The intention is to make it straightforward to integrate
 * this call in _every_ outgoing path, so that services will
 * facilitate signatures and leave the choice to clients.
 * Signature flags are gathered from an address provided by
 * the local client, and both it and the expiration may be
 * clipped to minimum and maximum values by the software
 * before passing it in here.
 *
 * A service that always calls a2id_sign() on outgoing traffic
 * should also always call a2id_verify() on incoming traffic.
 * This function is similarly permissive; users may require
 * signatures (on some aliases) through their ACL.
 * 
 * A signature would expand the length of the identity so its
 * local-part may span up to the 64 available characters.
 *
 * When the signature was requested and could be constructed,
 * return true and modify the tbs Identity.  When signing
 * failed, return false and unmodified buffer contents.
 *
 * The callback function will be called, with its private
 * data pointer, to retrieve context-dependent data from
 * the surrounding application.  This data may involve the
 * (stylised) content of subject headers, topics, ... as
 * defined in the sigdata enumeration type.
 *
 * Services will want to constrain the sigflags field to the
 * values that they can support before calling a2id_sign().
 * The sigflags is an OR-ed combination of A2ID_SIGFLAG_xxx.
 *
 * When flagging A2ID_SIGFLAG_EXPIRATION, provide a time on
 * the last valid day after today in expiration.
 * The a2id_sign() routine trims the value to a day number
 * since the 18000'th day after Jan 1st, 1970.
 *
 * Keys are supposed to be fixed in a server, and they are
 * found through static global variables.  You initially
 * load them with a2id_addkey() before dropping privileges.
 */
bool a2id_sign (a2id_t *tbs,
		a2id_sigdata_cb cb, void *cbdata);


/** @brief Try to verify a signature on an ARPA2 Identity.
 *
 * An absent signature returns successfully, because it is
 * left to later ACL filters to decide if a signature is
 * required.  When this service did not add keys it is
 * assumed that verification is done elsewehre.
 *
 * After returning successfully, the value in sigflags
 * indicates the flags that have been validated; this
 * is zeroed when nothing was validated.  Likewise, the
 * value returned in expiration is set after the day at
 * which the signature expires.
 *
 * Because it is so lenient, it is safe to always call
 * a2id_verify() on the incoming path.  This pairs up with
 * always calling a2id_sign() on the outgoing path.  To
 * the client, signatures are a feature that they can
 * choose to configure, if and where they desire it.
 *
 * Situations that do lead to errors being returned are
 * those where an address holds a signed Identity and at
 * least one key has been added to this server.  Multiple
 * keys may be loaded to allow for rollover procedures,
 * but when none validate the signature then a failure is
 * reported.  In this case, the sigflags are also zeroed
 * to spread no certainty.
 *
 * Some applications will want to enforce signatures.
 * They might look at the returned value in *sigflags
 * and at least test it to be non-zero, or they might
 * use an ARPA2 Selector that contains the signature
 * flags portion FLG in its grammar.  When sigflags
 * contiains A2ID_SIGFLAG_EXPIRATION, then expiration
 * is checked to not have passed.
 *
 * Keys are supposed to be fixed in a server, and they are
 * found through static global variables.  You initially
 * load them with a2id_addkey() before dropping privileges.
 */
bool a2id_verify (a2id_t *a2id_to_be_verified,
		a2id_sigdata_cb cb, void *cbdata);


/** @} */



/** @brief Print a parsed ARPA2 Identity structure on the detail
 * debugging output.
 *
 * This is normally maps to doing nothing, but it can
 * help with deep-down-debugging when DEBUG_DETAIL is set.
 *
 * The output shows the buffered identity with '|' bars
 * to signify the offsets.  Since these are monotonically
 * rising, counting them suffices to understand how the
 * information is split up.  This is what parsing does;
 * it defines what range of characters fit into certain
 * forms of meaning.
 */
#ifndef DEBUG_DETAIL
#define a2sel_detail(descr,id) do { ; } while (0)
#define a2id_detail(descr,id) a2sel_detail ((descr), (a2sel_t *) (id))
#else
#include <arpa2/except.h>
static inline void a2sel_detail (char *descr, const a2sel_t *sel) {
	int arrows = A2ID_OFS_COUNT;
	int buflen = sel->ofs [A2ID_OFS_END] + arrows + 1;
	char buf [buflen];
	int bufofs = 0;
	int selofs = 0;
	/* Print all but the final _BUFSIZE after the NUL char */
	while (arrows > 0) {
		assertxt (sel->ofs [A2ID_OFS_COUNT - arrows] >= selofs,
			"A2ID_OFS_xxx are not monotonically rising");
		if (sel->ofs [A2ID_OFS_COUNT - arrows] == selofs) {
			buf [bufofs++] = '|';
			arrows--;
			continue;
		}
		buf [bufofs++] = sel->txt [selofs++];
	}
	buf [bufofs] = '\0';
	log_detail ("%s: %s", descr, buf);
}
#endif /* DEBUG_DETAIL */


#endif /* ARPA2_IDENTITY_H */

/** @} */
