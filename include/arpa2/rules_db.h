/** @ingroup rules_h
 * @{
 * @defgroup rules_db_h <arpa2/rules_db.h> RuleDB underpinning Access Control, Groups, ...
 * @{
 * The database RuleDB stores records that each hold a Ruleset,
 * consisting of NUL-terminated Rules with generic words:
 * @code
 *
 *    #commentlabel =xvalue %FLAGS ^trigger ~selector
 *
 * @endcode
 * See documentation of [Rules](doc/RULES.MD) and
 * [Access Control for Communication](doc/ACCESS_COMM.MD) for details.
 *
 * Lookups are based on an LMDBkey which is a beginning of a secure
 * hash of lookup data; the remainder of the secure hash is stored
 * as a RESTkey in the beginning of the record, which is checked.
 * This logic is concealed within the database.c implementation.
 *
 * There is another useful prefix to the database values, and that
 * is a trunk code.  These are ignored by applications, but serve
 * bulk treatment of database entries, usually related to a certain
 * remote provider that originates or retrieves the data.  The
 * trunk code is provided during rules_dbopen() and defaults to 0.
 *
 * The default location is "/var/lib/arpa2/access/" but may be
 * set in RULES_ENVIRONMENT_PATH; the name of the database defaults
 * to "RuleDB" byt may be set in RULES_DATABASE_NAME; the size of
 * the database defaults to 1048576000L but may be overridden with
 * RULES_DATABASE_SIZE; the number of databases is maximally 10 but
 * this may be overridden with RULES_DATABASE_COUNT_MAX.
 */

/*
 * SPDX-License-Identifier: BSD-2-Clause
 * SPDX-FileCopyrightText: Copyright 2020 Rick van Rein <rick@openfortress.nl>
 */


#ifndef ARPA2_RULES_DB_H
#define ARPA2_RULES_DB_H


#include <stdlib.h>
#include <stdint.h>
#include <stdbool.h>

#include <lmdb.h>

#include <arpa2/digest.h>
#include <arpa2/identity.h>



// A few good sources on LMDB knowledge:
//
// https://blogs.kolabnow.com/2018/06/07/a-short-guide-to-lmdb
// http://www.lmdb.tech/doc/starting.html
// https://github.com/LMDB/lmdb/blob/mdb.master/libraries/liblmdb/lmdb.h


/** @defgroup rulesdata Data Structures and Constants for Rules and RuleDB
 * @{
 * Database entries are keyed by a secure hash, which helps to
 * distribute keys evenly over any prefix of bits, but it is not
 * efficient to store complete hashes as keys in the B-tree.
 * Partial keys allow more entries per page, meaning less pages
 * to load.  But more importantly, it leads to less levels being
 * loaded.
 *
 * Say we use SHA-256, so keys are 32 bytes long.  Compare that
 * to a 64-bit key which is native to LMDB.  Assume pages of 4kB.
 * As alternatives, we may consider the MDB_INTEGERKEY offering
 * of size_t and unsigned int types, 8 and 4 bytes on amd64.
 *
 * Keys of 32 bytes need 32 bytes of storage (not counting length)
 * and pointers are 8 bytes on a 64-bit machine.  In 4 kB we can
 * hold trunc ((4096-8) / (32+8)) = 102 keys and 103 downlinks.
 * The number of levels is the 103-base log of the key count.
 *
 * Keys of fixed 8 bytes need precisely 8 bytes of storage and
 * pointers need the same amount.  In 4 kB we can hold 255 keys
 * and 256 downlinks.  The number of levels is the 256-base log
 * of the key count.
 *
 * The second case requires 256/103 = 2.4852 times less pages
 * in depth, and each page stores more keys, making the cache of
 * index pages is 255/102 = 2.500 times more effictive.
 *
 * Keys of fixed 4 bytes (common for unsigned int) need 4 bytes
 * of storage, and 4 kB can hold 340 keys and 341 downlinks, so
 * the number of levels is a 341-base log.  That is a factor
 * 1.332 more efficient than 8-byte keys and the cache is a
 * factor 1.33 more efficient.  We add an option to allow this
 * as an explicit choice, namely RULES_TIGHT_LMDBKEY.  This
 * configuration flag impacts the storage format.
 *
 * The hash value does add protection against pre-image attacks,
 * so it must not be squandered.  We shall add the remaining
 * bytes as part of the value, namely in the beginning of each
 * record.  We therefore devise a derivation from hash value
 * to (1) an LMDB key and (2) a rest key to be stored in the
 * value field.  And we define macros to extract those parts
 * from a digest key.
 *
 * The risk of running into one clash with 4-byte keys is 50%
 * at sqrt(2^32) == 65536 entries, so it is likely, but still
 * just once and generally rare on individual matches; but we
 * need to handle clashes.  We had to anyway.
 *
 * The database should be opened as DUP_SORT, and accessed
 * with a cursor until we find a match with the rest key.
 * In DUP_SORT databases, considers key/value pairs are
 * considered the unique entries, not merely the keys so we
 * write and delete key/value pairs, which involves RESTkey
 * information as it incorporated into the value.
 *
 * Future versions of the database may add encryption; in
 * that variant, it should be helpful if not all entropy of
 * the lookup key is revealed as the database lookup key.
 */


/** Data structures, kept minimally sized under RULES_TIGHT_LMDBKEY.
 *
 * @typedef rules_dbkey_lmdbkey
 * @brief   Integer value for the LMDB lookup key (beginning of rules_dbkey)
 *
 * @typedef rules_dbkey
 * @brief   Byte array for storage of database keys
 *
 */
#ifdef RULES_TIGHT_LMDBKEY
	typedef unsigned int rules_dbkey_lmdbkey;
	typedef uint8_t rules_dbkey [A2MD_OUTPUT_SIZE]
		__attribute__((__aligned__(4)));
#else /* !RULES_TIGHT_LMDBKEY */
	typedef size_t rules_dbkey_lmdbkey;
	typedef uint8_t rules_dbkey [A2MD_OUTPUT_SIZE]
		__attribute__((__aligned__(8)));
#endif /* RULES_TIGHT_LMDBKEY */

/**
 * @def   RULES_LMDBKEY_SIZE
 * @brief The number of bytes in the LMDB key, which is only part of a hash;
 *        the remainder is RULES_RESTKEY_SIZE and the start is given by
 *        digest2lmdbkey()
 *
 * @def   RULES_RESTKEY_SIZE
 * @brief The number of bytes stored in the LMDB value, matching the remainder
 *        of the has that comes after the RULES_LMDBKEY_SIZE and the start is
 *        given by digest2restkey()
 *
 * @def   digest2lmdbkey(digest)
 * @brief Extract a pointer to the LMDB key from a digest pointer; the size
 *        is RULES_LMDBKEY_SIZE
 *
 * @def   digest2restkey(digest)
 * @brief Extract a pointer to the RESTkey from a digest pointer; the size is
 *        RULES_RESTKEY_SIZE
 */

#define RULES_LMDBKEY_SIZE (sizeof (rules_dbkey_lmdbkey))
#define RULES_RESTKEY_SIZE (A2MD_OUTPUT_SIZE - RULES_LMDBKEY_SIZE)

#define digest2lmdbkey(digest) ((rules_dbkey_lmdbkey *) ((uint8_t *) digest))
#define digest2restkey(digest) (((uint8_t *) digest) + RULES_LMDBKEY_SIZE)


/** Database configuration: environment path, name, size, maximum count.
 *
 * @def   RULES_ENVIRONMENT_PATH
 * @brief The directory that LMDB considers its (storage) environment
 *
 * @def   RULES_DATABASE_NAME
 * @brief The name of the database
 *
 * @def   RULES_DATABASE_SIZE
 * @brief The size of the database in bytes
 *
 * @def   RULES_DATABASE_COUNT_MAX
 * @brief Maximum number of databases to store in the environment
 *
 */

#ifndef RULES_ENVIRONMENT_PATH
#define RULES_ENVIRONMENT_PATH "/var/lib/arpa2/access/"
#endif

#ifndef RULES_DATABASE_NAME
#define RULES_DATABASE_NAME "RuleDB"
#endif

#ifndef RULES_DATABASE_SIZE
#define RULES_DATABASE_SIZE 1048576000L
#endif

#ifndef RULES_DATABASE_COUNT_MAX
#define RULES_DATABASE_COUNT_MAX 10
#endif



//TODO//TESTPROGRAM// assert (sizeof (rules_dbkey) == A2MD_OUTPUT_SIZE);


/**
 * @def   RULES_TRUNK_ANY
 * @brief Wildcard value for trunk identities.
 *
 * @def   RULES_TRUNK_MIN
 * @brief Trunk identities count up from 1.
 *
 * @def   RULES_TRUNK_MAX
 * @brief Trunk identities are stored in 32 bits.
 *
 * @def   RULES_TRUNK_SIZE
 * @brief Trunk identities are stored in 32 bits.
 *
 */
#define RULES_TRUNK_ANY  0
#define RULES_TRUNK_MIN  1
#define RULES_TRUNK_MAX  4294967295
#define RULES_TRUNK_SIZE 4


/** @brief Trunk identities are stored as 4-byte values in network order.
 */
typedef union {
	uint8_t netbytes [RULES_TRUNK_SIZE];
	uint32_t netint32;
} rules_trunk;


/** @brief The RuleDB structure keeps track of LMDB control.
 *
 * It holds the key for the last rules_dbget() call
 * and needs it when rules_dbset() is subsequently
 * called.  It also remembers if a value was found
 * then, to know if a record must be updated or added.
 */
struct rules_db {
	MDB_env    *env;
	MDB_txn    *txn;
	MDB_dbi     dbi;
	MDB_cursor *crs;
	rules_dbkey dig;
	bool        got;
	rules_trunk trk;
};


/** @} */
// And another... probably due to upsetting __attribute__((__aligned__())) above
/** @} */


/** @defgroup rulesdbmanage Management operations for the RuleDB
 * @{
 */


/** @brief Rollback recent writes to the ACL database.
 */
bool rules_dbrollback (struct rules_db *ruledb);


/** @brief Commit recent changes to the ACL database.
 */
bool rules_dbcommit (struct rules_db *ruledb);


/** @brief Suspend reading from the ACL database.
 */
bool rules_dbsuspend (struct rules_db *ruledb);


/** @brief Resume reading from the ACL databaes.
 */
bool rules_dbresume (struct rules_db *ruledb);


/** @brief Open the ACL database, possibly for bulk updates.
 *
 * Set \a rdonly to `false` to enable write access.  This implies more
 * rigid locking strategies, so any such actions should be short-lived.
 *
 * Set the \a trunk to look at a specific value, or use RULES_TRUNK_ANY
 * for accessing any trunk identity.
 *
 * The ruledb parameter should be zeroed before
 * first use.  After rules_dbclose() it may be reused for another round
 * of rules_dbopen() without first zeroing content.
 */
bool rules_dbopen (struct rules_db *ruledb, bool rdonly, uint32_t trunk);


/** @brief Open the ACL database for reading.
 *
 * The ruledb parameter should be zeroed before
 * first use.  After rules_dbclose() it may be reused for another round
 * of rules_dbopen() without first zeroing content.
 */
static inline bool rules_dbopen_rdonly (struct rules_db *ruledb) {
	return rules_dbopen (ruledb, true, RULES_TRUNK_ANY);
}


/** @brief Close the ACL database.
 */
bool rules_dbclose (struct rules_db *ruledb);


/** @} */


/** @defgroup rulesets Operations on Rulesets
 * @{
 */


/**
 * @static rules_dbnext
 * @brief Next in iteration over a ruleset, started with rules_dbloop().
 * @returns true if a Rule was found
 *
 * Iterate over an MDB_val with a sequence of NUL-terminated rules,
 * as returned by rules_dbget().  The normal construction is
 * @code
 *
 *	... rules_dbget (..., &dbdata);
 *	char *rule;
 *	if (rules_dbloop (&dbdata, &rule)) do {
 *		...process (rule)...
 *	} while (rules_dbnext (&dbdata, &rule));
 *
 * @endcode
 */
static inline bool rules_dbnext (const MDB_val *dbdata, char **rule) {
	*rule += strlen (*rule) + 1;
	return ((*rule) < ((char *) dbdata->mv_data) + dbdata->mv_size);
}
/**
 * @static rules_dbloop
 * @brief Start iteration over a ruleset, continue with rules_dbnext().
 * @returns true if a Rule was found
 */
static inline bool rules_dbloop (const MDB_val *dbdata, char **rule) {
	*rule = (char *) dbdata->mv_data;
	return (dbdata->mv_size > 0);
}


/** @brief Get rules from the RuleDB.
 *
 * Every rule is terminated by a NUL character
 * and the functions rules_dbloop() and _dbnext() can be used as
 * iterators to pass through them.
 *
 * This function returns true on success.  This includes when it finds
 * no record, for which it returns an empty set of rules.
 *
 * The digest key will be split into the LMDBkey and RESTkey.  Cursor
 * iteration is used to search for a record to allow just that.
 *
 * Note that what the database considers a value is not yet a value to
 * the application.  The first RULES_TRUNK_SIZE bytes hold the trunk
 * identifier, then comes the RESTkey and only then does application
 * data start.  This application data holds a Ruleset in the case of
 * the RuleDB; this is also called an Access Control List in some cases.
 * This call returns only the application data.
 */
bool rules_dbget (struct rules_db *ruledb, rules_dbkey digkey, MDB_val *out_dbdata);


/**
 * @typedef changerules_what
 * @brief   Whether to add or delete rules in the ruleset.
 *
 * @fn    rules_edit_generic
 * @brief Generic handling for ruleset addition and deletion.
 *
 * When an opt_selector is provided, the parser forbids "~selector"
 * words.  Instead, this one opt_selector is then lardered with the rule.
 *
 * When an opt_nested RuleDB is provided, its read/write transaction
 * is used instead of opening a local one.  This is like running a
 * nested transaction inside this function, and using it to compose
 * a larger one with multiple of these transactions.  The return
 * value indicates success of the internal "transaction" and when
 * false is returned, the opt_nested RuleDB should (eventually) be
 * rolled back too.
 *
 * Some normalisation may be done on the rule, but it is the same for
 * addition and deletion, so that will cancel out.
 */
typedef enum { do_add, do_del } changerules_what;
//
bool rules_edit_generic (rules_dbkey prekey, unsigned prekeylen,
			char *xskey,
			char *rules, unsigned ruleslen,
			changerules_what whattodo,
			a2sel_t *opt_selector, struct rules_db *opt_nested);


/** @brief Add rules to the database.
 *
 * Given a prepared key encompssing the Access Domain and Type, create
 * records for the given Access Name and Rule.  The records should
 * represent the ruleset provided as concatenated NUL-terminated strings.
 * When opt_selector is provided, the changes will be made specifically
 * for that Selector; otherwise, "~selector" words may specify one
 * or more Selector to update.
 *
 * This function returns true on success only.  This success is the
 * result of a commit on the underlying database, so it ought to be
 * true for all the records that a rule may generate.
 */
static inline bool rules_dbadd (rules_dbkey prekey, unsigned prekeylen,
			char *xskey,
			char *rules, unsigned ruleslen, a2sel_t *opt_selector) {
	return rules_edit_generic (prekey, prekeylen, xskey, rules, ruleslen,
			do_add, opt_selector, NULL);
}


/** @brief Delete rules from the database.
 *
 * Given a prepared key encompssing the Access Domain and Type, delete
 * records for the given Access Name and Rule.  The records should
 * represent the ruleset provided as concatenated NUL-terminated strings.
 * When opt_selector is provided, the changes will be made specifically
 * for that Selector; otherwise, "~selector" words may specify one or more
 * Selector to update.
 *
 * This function returns true on success only.  This success is the
 * result of a commit on the underlying database, so it ought to be
 * true for all the records that a rule may generate.
 */
static inline bool rules_dbdel (rules_dbkey prekey, unsigned prekeylen,
			char *xskey,
			char *rules, unsigned ruleslen, a2sel_t *opt_selector) {
	return rules_edit_generic (prekey, prekeylen, xskey, rules, ruleslen,
			do_del, opt_selector, NULL);
}


/** @brief Set rules in the RuleDB.
 *
 * The rules must be NUL terminated but more
 * than one of them can be combined in the inputs.  The two offered
 * bits are concatenated to simplify updates.  Even more friendly use
 * is possible with the wrappers rules_dbadd() and _dbdel().
 *
 * This function operates on the assumption that rules_dbget() was
 * run before, and returned successfully.  This is why no digest key
 * is required.  The input data is allowed to point into data that was
 * acquired during that call.
 *
 * To add rules with this function, set in0_dbdata to the result of
 * rules_dbget() and set in1_dbdata to the new rules, each with
 * a NUL terminator.  Or switch the two to insert at the beginning.
 *
 * To delete rules with this function, set in0_dbdata to the part of
 * rules_dbget() before the rule that will be removed, in1_dbdata
 * to the part after the rule to be removed (making sure to also
 * remove its trailing NUL character).  Either or both of these may
 * be empty.
 *
 * This function returns true on succes.
 *
 * Note that what the database considers a value is not yet a value to
 * the application.  The first RULES_TRUNK_SIZE bytes hold the trunk
 * identifier, then comes the RESTkey and only then does application
 * data start.  This application data holds a Ruleset in the case of
 * the RuleDB; this is also called an Access Control List in some cases.
 * This call expects to be handed only application data.
 */
bool rules_dbset (struct rules_db *ruledb, MDB_val *in0_dbdata, MDB_val *in1_dbdata);


/** @} */



/** @defgroup ruleskeyderive Database Key derivation and Privacy Control
 * @{
 * The keys used to lookup information in the RuleDB are formed in
 * a number of steps.  Each of these steps reduce the ability of the
 * holder of a key to access information from the RuleDB.
 *
 * Current protection is agains treating the RuleDB as an oracle for
 * keys, which would allow stolen databases to yield sensitive data
 * that identifies users.  In a future release, encrypted values will
 * also shield the technical data contained therein from prying eyes.
 *
 * Note that the keys are derived from a message digest, and even if
 * that process uses a secure hash, it still works on rather limited
 * entropy.  The result is that guessing becomes feasible, both for
 * the key and (future:encrypted) value.  The use of a Database Secret
 * in the first stage avoids that, by inserting actual entropy, known
 * only to the database manager, in the top of the key derivation chain:
 *
 *  -# **Database Secret** is a code that is known only to the
 *     owner of the RuleDB
 *  -# **Domain Key** may be known to the administrator of a
 *     domain name
 *  -# **Service Key** may be distributed to administrators of
 *     services of a given Access Type and is specific to a domain
 *     name being serviced
 *  -# **Request Key** is derived for a specific request
 *
 * @note None of this is obligatory; it is mostly very helpful to
 *       structure commercial hosting operations and implement
 *       privacy laws and assumed by default in ARPA2 Access Control,
 *       Groups and anything else built on these Rules and the RuleDB.
 */


/** @brief Derive the Domain Key for a given domain name and
 *         optional Database Secret.
 *
 * First phase of key preparation,
 *   - [Database Secret], Domain being accessed --> "Domain Key"
 * to be followed by
 *   - Access Type                              --> "Service Key"
 *   - Access Name, Selector (which walks up)   --> "Request Key"
 *
 * This function may precede rules_dbkey_service().
 *
 * This phase is usually performed in a restricted area,
 * and protects the Database Secret from being shared between
 * domains.  It does this by hashing the key along with
 * the domain and returning the result as a next-level key.
 *
 * @param[out] domkey The Domain Key derived herein
 * @param[in] opt_dbkey Optional Database Secret key bytes, set to NULL for none
 * @param[in] dkkeylen The length of \a opt_dbkey if not NULL
 * @param[in] xsdomain The Access Domain as a NUL-terminated UTF-8 string
 * @returns true on success, false with errno set on failure
 *
 * @note Any byte sequence works as the Database Secret.  If it is not
 *       provided then anyone can compute the Domain Key, Service Key and
 *       Request Key with only the knowledge of the other public inputs.
 *       Such a party can start looking up data in your database.  Using
 *       a Database Secret constrains the leakage of sensitive data to
 *       only the are derived from any derived key; this may help you to
 *       implement privacy policies.  Domain Keys can query all information
 *       underneath a domain name; Service Keys can query all information
 *       underneath a domain's service (Access Type).  Request Keys can
 *       only query information for a given request.
 *
 * @note The \a xsdomain is the domain name after mapping to Unicode;
 *       it must not use Punycode, the wire format for Internationalised DNS.
 */
bool rules_dbkey_domain (rules_dbkey domkey,
			const uint8_t *opt_dbkey, int dbkeylen,
			const char *xsdomain);


/** @brief Derive the Service Key for a given Access Type,
 *         using a Domain Key as a start.
 *
 * After the key preparation in phase 1,
 *   - [Database Secret], Domain being accessed --> "Domain Key"
 * we turn to the second phase of key preparation,
 *   - Access Type                              --> "Service Key"
 * to be followed by
 *   - Access Name, Selector (which walks up)   --> "Request Key"
 *
 * This function may follow after rules_dbkey_domain() and may
 * precede by rules_dbkey_selector().
 *
 * This is intended to derive purpose-specific keys that can be
 * distributed to service providers.  They will share access data
 * with other providers offering the same access type, which is
 * somewhat unavoidable, but they will not be able to see the
 * access control lists for other access types.
 *
 * @param[out] svckey The Service Key derived herein
 * @param[in] domkey The previously derived Domain Key
 * @param[in] domkeylen The length of \a svckey
 * @param[in] xstype The Access Type input
 * @returns true on success, false with errno set on failure
 */
bool rules_dbkey_service (rules_dbkey svckey,
			const uint8_t *domkey, unsigned domkeylen,
			const uint8_t xstype [16]);


/** @brief Derive the Request Key for a given Access Name and
 *         Selector, using a Domain Key as a start.  Note that
 *         this functionality may be wrapped inside an iterator
 *         like rules_dbiterate()
 *
 * After the key preparation in phase 1+2,
 *   - [Database Secret], Domain being accessed --> "Domain Key"
 *   - Access Type                              --> "Service Key"
 * we turn to the final phase of key derivation,
 *   - Access Name, Selector (which walks up)   --> "Request Key"
 *
 * This function may follow after rules_dbkey_service().
 *
 * This is intended to derive database lookup keys that can be
 * used as parameters to rules_dbget() and rules_dbset().
 *
 * The form of the Access Name is determined by the Access Type,
 * but generally takes the shape of an UTF-8 string.  The
 * Selector is provided in parsed and normalised form in a data
 * structure from a2sel_parse().
 *
 * This process is sometimes broken into pieces, where the digest
 * is cloned after writing the svckey bytes and xsname message; this
 * is helpful for efficient iteration over various selector values.
 * This is not done here, but it is supported by digesting the
 * selector last.
 *
 * @param[out] reqkey The Request Key derived herein
 * @param[in] svckey The previously derived Service Key
 * @param[in] svckeylen The length of \a svckey
 * @param[in] xsname The Access Name input
 * @param[in] selector The Selector to lookup for
 * @returns true on success, false with errno set on failure
 */
bool rules_dbkey_selector (rules_dbkey reqkey,
			const uint8_t *svckey, unsigned svckeylen,
			const char *xsname,
			const a2sel_t *selector);


/** @} */

#endif /* ARPA2_RULES_DB_H */

/** @} */
/** @} */
