/** @defgroup arpa2socket Socket utilities: Parsing, binding, connecting
 *
 * Socket utilities often end up parsing ports and addresses, figuring
 * out how to offer fallback support for IPv4 in a world full of IPv6,
 * setting non-blocking behaviour, and so on.
 *
 * These general routines help with such standard tasks.  There is also
 * portability support towards Windows (although that is implementing
 * evermore of the other operating system, the one that is stable).
 *
 * This module has no ambition of incorporating DNS support.  Resolving
 * that, possible with security, iteration and parsing records is a
 * specialised topic for which external libraries are better equipped.
 *
 * @{
 *
 * SPDX-License-Identifier: BSD-2-Clause
 * SPDX-FileCopyrightText: Copyright 2020 Rick van Rein <rick@openfortress.nl>
 */


#ifndef ARPA2COMMON_SOCKET_H
#define ARPA2COMMON_SOCKET_H


#include <stdbool.h>
#include <stdint.h>

#include <errno.h>
// #include <com_err.h>
#include <arpa2/com_err.h>


/** @def socket_init
 *
 * Initialise sockets at a library level.  For portability; some operating
 * systems need this.
 */

/** @def socket_fini
 *
 * Finalise sockets at a library level.  For portability; some operating
 * systems need this.
 */

/** @def socket_close
 *
 * Close a socket in a portable manner.  This may be quite simple at the
 * programming level, but is a strong requirement on platforms on which
 * sockets and file descriptors differ.  (Those are non-POSIX platforms;
 * a vanishing kind but still not completely abolished.)
 */

#ifndef _WIN32
#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <netinet/in.h>

#define socket_init() do { } while(0)
#define socket_fini() do { } while(0)
#define socket_close(s) close(s)

#else /* _WIN32 */

#include <winsock2.h>
/* https://stackoverflow.com/a/22735803/433626 */
#include <ws2tcpip.h>
/* https://stackoverflow.com/a/11924203/433626 */
typedef u_short sa_family_t;

#define socket_init() do { \
	WSADATA wsaData; \
	if (WSAStartup(MAKEWORD(2, 0), &wsaData) != 0) exit (1); \
	} while (0)
#define socket_fini() do { \
	if (WSACleanup() != 0) exit (1); \
	} while (0)
#define socket_close(s) closesocket(s)
#endif /* _WIN32 */



/** @brief Given a socket address, determine its length.
 *
 * This function does not fail.
 *
 * TODO:inline
 */
socklen_t socket_addrlen (const struct sockaddr_storage *ss);


/* Given a socket address, determine its port.  In case of a parsed
 * address, this may well be a 0 port for default or random allocation.
 *
 * This function does not fail.
 *
 * TODO:inline
 */
uint16_t socket_port (const struct sockaddr_storage *ss);


/** @brief Store a raw address from a given family in a socket address,
 * together with a port that may be set to 0 as a catch-all.
 */
bool socket_address (sa_family_t af, uint8_t *addr, uint16_t portnr, struct sockaddr_storage *ss);


/** @brief Parse an address and port, and store them in a sockaddr
 *
 * The sockaddr will be setup as type AF_INET or AF_INET6.  The
 * space provided is large enough to hold either, because the
 * operating system defines it as a union.
 *
 * The opt_port may be NULL, in which case the port is set to 0
 * in the returned sockaddr; otherwise, its value is rejected
 * if it is 0.
 *
 * We always try IPv6 address parsing first, but fallback to
 * IPv4 if we have to, but that fallback is deprecated.  The
 * port will be syntax-checked and range-checked.
 *
 * Return true on success, or false with errno set on error.
 */
bool socket_parse (char *addr, char *opt_port, struct sockaddr_storage *out_sa);


/** @brief Open a connection as a client, to the given address.
 *
 * Do not bind locally.
 *
 * Set contype to one SOCK_DGRAM, SOCK_STREAM or SOCK_SEQPACKET.
 *
 * The resulting socket is written to out_sox.
 *
 * Return true on success, or false with errno set on failure.
 */
bool socket_client (const struct sockaddr_storage *peer, int contype, int *out_sox);


/** @brief Open a listening socket as a server, at the given address.
 *
 * Set contype to one of SOCK_DGRAM, SOCK_STREAM or SOCK_SEQPACKET.
 *
 * The resulting socket is written to out_sox.
 *
 * Return true on success, or false with errno set on failure.
 */
bool socket_server (const struct sockaddr_storage *mine, int contype, int *out_sox);


/** @brief Switch a file descriptor to non-blocking mode.
 *
 * This is needed for most event-based systems.  Note that it is even
 * required on a socket that wants to accept() because of the chance
 * that a remote resets the attempt just when we are moving from the
 * signal received to our accept() attempt -- and we might then get
 * blocked.
 *
 * Return true on success.
 */
bool socket_nonblock (int sox);


#endif /* ARPA2COMMON_SOCKET_H */


/** @} */
