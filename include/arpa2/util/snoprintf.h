/** @addtogroup arpa2util ARPA2 Utility Collection
 * @{
 * @defgroup snoprintf Offset-passing variant of snprintf()
 * @{
 *
 * These functions are similar to POSIX functions snprintf() and vsnprintf(),
 * but they introduce an offset inside the buffer to print to.  The return
 * value is negative on failure, otherwise it is the size that is/would be
 * written if enough space is/were available.
 *
 * This design allows for straightforward chaning of snoprintf() and/or
 * vsnoprintf() operations, feeding the output back in as the new offset
 * value.  To facilitate such chaining with delayed detection of any
 * errors, it is permitted to provide a negate offset value, which will
 * simply be returned without further action.  To the same purpose, any
 * snoprintf() or vsnoprintf() calls that expand beyond an already overflown
 * buffer will simply continue to count characters that would be printed,
 * without further modifying the buffer.
 *
 * As a special guesture towards the user, any overflow leads to the
 * final 4 characters of the buffer being set to "..." which does imply
 * a requirement that the size always be at least 4.
 *
 * Designed by Henri Manson and Rick van Rein for ARPA2 Projects.
 *
 * SPDX-License-Identifier: BSD-2-Clause
 * SPDX-FileCopyrightText: Henri Manson <info@mansoft.nl>
 * SPDX-FileCopyrightText: Copyright 2020 Rick van Rein <rick@openfortress.nl>
*/

int vsnoprintf(char *buf, size_t size, ssize_t offset, const char *format, va_list ap);
int  snoprintf(char *buf, size_t size, ssize_t offset, const char *format, ...);

/** @} */

/** @} */
