/* editor.c -- Support routines for rule setup in LMDB records.
 *
 * This is mostly in support of LDAP attributes for ACL entries:
 *  - accessDomain -- a UTF-8 domain name serving as ACL scope
 *  - accessType   -- a UUID for a (well-known) usage pattern
 *  - accessName   -- a UTF-8 string formatted under accessType specs
 *  - accessRights -- a UTF-8 string of space-separated ACL words
 *
 * The Access Domain and Type are processed with the RuleDB functions
 * rules_dbkey_domain() and rules_dbkey_service().  This results
 * in an rules_dbkey under which Access Name and Rights are further
 * processed.
 *
 * The iteration and "inside-out" turning is proofed and described in
 * https://gitlab.com/arpa2/steamworks-pulleyback/-/tree/master/acl/pydemo
 * (for instance in commit 0100ee0009d068cfec6c82109d28ec0378e4d14a)
 *
 * SPDX-License-Identifier: BSD-2-Clause
 * SPDX-FileCopyrightText: Copyright 2020 Rick van Rein <rick@openfortress.nl>
 */


#include <stdint.h>
#include <stdbool.h>
#include <string.h>
#include <assert.h>

#include <lmdb.h>

#include "arpa2/except.h"
#include "arpa2/digest.h"

#include "arpa2/identity.h"
#include "arpa2/rules.h"
#include "arpa2/rules_db.h"


struct _ruleit {
	struct rules_request req;
	struct rules_db *ruledb;
	a2md_state midkey;
	changerules_what whattodo;
	char *copybuf;
	char *copyptr;
};


/* Find NUL or the current/next space.
 */
static char *_findspace (char *s) {
	while ((*s != '\0') && (*s != ' ')) {
		s++;
	}
	return s;
}


static bool _rules_selector (struct rules_request *self, a2sel_t const *selector) {
	bool ok = true;
	//
	// Upgrade the data structure to our own
	//  - rit->copybuf is the start of a large-enough output buffer
	//  - rit->copybuf may already hold words from a previous pass
	//  - rit->copyptr points at new words to copy
	struct _ruleit *rit = (struct _ruleit *) self;
	//
	// Be mindful of the need to add a space before adding a word
	char *copytmp = rit->copybuf;
	bool needspc = false;  // Generally, = (copytmp > rit->copybuf)
	//
	// Remove any "^callback" from a previous round
	while (*copytmp != '\0') {
		//
		// Find the next word or the trailing NUL
		char *copyond = _findspace (copytmp);
		if (*copyond == ' ') {
			needspc = false;
			copyond++;
		}
		//
		// Perhaps remove the word
		if ((*copytmp == '^') || (*copytmp == '%')) {
			/* erase "^callback" or "%rights" word */
			memmove (copytmp, copyond, strlen (copyond) + 1);
		} else {
			/* skip over any other word */
			copytmp = copyond;
		}
	}
	//
	// Extend the buffer with any new =keepvar, #comments, ^callback, '%rights'
	//  - copytmp is beyond prior words, has no trailing NUL
	//  - copytmp and rit->copybuf differ iff words have been added
	//  - rit->copyptr points at new words to copy
	while (*rit->copyptr != '\0') {
		//
		// Find the next word or the trailing NUL
		char *copyond = _findspace (rit->copyptr);
		//
		// Perhaps append the word
		if ((*rit->copyptr != '~') && (*rit->copyptr != '%')) {
			/* insert a space if not the first word */
			if (needspc) {
				*copytmp++ = ' ';
			}
			/* append the word */
			memmove (copytmp, rit->copyptr, copyond - rit->copyptr);
			copytmp += copyond - rit->copyptr;
			needspc = true;
		}
		//
		// Break out if the next word is a terminator
		if (*copyond == '\0') {
			rit->copyptr = copyond;
			break;
		}
		copyond++;
		if (*copyond == '~') {
			rit->copyptr = _findspace (copyond);
			break;
		}
		//
		// Move on to the next word
		rit->copyptr = copyond;
	}
	//
	// Append the current %FLAGS for temporary use
	if (self->flags_rawstr != NULL) {
		if (needspc) {
			*copytmp++ = ' ';
		}
		int copylen = _findspace ((char *) self->flags_rawstr) - self->flags_rawstr;
		memcpy (copytmp, self->flags_rawstr, copylen);
		copytmp += copylen;
	}
	//
	// Close the buffer at its current, temporary endpoint
	//  - So it holds words separated by one space
	*copytmp = '\0';
	//
	// Log the rule and its destination string
	log_debug ("Inside-out rule \"%s\" for selector %s", rit->copybuf, selector->txt);
	//
	// Clone the midkey to the endkey
	a2md_state endkey;
	ok = ok && a2md_clone (&endkey, &rit->midkey);
	bool mid2endkey = ok;
	//
	// Expand the endkey with the normalised selector
	a2md_write (&endkey, selector->txt, selector->ofs [A2ID_OFS_END]);
	rules_dbkey dbkey;
	if (mid2endkey) {
		bool got = a2md_close (&endkey, dbkey);
		ok = ok && got;
	}
	//
	// Load the old RuleDB entry; this may be an empty set
	MDB_val oldacl;
	bool gotdb = ok && rules_dbget (rit->ruledb, dbkey, &oldacl);
	ok = ok && gotdb;
	//
	// Construct the new RuleDB entry from the old
	MDB_val newacl;
	if (rit->whattodo == do_add) {
		newacl.mv_data =         rit->copybuf     ;
		newacl.mv_size = strlen (rit->copybuf) + 1;
	} else /* rit->changerules_what == do_del */ {
		char *rule;
		bool notfound = true;
		if (ok && rules_dbloop (&oldacl, &rule)) do {
			notfound = (0 != strcmp (rule, rit->copybuf));
		} while (notfound && rules_dbnext (&oldacl, &rule));
		if (notfound) {
			ok = false;
		} else {
			newacl.mv_data = rule + strlen (rule) + 1;
			newacl.mv_size = ((char *) oldacl.mv_data) + oldacl.mv_size - ((char *) newacl.mv_data);
			oldacl.mv_size = rule - ((char *) oldacl.mv_data);
		}
	}
	//
	// Write the new RuleDB entry on top of the remains of the old
	ok = ok && rules_dbset (rit->ruledb, &oldacl, &newacl);
	//
	// Reset copybuf when the rule ends here
	if (*rit->copyptr == '\0') {
		*rit->copybuf = '\0';
	}
	//
	// Forward the copyptr beyond NUL or the space after '~'
	rit->copyptr++;
	//
	// Return success or failure
	return ok;
}


/* Generic handling for ruleset addition and deletion.
 *
 * When an opt_selector is provided, the parser forbids "~selector" words.
 * Instead, this one opt_selector is then lardered with the rule.
 *
 * When an opt_nested RuleDB is provided, its read/write transaction
 * is used instead of opening a local one.  This is like running a
 * nested transaction inside this function, and using it to compose
 * a larger one with multiple of these transactions.  The return
 * value indicates success of the internal "transaction" and when
 * false is returned, the opt_nested RuleDB should (eventually) be
 * rolled back too.
 *
 * Some normalisation may be done on the rule, but it is the same for
 * addition and deletion, so that will cancel out.
 */
bool rules_edit_generic (rules_dbkey prekey, unsigned prekeylen,
			char *xskey,
			char *rules, unsigned ruleslen,
			changerules_what whattodo,
			a2sel_t *opt_selector, struct rules_db *opt_nested) {
	bool ok = true;
	//
	// Have an iterator data structure
	struct _ruleit rit;
	memset (&rit, 0, sizeof (rit));
	//
	// Have a copy buffer, maxsized to fit the input
	char copybuf [ruleslen + 10];
	memset (copybuf, 0, sizeof (copybuf));
	rit.copybuf = copybuf;
	//
	// Set copyptr to the first rule; callbacks will advance it
	rit.copyptr = rules;
	//
	// Setup for rule add or delete
	rit.whattodo = whattodo;
	//
	// Setup callback functions (we only need "~selector" callbacks)
	bool permit_selector = (opt_selector == NULL);
	rit.req.optcb_flags = NULL;	/* Will copy the data... */
	rit.req.optcb_trigger = NULL;	/* ...and this data too! */
	rit.req.optcb_selector = permit_selector ? _rules_selector : NULL;
	//TODO// rit.req.optcb_endrule = permit_selector ? NULL : _rules_selector;
	rit.req.optcb_endruleset = NULL;
	//
	// Start digesting the prekey, and later fork its output state
	ok = ok && a2md_open (&rit.midkey);
	bool pre2midkey = ok;
	ok = ok && a2md_write_msg (&rit.midkey, prekey, prekeylen);
	//
	// Extend the hash state with the Access Name
	ok = ok && a2md_write_msg (&rit.midkey, xskey, strlen (xskey));
	//
	// Open the database in read/write mode
	bool gotdb = false;
	struct rules_db ruledb;
	if (opt_nested != NULL) {
		rit.ruledb = opt_nested;
	} else {
		memset (&ruledb, 0, sizeof (ruledb));
		rit.ruledb = &ruledb;
		bool gotdb = rules_dbopen (&ruledb, false, RULES_TRUNK_ANY);
		ok = ok && gotdb;
	}
	//
	// Parse the LDAP rules to produce selectors
	ok = ok && rules_process (&rit.req, rules, ruleslen, permit_selector);
	//
	// Commit our transaction if ok, then close the database
	if (opt_nested != NULL) {
		; /* caller will rollback or commit */
	} else if (ok) {
		ok = rules_dbcommit (rit.ruledb);
	} else if (gotdb) {
		rules_dbclose (rit.ruledb);
	}
	//
	// Cleanup the middle key
	if (pre2midkey) {
		a2md_close (&rit.midkey, NULL);
	}
	//
	// Return whether the transaction succeeded
	return ok;
}

