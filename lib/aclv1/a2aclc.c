/*
 * Copyright (c) 2018 Tim Kuijsten
 *
 * Permission to use, copy, modify, and/or distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES WITH
 * REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY
 * AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT,
 * INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM
 * LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR
 * OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
 * PERFORMANCE OF THIS SOFTWARE.
 */

/*
 * ARPA2 Communication ACL library
 *
 * Retreive, validate and modify access policies.
 */

#include <sys/stat.h>

#include <assert.h>
#include <ctype.h>
#include <errno.h>
#include <fcntl.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>

#include "a2aclc.h"

static const char basechar[256] = {
	0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
	    0, 0, 0, 0, 0, 0, 0, 0, 0,
	1, /* ! */
	1, /* " */
	1, /* # */
	1, /* $ */
	1, /* % */
	1, /* & */
	1, /* ' */
	1, /* ( */
	1, /* ) */
	1, /* * */
	0, /* "+" PLUS is special */
	1, /* , */
	1, /* - */
	0, /* "." DOT is special */
	1, /* / */
	1, 1, 1, 1, 1, 1, 1, 1, 1, 1, /* 0-9 */
	1, /* : */
	1, /* ; */
	1, /* < */
	1, /* = */
	1, /* > */
	1, /* ? */
	0, /* "@" AT is special */
	1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
	    1, 1, /* A-Z */
	1, /* [ */
	1, /* \ */
	1, /* ] */
	1, /* ^ */
	1, /* _ */
	1, /* ` */
	1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
	    1, 1, /* a-z */
	1, /* { */
	1, /* | */
	1, /* } */
	1  /* ~ */
	/* let rest of static array initialize to 0 */
};

/*
 * aclc rule segment iterator storage, contains internel state for
 * a2aclc_nextsegment(3).
 */
struct a2aclcit {
	enum states { S, SETLIST, LIST, WILDCARD, REQSIGFLAGS, SEGMENTNAME,
	    SUBSEGMENT, POSTSEGMENT, E } state;
	const char *aclcrule;
	size_t aclcrulesize, n;
	int initialized;	/* magic */
};

struct a2aclcseg {
	const char *seg;
	size_t segsize;
	int reqsigflags;
};

/* semi-public function of upcoming libarpa2id API */
size_t a2id_localpart_options(char *dst, size_t dstsize, int *nropts,
    const a2id *a2id);

/*
 * Allocate and initialize a new aclc rule segment iterator, to be used by
 * a2aclc_nextsegment(3).
 *
 * On successful return the new structure should be free(3)d by the caller.
 *
 * Return a newly initialized structure on success or NULL on error with errno
 * set.
 */
struct a2aclcit *
a2aclc_newit(const char *aclcrule, size_t aclcrulesize)
{
	struct a2aclcit *it;

	if ((it = calloc(1, sizeof(*it))) == NULL)
		return NULL;

	it->initialized = 42;
	it->state = S;
	it->aclcrule = aclcrule;
	it->aclcrulesize = aclcrulesize;
	it->n = 0;

	return it;
}

/*
 * Check if "id" matches with "aclcseg".
 *
 * Signature presence is tested if required, but not validated.
 *
 * Return 1 if true, 0 if false.
 */
int
a2aclc_aclcsegmatch(const a2id *id, const struct a2aclcseg *aclcseg)
{
	char idoptseg[A2ID_MAXLOCALPART_OPTIONSSZ];
	size_t idoptsegsize;

	if (id == NULL || aclcseg == NULL)
		return 0;

	/* Handle signature presence requirements. */
	if (aclcseg->reqsigflags)
		if (a2id_hassignature(id) == 0)
			return 0;

	/* Handle wildcard aclc */
	if (aclcseg->segsize == 0)
		return 1;

	idoptsegsize = a2id_localpart_options(idoptseg, sizeof(idoptseg), NULL,
	    id);
	if (idoptsegsize == 0)
		return 0;

	/* assume no leading '+' in segments */
	assert(aclcseg->seg[0] != '+');
	assert(idoptseg[0] != '+');

	if (aclcseg->segsize > idoptsegsize)
		return 0;

	if (strncmp(aclcseg->seg, idoptseg, aclcseg->segsize) != 0)
		return 0;

	if (aclcseg->segsize == idoptsegsize)
		return 1;

	if (idoptseg[aclcseg->segsize] == ' ' ||
	    idoptseg[aclcseg->segsize] == '+')
		return 1;

	return 0;
}

/*
 * Parse an aclc rule. Returns whenever a new segment is parsed or the end of the
 * aclc rule is reached. "aclcit" should be created once with a2aclc_newit(3) and
 * used with this function repeatedly until 0 or -1 is returned.
 *
 * Returns 1 if "list", "aclcseg" are set to a new segment. 0 if there are no
 * more segments and -1 on error (illegal syntax or list-types etc.).
 */
int
a2aclc_nextsegment(char *list, struct a2aclcseg *aclcseg, struct a2aclcit *aclcit)
{
	unsigned char c, lookahead;

	if (list == NULL || aclcseg == NULL || aclcit == NULL)
		return -1;

	if (aclcit->initialized != 42 || aclcit->aclcrule == NULL)
		return -1; /* not initialized with a2aclc_newit(3) */

	aclcseg->reqsigflags = 0;
	aclcseg->segsize = 0;
	aclcseg->seg = NULL;

	for (; aclcit->n < aclcit->aclcrulesize; aclcit->n++) {
		c = aclcit->aclcrule[aclcit->n];

		switch (aclcit->state) {
		case S:
			if (isblank(c)) {
				/* keep going */
			} else if (c == '%') {
				aclcit->state = SETLIST;
			} else
				goto done;
			break;
		case SETLIST:
			switch (c) {
			case 'W':
				/* FALLTHROUGH */
			case 'G':
				/* FALLTHROUGH */
			case 'B':
				/* FALLTHROUGH */
			case 'A':
				aclcit->state = LIST;
				*list = c;
				break;
			default:
				goto done;
			}
			break;
		case LIST:
			if (isblank(c)) {
				/* keep going */
			} else if (c == '+') {
				aclcit->state = WILDCARD;
			} else
				goto done;
			break;
		case WILDCARD:
			aclcseg->seg = &aclcit->aclcrule[aclcit->n];
			if (isblank(c)) {
				aclcit->state = POSTSEGMENT;
				return 1;
			} else if (c == '+') {
				aclcseg->reqsigflags = 1;
				aclcit->state = REQSIGFLAGS;
			} else if (basechar[c] || c == '.') {
				aclcseg->segsize++;
				aclcit->state = SEGMENTNAME;
			} else
				goto done;
			break;
		case SEGMENTNAME:
			if (basechar[c] || c == '.') {
				aclcseg->segsize++;
			} else if (isblank(c)) {
				aclcit->state = POSTSEGMENT;
				return 1;
			} else if (c == '+') {
				/* look-ahead for SUBSEGMENT or REQSIGFLAGS */
				if (aclcit->n + 1 < aclcit->aclcrulesize) {
					lookahead = aclcit->aclcrule[aclcit->n + 1];
				} else
					lookahead = '\0';

				if (basechar[lookahead] || lookahead == '.') {
					aclcseg->segsize++;
					aclcit->state = SUBSEGMENT;
				} else if (lookahead == '\0' ||
				    isblank(lookahead)) {
					/* REQSIGFLAGS */
					aclcseg->reqsigflags = 1;
					aclcit->state = REQSIGFLAGS;
				} else {
					goto done;
				}
			} else
				goto done;
			break;
		case SUBSEGMENT:
			if (basechar[c] || c == '.') {
				aclcseg->segsize++;
				aclcit->state = SEGMENTNAME;
			} else
				goto done;
			break;
		case POSTSEGMENT:
			if (isblank(c)) {
				/* keep going */
			} else if (c == '+') {
				aclcit->state = WILDCARD;
			} else if (c == '%') {
				aclcit->state = SETLIST;
			} else
				goto done;
			break;
		case REQSIGFLAGS:
			if (isblank(c)) {
				aclcit->state = POSTSEGMENT;
				return 1;
			} else
				goto done;
			break;
		default:
			abort();
		}
	}

done:
	/* Is all input processed? */
	if (aclcit->n != aclcit->aclcrulesize)
		return -1;

	/* Are we in one of the final states? */
	if (aclcit->state != WILDCARD &&
	    aclcit->state != REQSIGFLAGS &&
	    aclcit->state != POSTSEGMENT &&
	    aclcit->state != SEGMENTNAME &&
	    aclcit->state != E)
		return -1;

	/* Is there a segment left? */
	if (aclcit->state == WILDCARD ||
	    aclcit->state == SEGMENTNAME ||
	    aclcit->state == REQSIGFLAGS) {
		aclcit->state = E;
		return 1;
	}

	return 0;
}

/*
 * Determine if communication between "remoteid" and "localid" is whitelisted,
 * greylisted, blacklisted or abandoned.
 *
 * The result is written to "list" in the form of the first letter of the list
 * this pair is on which is one of: 'W', 'G', 'B', 'A'. If no policy is found it
 * is set to 'G'.
 *
 * "remoteid" will be generalized until an aclc rule is found or until it equals
 * the most general selector "@." which can not be further generalized.
 *
 * Returns 0 on success and updates "*list" to point to the applicable list-
 * character which is either a 'W', 'G', 'B', or 'A'. Returns -1 on error.
 *
 * XXX returns -1 if an aclc rule is syntactically incorrect, these checks should
 * better be done on import.
 */
int
a2aclc_whichlist(char *list, a2id *remoteid, const a2id *localid)
{
	struct a2aclcseg aclcseg;
	struct a2aclcit *it;
	char aclcrule[A2ACLC_MAXLEN], coreid[A2ID_MAXSZ], remotestr[A2ID_MAXSZ];
	size_t aclcrulesize, remotestrsz, coreidsz;
	int match, r;

	coreidsz = a2id_coreform(coreid, sizeof(coreid), localid);
	if (coreidsz >= sizeof(coreid))
		return -1;

	for (;;) {
		remotestrsz = a2id_tostr(remotestr, sizeof(remotestr), remoteid);
		if (remotestrsz >= sizeof(remotestr))
			return -1;

		aclcrulesize = sizeof(aclcrule);
		if (a2aclc_getaclcrule(aclcrule, &aclcrulesize, remotestr,
		    remotestrsz, coreid, coreidsz) == -1)
			return -1;

		if (aclcrulesize == 0) {
			if (a2id_generalize(remoteid))
				continue;
			else
				break;
		}

		if ((it = a2aclc_newit(aclcrule, aclcrulesize)) == NULL)
			return -1;

		/* iterate over aclc segments and see if there is a match */
		match = 0;
		while ((r = a2aclc_nextsegment(list, &aclcseg, it)) == 1) {
			if (a2aclc_aclcsegmatch(localid, &aclcseg)) {
				match = 1;
				break;
			}
		}

		free(it);
		it = NULL;

		if (r == -1)
			return -1;

		if (match)
			return 0;

		if (a2id_generalize(remoteid) != 1)
			break;
	}

	/* default policy */
	*list = 'G';
	return 0;
}

/*
 * Parse an aclc policy line consisting of a remote selector, a local id and an
 * aclc rule. The IDs and aclc rule are only parsed loosely and "line" must have
 * the following form:
 *
 *    policyline = *WSP remotesel 1*WSP localid 1*WSP aclcrule
 *    remotesel  = 2*graph
 *    localid    = 3*graph
 *    aclcrule    = 3*print
 *    graph      = %x21-7e
 *    print      = %x20-7e
 *
 * Return 0 on success and set "remotesel", "localid" and "aclcrule" to point to
 * the start of the respective part in "line". Sizes are updated accordingly and
 * "err" is set to NULL.
 * Return -1 on error. If there was a syntax error in "line" then "err" is set
 * to point to the first erroneous character in "line". If there was another
 * error then "err" is set to NULL and errno is set.
 */
int
a2aclc_parsepolicyline(const char **remotesel, size_t *remoteselsize,
    const char **localid, size_t *localidsize, const char **aclcrule,
    size_t *aclcrulesize, const char *line, size_t linesize, const char **err)
{
	const size_t minrulelen = sizeof("@. a@b %B+") - 1;
	size_t n;

	*err = NULL;
	n = 0;

	if (remotesel == NULL || remoteselsize == NULL || localid == NULL ||
	    localidsize == NULL || aclcrule == NULL || aclcrulesize == NULL ||
	    line == NULL || linesize == 0 || err == NULL) {
		errno = EINVAL;
		return -1;
	}

	if (linesize < minrulelen) {
		*err = &line[0];
		return -1;
	}

	/*
	 * Parse remote selector.
	 */

	while (n < linesize && isblank(line[n]))
		n++;
	if (n == linesize) {
		*err = &line[n - 1];
		return -1;
	}

	*remotesel = &line[n];

	while (n < linesize && isgraph(line[n]))
		n++;
	if (n == linesize) {
		*err = &line[n - 1];
		return -1;
	}

	if (!isblank(line[n])) {
		*err = &line[n];
		return -1;
	}

	*remoteselsize = &line[n] - *remotesel;
	if (*remoteselsize < 2) {
		*err = &line[n];
		return -1;
	}

	/*
	 * Parse local id.
	 */

	while (n < linesize && isblank(line[n]))
		n++;
	if (n == linesize) {
		*err = &line[n - 1];
		return -1;
	}

	*localid = &line[n];

	while (n < linesize && isgraph(line[n]))
		n++;
	if (n == linesize) {
		*err = &line[n - 1];
		return -1;
	}

	if (!isblank(line[n])) {
		*err = &line[n];
		return -1;
	}

	*localidsize = &line[n] - *localid;
	if (*localidsize < 3) {
		*err = &line[n];
		return -1;
	}

	/*
	 * Parse aclc rule until end of line.
	 */

	while (n < linesize && isblank(line[n]))
		n++;
	if (n == linesize) {
		*err = &line[n - 1];
		return -1;
	}

	*aclcrule = &line[n];

	while (n < linesize && isprint(line[n]))
		n++;

	if (n != linesize) {
		assert(n < linesize);
		*err = &line[n];
		return -1;
	}

	*aclcrulesize = (&line[n - 1] - *aclcrule) + 1;
	if (*aclcrulesize < 3) {
		*err = &line[n - 1];
		return -1;
	}

	return 0;
}

/*
 * Import an aclc policy from a readable descriptor yielding aclc rules.
 *
 * Each line in the file must be of the format "remotesel localid aclcrule".
 * Extraneous blanks are ignored.
 *
 * Returns the number of imported aclc rules on success or -1 on error with errno
 * set. If "errstr" is passed and -1 is returned a descriptive error is set in
 * "errstr", nul terminated and at most "errstrsize" bytes, including the
 * terminating nul.
 */
ssize_t
a2aclc_fromdes(int d, char *errstr, size_t errstrsize)
{
	const ssize_t minrulelen = sizeof("@. a@b %B+") - 1;
	const char *remotesel, *localid, *aclcrule, *err;
	char *line;
	ssize_t i, n;
	size_t remoteselsize, localidsize, aclcrulesize, s;
	FILE *fp;

	if (errstrsize)
		errstr[0] = '\0';

	if ((fp = fdopen(d, "re")) == NULL)
		return -1; /* errno set */

	i = 0;
	line = NULL;

	while ((n = getline(&line, &s, fp)) > 0) {
		i++;
		if (n < minrulelen) {
			if (errstrsize)
				snprintf(errstr, errstrsize, "illegal aclc rule "
				    "at line %zu: %s", i, line);
			errno = EINVAL;
			free(line);
			return -1;
		}

		if (line[n - 1] == '\n') {
			line[n - 1] = '\0';
			n--;
		}

		if (a2aclc_parsepolicyline((const char **)&remotesel,
		    &remoteselsize, (const char **)&localid, &localidsize,
		    (const char **)&aclcrule, &aclcrulesize, line, n,
		    (const char **)&err) == -1) {
			if (err) {
				if (errstrsize)
					snprintf(errstr, errstrsize, "illegal "
					    "aclc policy line at #%zu,%lu: %s",
					    i, err - line, line);
				errno = EINVAL;
				free(line);
				return -1;
			} else {
				free(line);
				return -1; /* errno is set */
			}
		}

		if (a2aclc_putaclcrule(aclcrule, aclcrulesize, remotesel,
		    remoteselsize, localid, localidsize) == -1) {
			if (errstr && errstrsize)
				snprintf(errstr, errstrsize, "failed to save "
				    "aclc rule #%zu: %s", i, line);
			errno = EINVAL;
			free(line);
			return -1;
		}
	}

	free(line);
	if (ferror(fp)) {
		fprintf(stderr, "%s: error while reading aclc file\n", __func__);
		exit(1);
	}

	return i;
}

/*
 * Check if "subject" is newer than "reference" when looking at the last
 * modification times.
 *
 * Return 1 if "subject" is newer than "reference" or if "reference" does not
 * exist. Return 0 if subject is older or equal to "reference". Return -1 if an
 * error occurred with errno set (i.e. subject does not exist).
 */
int
a2aclc_isnewer(const char *subject, const char *reference)
{
	struct stat rst, sst;
	int r, refd, subd;

	if (subject == NULL || reference == NULL)
		return -1;

	if ((subd = open(subject, O_RDONLY|O_CLOEXEC)) == -1)
		return -1;

	if ((refd = open(reference, O_RDONLY|O_CLOEXEC)) == -1)
		return 1;

	r = 0;

	/* compare mtimes */
	if (fstat(refd, &rst) == -1)
		goto out; /* errno set */

	if (fstat(subd, &sst) == -1)
		goto out; /* errno set */

	if (rst.st_mtime < sst.st_mtime)
		r = 1;

out:
	close(refd);
	close(subd);

	return r;
}

/*
 * Import an aclc policy from a text file specified by "filename" into an
 * internal database cache. If a database cache file does not exist, it is
 * created and if the cache is stale it is automatically recreated. The
 * currently supported database backends are "dbm" and "dblmdb" of which the
 * first is a simple memory based key-value store, and the latter is using LMDB.
 *
 * Each line in "filename" must consist of exactly one aclc rule, which is a
 * triplet of the form: <remote selector, local ID, aclc segments>
 *
 * Where remote selector is an ARPA2 ID Selector, local ID is an ARPA2 ID in
 * core form and aclc segments are one or more aclc segments. Extraneous blanks
 * are ignored.
 *
 * If "totrules" is not NULL, it will be updated with the total number of rules
 * in the database. If "updrules" is not NULL it will be updated with the number
 * of newly imported rules by this call.
 *
 * If there is an error and "errstr" is not NULL, then "errstr" is updated with
 * a descriptive error of at most "errstrsize" bytes, including the terminating
 * nul.
 *
 * Returns 0 on success or -1 on error with errno set.
 */
int
a2aclc_fromfile(const char *filename, size_t *totrules, size_t *updrules,
    char *errstr, size_t errstrsize)
{
	char dbcache[104];
	size_t s;
	int fd, r, recreate;

	if (errstrsize)
		errstr[0] = '\0';

	if (filename == NULL) {
		errno = EINVAL;
		return -1;
	}

	s = strlen(filename);
	if (s == 0 || sizeof(dbcache) - 4 < s) {
		errno = EINVAL;
		return -1;
	}

	r = snprintf(dbcache, sizeof(dbcache), "%.*s.db", (int)s,
	    filename);
	if (r <= 0 || sizeof(dbcache) <= (size_t)r) {
		errno = EINVAL;
		return -1;
	}

	/* remove stale db caches before opening/creating */
        recreate = a2aclc_isnewer(filename, dbcache);
        if (recreate == -1)
		return -1;

        if (recreate != 0)
		if (unlink(dbcache) == -1 && errno != ENOENT)
			return -1; /* errno set */

	if (a2aclc_dbopen(dbcache) == -1) {
		if (errstrsize)
			snprintf(errstr, errstrsize, "error opening database,"
			    " param: %s\n", dbcache);
		errno = EINVAL;
		return -1;
	}

	if (updrules)
		*updrules = 0;

        if (recreate) {
		if ((fd = open(filename, O_RDONLY|O_CLOEXEC)) == -1) {
			a2aclc_dbclose();
			return -1; /* errno set */
		}

		if ((r = a2aclc_fromdes(fd, errstr, errstrsize)) < 0) {
			a2aclc_dbclose();
			close(fd);
			errno = EINVAL;
			unlink(dbcache);
			return -1;
		}

		close(fd);
		if (updrules)
			*updrules = r;
	}


	if (totrules) {
		if (a2aclc_count(totrules) == -1) {
			errno = EINVAL;
			unlink(dbcache);
			return -1;
		}
	}

	return 0;
}

/*
 * This is a simple implementation, relying on the assumption that
 * precisely one level of hostname precedes the domain name which
 * serves as the realm.  Note that this may be softened in later
 * versions, probably by introduction of a database that maps a
 * hostname to its realm (which must then also be trailing the
 * hostname).
 */
const char *
a2aclc_hostname2realm (const char *fqdn_hostname) {
	const char *ptr = strchr (fqdn_hostname, '.');
	if (ptr == NULL) {
		return NULL;
	}
	ptr++;
	return ptr;
}
