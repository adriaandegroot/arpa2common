/*
 * Copyright (c) 2018 Tim Kuijsten
 *
 * Permission to use, copy, modify, and/or distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES WITH
 * REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY
 * AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT,
 * INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM
 * LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR
 * OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
 * PERFORMANCE OF THIS SOFTWARE.
 */

#ifndef A2ACLC_H
#define A2ACLC_H

#include <sys/types.h>

#include "a2id.h"

#define A2ACLC_MAXLEN 500

int a2aclc_whichlist(char *, a2id *, const a2id *);
int a2aclc_fromfile(const char *, size_t *, size_t *, char *, size_t);

/*
 * When implementing a new database backend like "dbmem" and "dblmdb", the
 * following five functions must be implemented:
 *    a2aclc_dbopen: Initialize a database backend.
 *
 *    a2aclc_dbclose: Close a database backend.
 *
 *    a2aclc_count: Update "count" to the total number of rules in the database.
 *
 *    a2aclc_putaclcrule: Store a communication aclc rule given a remote and local
 * 	ID. A copy of "aclcrule", "remotesel" and "localid" must be made since
 *	these are being free(3)d after this functions returns.
 *
 *    a2aclc_getaclcrule: Search for a communication aclc rule based on a remote
 *	selector and local ID. "aclcrule" must be allocated by the caller.
 *	"aclcrulesize" is a value/result parameter. In case no aclc rule is found
 *	then "aclcrule" is left untouched, "aclcrulesize" is set to 0 and 0 is
 *	returned.
 *
 * All five functions must return 0 on success, and -1 on failure.
 */

int a2aclc_dbopen(const char *path);
int a2aclc_dbclose(void);
int a2aclc_count(size_t *count);
int a2aclc_putaclcrule(const char *aclcrule, size_t aclcrulesize,
    const char *remotesel, size_t remoteselsize, const char *localid,
    size_t localidsize);
int a2aclc_getaclcrule(char *aclcrule, size_t *aclcrulesize, const char *remotesel,
    size_t remoteselsize, const char *localid, size_t localidsize);
int a2aclc_printdb(int d);

/*
 * Lookup the realm for a fully qualified host name.  The realm is a trailer
 * of the hostname.  The return value is NULL if no realm could be found for
 * the supplied host name.
 *
 * This function is used by protocols that communicate host names instead of
 * domain names.  A good example is HTTP.  On such systems, aliases must not
 * interfere with the specified host names; the best chance for that is to
 * not lookup the client-sent host name, but the configured canonical host
 * name (web servers tend to have a canonical ServerName and additionally
 * allow ServerAlias names that are also recognised as Host: header).
 */
const char * a2aclc_hostname2realm (const char *fqdn_hostname);

#endif /* A2ACLC_H */
