/*
 * Copyright (c) 2018 Tim Kuijsten
 *
 * Permission to use, copy, modify, and/or distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES WITH
 * REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY
 * AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT,
 * INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM
 * LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR
 * OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
 * PERFORMANCE OF THIS SOFTWARE.
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define MIN(x,y) ((x) < (y) ? (x) : (y))

/*
 * Extremely simple memory-only database implementation for ARPA2 Communication
 * ACL.
 *
 * Time complexity:
 *   find: O(n)
 *   insert/delete: O(n)
 *
 * Space complexity:
 *   O(n)
 */

struct dbmem_entry {
	void *remotesel;
	size_t remoteselsize;
	void *localid;
	size_t localidsize;
	void *aclcrule;
	size_t aclcrulesize;
};

static struct dbmem_entry **list = NULL;
static size_t listsize = 0;

static void
dbmem_printentry(int d, const struct dbmem_entry *ep)
{
	dprintf(d, "remotesel: %zu %.*s\nlocalid: %zu %.*s\naclcrule: %zu %.*s"
	    "\n",
	    ep->remoteselsize,
	    (int)ep->remoteselsize,
	    (char*)ep->remotesel,
	    ep->localidsize,
	    (int)ep->localidsize,
	    (char*)ep->localid,
	    ep->aclcrulesize,
	    (int)ep->aclcrulesize,
	    (char*)ep->aclcrule);
}

/*
 * Free dbmem_entry structure.
 */
static void
dbmem_free(struct dbmem_entry *ep)
{
	if (ep == NULL)
		return;

	free(ep->aclcrule);
	ep->aclcrule = NULL;

	free(ep->remotesel);
	ep->remotesel = NULL;

	free(ep->localid);
	ep->localid = NULL;

	free(ep);
	ep = NULL;
}

/*
 * Allocate a new dbmem_entry structure.
 *
 * Return a newly allocated structure on success that should be freed with
 * dbmem_free by the caller when done. Return NULL on error with errno set.
 */
static struct dbmem_entry *
dbmem_alloc(const void *aclcrule, size_t aclcrulesize, const void *remotesel,
    size_t remoteselsize, const void *localid, size_t localidsize)
{
	struct dbmem_entry *ep = NULL;

	if ((ep = calloc(1, sizeof(*ep))) == NULL)
		goto err; /* errno set */

	if ((ep->aclcrule = malloc(aclcrulesize)) == NULL)
		goto err; /* errno set */

	if ((ep->remotesel = malloc(remoteselsize)) == NULL)
		goto err; /* errno set */

	if ((ep->localid = malloc(localidsize)) == NULL)
		goto err; /* errno set */

	memcpy(ep->aclcrule, aclcrule, aclcrulesize);
	memcpy(ep->remotesel, remotesel, remoteselsize);
	memcpy(ep->localid, localid, localidsize);

	ep->aclcrulesize = aclcrulesize;
	ep->remoteselsize = remoteselsize;
	ep->localidsize = localidsize;

	return ep;

err:
	dbmem_free(ep);
	return NULL;
}

/*
 * Initialize a database backend.
 *
 * Must return 0 on success, -1 on failure.
 */
int
a2aclc_dbopen(const char *path)
{
	/* silence compiler */
	path = NULL;

	if (list != NULL)
		return -1;

	return 0;
}

/*
 * Close a database backend by purging everything from memory.
 *
 * Must return 0 on success, -1 on failure.
 */
int
a2aclc_dbclose(void)
{
	if (list == NULL)
		return 0;

	while (listsize-- > 0) {
		dbmem_free(list[listsize]);
		list[listsize] = NULL;
	}

	if (listsize > 0)
		return -1;

	free(list);
	list = NULL;

	return 0;
}

/*
 * Update "count" to the total number of rules in the database.
 *
 * Return 0 on success, -1 on failure.
 */
int
a2aclc_count(size_t *count)
{
	*count = listsize;
	return 0;
}

/*
 * Store a communication aclc rule given a remote and local ID. A copy of
 * "aclcrule", "remotesel" and "localid" must be made since these are being
 * free(3)d after this functions returns.
 *
 * Must return 0 on success, -1 on failure.
 */
int
a2aclc_putaclcrule(const char *aclcrule, size_t aclcrulesize, const char *remotesel,
    size_t remoteselsize, const char *localid, size_t localidsize)
{
	struct dbmem_entry *ep;

	if (aclcrule == NULL || aclcrulesize == 0 || remotesel == NULL ||
	    remoteselsize == 0 || localid == NULL || localidsize == 0)
		return -1;

	if ((listsize * sizeof(ep)) > ((listsize + 1) * sizeof(ep)))
		return -1; /* overflow */

	if ((ep = dbmem_alloc(aclcrule, aclcrulesize, remotesel, remoteselsize, localid,
	    localidsize)) == NULL)
		return -1;

	if ((list = realloc(list, (listsize + 1) * sizeof(ep))) == NULL) {
		dbmem_free(ep);
		ep = NULL;
		return -1;
	}

	list[listsize] = ep;
	listsize++;

	return 0;
}

/*
 * Search for a communication aclc rule based on a remote selector and local ID.
 *
 * "aclcrule" must be allocated by the caller. "aclcrulesize" is a value/result
 * parameter. If no aclc rule is found then "aclcrule" is left untouched and
 * "aclcrulesize" is set to 0.
 *
 * Must return 0 on success, -1 on error. If no "aclcrule" is found, 0 is
 * returned and *aclcrulesize is set to 0.
 */
int
a2aclc_getaclcrule(char *aclcrule, size_t *aclcrulesize, const char *remotesel,
    size_t remoteselsize, const char *localid, size_t localidsize)
{
	size_t i;

	if (aclcrule == NULL || aclcrulesize == NULL || remotesel == NULL ||
	    remoteselsize == 0 || localid == NULL || localidsize == 0)
		return -1;

	for (i = 0; i < listsize; i++) {
		if (memcmp(list[i]->remotesel, remotesel, MIN(list[i]->remoteselsize,
		    remoteselsize)) != 0)
			continue;

		if (memcmp(list[i]->localid, localid, MIN(list[i]->localidsize,
		    localidsize)) != 0)
			continue;

		if (list[i]->aclcrulesize > *aclcrulesize)
			return -1;

		memcpy(aclcrule, list[i]->aclcrule, list[i]->aclcrulesize);
		*aclcrulesize = list[i]->aclcrulesize;
		return 0;
	}

	*aclcrulesize = 0;
	return 0;
}

int
a2aclc_printdb(int d)
{
	size_t i;

	for (i = 0; i < listsize; i++)
		dbmem_printentry(d, list[i]);

	return 0;
}
