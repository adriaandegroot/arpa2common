/*
 * Copyright (c) 2019 Tim Kuijsten
 *
 * Permission to use, copy, modify, and/or distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES WITH
 * REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY
 * AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT,
 * INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM
 * LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR
 * OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
 * PERFORMANCE OF THIS SOFTWARE.
 */

/*
 * ARPA2 RESOURCE ACL library
 *
 * Retreive, validate and modify resource access right policies.
 */

#include <sys/stat.h>

#include <ctype.h>
#include <errno.h>
#include <fcntl.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>

#include "a2id.h"
#include "a2aclr.h"

#define RESOURCELEN 36

/*
 * Determine the rights that "id" within "realm" has over "resource" and
 * optionally "instance".
 *
 * The result is written to "rights".
 *
 * Returns 0 if a policy was found and the applicable string has been written
 * to "rights". In case no rights could be found for this combination of
 * "realm", "id", "resource" and "instance", 0 is returned and rightssize is set
 * to 0.
 * Returns -1 on error.
 */
int
a2aclr_whichrights(void *ctx, char *rights, size_t *rightssize, const char *realm,
    size_t realmsize, const char *id, size_t idsize, const char *resource,
    size_t resourcesize, const char *instance, size_t instancesize)
{
	a2id a2id;
	char idstr[A2ID_MAXSZ];
	size_t size, idstrsize;

	if (ctx == NULL || rights == NULL || rightssize == NULL ||
	    realm == NULL || id == NULL || resource == NULL)
		return -1;

	if (*rightssize == 0)
		return -1;

	if (a2id_fromstr(&a2id, id, idsize, 0) == -1)
		return -1;

	for (;;) {
		idstrsize = a2id_tostr(idstr, sizeof(idstr), &a2id);
		if (idstrsize >= sizeof(idstr))
			return -1;

		size = *rightssize;
		if (a2aclr_getrights(ctx, rights, &size, realm, realmsize, idstr,
		    idstrsize, resource, resourcesize, instance, instancesize)
		    == 0) {
			if (size > 0) {
				*rightssize = size;
				return 0;
			}
		}

		if (a2id_generalize(&a2id) == 0) {
			/* no match */
			*rightssize = 0;
			return 0;
		}
	}
}

/*
 * Determine if "id" within "realm" has all "reqrights" over "resource" and
 * "instance".
 *
 * Returns 0 if one or more rights in "rights" are not permitted or if
 * "reqrights" is empty.
 * Returns >0 otherwise.
 */
int
a2aclr_hasrights(void *ctx, const char *reqrights, size_t reqrightssize,
    const char *realm, size_t realmsize, const char *id, size_t idsize,
    const char *resource, size_t resourcesize, const char *instance,
    size_t instancesize)
{
	char rights[MAXRIGHTSSIZE];
	size_t i, rightssize;

	if (ctx == NULL)
		return 0;

	rightssize = sizeof(rights);
	if (a2aclr_whichrights(ctx, rights, &rightssize, realm, realmsize, id, idsize,
	    resource, resourcesize, instance, instancesize) == -1)
		return 0;

	if (reqrightssize == 0)
		return 0;

	for (i = 0; i < reqrightssize; i++) {
		/* prevent strchr to locate the terminating nul */
		if (reqrights[i] == '\0')
			return 0;

		if (strchr(rights, reqrights[i]) == NULL)
			return 0;
	}

	return 1;
}

/*
 * Determine if "id" within "realm" has the "reqright" over "resource" and
 * "instance".
 *
 * Returns 0 if "reqright" is not permitted.
 * Returns >0 otherwise.
 */
int
a2aclr_hasright(void *ctx, const char reqright, const char *realm,
    size_t realmsize, const char *id, size_t idsize, const char *resource,
    size_t resourcesize, const char *instance, size_t instancesize)
{
	return a2aclr_hasrights(ctx, &reqright, 1, realm, realmsize, id, idsize,
	    resource, resourcesize, instance, instancesize);
}

/*
 * Parse a resource ACL policy line. "line" must be nul terminated. No resource
 * instance can be differentiated by a resource instance of length zero by the
 * fact that "resource" points to NULL instead of the trailing nul of "line",
 * respectively.
 *
 * policyline = realm SEP selector SEP rights SEP resource [ SEP instance ]
 *    SEP        = TAB
 *    realm      = 1*print
 *    selector   = 2*graph
 *    rights     = 1*RIGHTS
 *    resource   = UUID
 *    instance   = 1*print
 *    UUID       = 36graph
 *    graph      = %x21-7e
 *    print      = %x20-7e
 *    RIGHTS     = 1*(A / S / D / C / W / R / P / K / O / V)
 *
 * I.e.:
 * example.org joe@example.com	V	11bc2aa8-4151-4a73-b589-0a86442b8502\
 *    	0b23d65d-2e31-496d-b715-f93f5d73b662
 *
 * Returns 0 on success and set "realm", "selector", "rights", "resource" and
 * optionally "instance" to point to the start of the respective part in "line".
 * Sizes are updated accordingly. Returns -1 on error and updates "err" to point
 * to the first erroneous character in "line".
 */
static int
parsepolicyline(const char **realm, size_t *realmsize,
    const char **selector, size_t *selectorsize,
    const char **rights, size_t *rightssize,
    const char **resource, size_t *resourcesize,
    const char **instance, size_t *instancesize,
    const char *line, const char **err)
{
	const char sep = '\t';
	a2id idsel;
	size_t i, n;

	*realm = NULL;
	*selector = NULL;
	*rights = NULL;
	*resource = NULL;
	*instance = NULL;

	*realmsize = 0;
	*selectorsize = 0;
	*rightssize = 0;
	*resourcesize = 0;
	*instancesize = 0;

	n = 0;

	for (i = 0; isprint(line[n + i]); i++)
		;

	if (line[n + i] != sep || i < 1) {
		*err = &line[n + i];
		return -1;
	}

	*realmsize = i;
	*realm = &line[n];

	/* jump string plus separator */
	n += i + 1;

	for (i = 0; isgraph(line[n + i]); i++)
		;

	if (line[n + i] != sep || i < 2) {
		*err = &line[n + i];
		return -1;
	}

	*selectorsize = i;
	*selector = &line[n];

	n += i + 1;

	/* Make sure this is a valid selector. */
	if (a2id_fromstr(&idsel, *selector, *selectorsize, 1) == -1) {
		*err = *selector;
		return -1;
	}

	/* Rights */

	i = strspn(&line[n], ALLRIGHTS);

	if (line[n + i] != sep || i < 1) {
		*err = &line[n + i];
		return -1;
	}

	*rightssize = i;
	*rights = &line[n];

	n += i + 1;

	/* Resource */

	for (i = 0; isgraph(line[n + i]); i++)
		;

	if (i != RESOURCELEN) {
		*err = &line[n + i];
		return -1;
	}

	if (line[n + i] == '\0') {
		/* eol, no instance, we're done */
		*resourcesize = i;
		*resource = &line[n];
		return 0;
	}

	if (line[n + i] != sep) {
		*err = &line[n + i];
		return -1;
	}

	*resourcesize = i;
	*resource = &line[n];

	n += i + 1;

	/* allow trailing tab without resource instance */
	if (line[n] == '\0')
		return 0;

	/* Instance */

	for (i = 0; isprint(line[n + i]); i++)
		;

	if (line[n + i] != '\0' || i < 1) {
		*err = &line[n + i];
		return -1;
	}

	*instancesize = i;
	*instance = &line[n];

	return 0;
}

/*
 * Import an ACL policy from a readable descriptor yielding ACL rules.
 *
 * Empty lines or lines starting with a hash '#' are ignored.
 *
 * Returns the number of imported ACL rules on success or -1 on error with errno
 * set. If "errstr" is passed and -1 is returned a descriptive error is set in
 * "errstr", nul terminated and at most "errstrsize" bytes, including the
 * terminating nul.
 */
static ssize_t
fromdes(void *ctx, int d, char *errstr, size_t errstrsize)
{
	const char *realm, *selector, *rights, *resource, *instance, *err;
	char *line;
	ssize_t lineno, imported;
	size_t realmsize, selectorsize, rightssize, resourcesize, instancesize, s, len;
	FILE *fp;

	if (errstrsize)
		errstr[0] = '\0';

	if ((fp = fdopen(d, "re")) == NULL)
		return -1; /* errno set */

	s = 0;
	lineno = 0;
	imported = 0;
	line = NULL;

	while (getline(&line, &s, fp) > 0) {
		lineno++;

		len = strcspn(line, "\n");
		if (len == 0)
			continue;

		if (line[0] == '#')
			continue;

		line[len] = '\0';

		if (parsepolicyline(&realm, &realmsize, &selector, &selectorsize,
		    &rights, &rightssize, &resource, &resourcesize, &instance,
		    &instancesize, line, &err) == -1) {
			if (errstrsize)
				snprintf(errstr, errstrsize, "illegal policy line"
				    " at %zu,%lu ->%s", lineno, err - line + 1, err);
			errno = EINVAL;
			free(line);
			return -1;
		}

		if (a2aclr_putrights(ctx, realm, realmsize, selector, selectorsize,
		    resource, resourcesize, instance, instancesize, rights,
		    rightssize) == -1) {
			if (errstr && errstrsize)
				snprintf(errstr, errstrsize, "failed to save ACL "
				    "rule #%zu: %s", lineno, line);
			errno = EINVAL;
			free(line);
			return -1;
		}

		imported++;
	}

	free(line);
	if (ferror(fp)) {
		if (errstr && errstrsize)
			snprintf(errstr, errstrsize, "%s: error while reading ACL"
			    "file", __func__);
		errno = EINVAL;
		return -1;
	}

	return imported;
}

/*
 * Import a Resource ACL policy from a text file specified by "policyfile" into
 * the database "ctx".
 *
 * If there is an error and "errstr" is not NULL, then "errstr" is updated with
 * a descriptive error of at most "errstrsize" bytes, including a terminating
 * nul.
 *
 * Each line in "policyfile" must consist of the following five-tuple (with
 * instance being optional): <realm selector rights resource instance>
 * Each column should be separated from the other by one tab character. Empty
 * lines or lines starting with a hash '#' are ignored.
 *
 * Returns the number of processed rules on success or -1 on error with errno
 * set. If "errstr" is passed and -1 is returned a descriptive error is set in
 * "errstr", nul terminated and at most "errstrsize" bytes, including the
 * terminating nul.
 */
ssize_t
a2aclr_importpolicyfile(void *ctx, const char *policyfile, char *errstr,
    size_t errstrsize)
{
	ssize_t rc;
	int d;

	if (ctx == NULL) {
		errno = EINVAL;
		return -1;
	}

	if (errstrsize)
		errstr[0] = '\0';

	if (policyfile == NULL) {
		errno = EINVAL;
		return -1;
	}

	if ((d = open(policyfile, O_RDONLY|O_CLOEXEC)) == -1)
		return -1; /* errno set */

	rc = fromdes(ctx, d, errstr, errstrsize);
	close(d);

	return rc;
}

/*
 * This is a simple implementation, relying on the assumption that
 * precisely one level of hostname precedes the domain name which
 * serves as the realm.  Note that this may be softened in later
 * versions, probably by introduction of a database that maps a
 * hostname to its realm (which must then also be trailing the
 * hostname).
 */
const char *
a2aclr_hostname2realm (const char *fqdn_hostname) {
	const char *ptr = strchr (fqdn_hostname, '.');
	if (ptr == NULL) {
		return NULL;
	}
	ptr++;
	return ptr;
}
