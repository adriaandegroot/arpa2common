/*
 * Copyright (c) 2019 Tim Kuijsten
 *
 * Permission to use, copy, modify, and/or distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES WITH
 * REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY
 * AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT,
 * INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM
 * LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR
 * OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
 * PERFORMANCE OF THIS SOFTWARE.
 */

#ifndef A2RACL_H
#define A2RACL_H

#include <sys/types.h>

#include <stddef.h>

#include "a2aclr_db.h"

/*
 *     A superpower
 *     S
 *     D right to delete
 *     C right to create
 *     W writing
 *     R reading
 *     P
 *     K checking for existence
 *     O editing and removing one's own objects
 *     V
 *
 *   Based on http://idp.arpa2.net/diameter.html
 */
#define ALLRIGHTS "ASDCWRPKOV"

ssize_t a2aclr_importpolicyfile(void *ctx, const char *policyfile,
    char *errstr, size_t errstrsize);

int a2aclr_hasright(void *ctx, const char reqright, const char *realm,
    size_t realmsize, const char *id, size_t idsize, const char *resource,
    size_t resourcesize, const char *instance, size_t instancesize);

int a2aclr_hasrights(void *ctx, const char *reqrights, size_t reqrightssize,
    const char *realm, size_t realmsize, const char *id, size_t idsize,
    const char *resource, size_t resourcesize, const char *instance,
    size_t instancesize);

int a2aclr_whichrights(void *ctx, char *rights, size_t *rightssize,
    const char *realm, size_t realmsize, const char *id, size_t idsize,
    const char *resource, size_t resourcesize, const char *instance,
    size_t instancesize);

/*
 * Lookup the realm for a fully qualified host name.  The realm is a trailer
 * of the hostname.  The return value is NULL if no realm could be found for
 * the supplied host name.
 *
 * This function is used by protocols that communicate host names instead of
 * domain names.  A good example is HTTP.  On such systems, aliases must not
 * interfere with the specified host names; the best chance for that is to
 * not lookup the client-sent host name, but the configured canonical host
 * name (web servers tend to have a canonical ServerName and additionally
 * allow ServerAlias names that are also recognised as Host: header).
 */
const char * a2aclr_hostname2realm (const char *fqdn_hostname);

#endif /* A2RACL_H */
