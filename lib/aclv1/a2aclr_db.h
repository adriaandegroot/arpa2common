/*
 * Copyright (c) 2019 Tim Kuijsten
 *
 * Permission to use, copy, modify, and/or distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES WITH
 * REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY
 * AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT,
 * INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM
 * LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR
 * OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
 * PERFORMANCE OF THIS SOFTWARE.
 */

/*
 * There are currently two database backend implementations: "dbmem" which is a
 * simple memory based key-value store and "dblmdb" which uses the LMDB database
 * backend. A particular backend can be choosen at compile-time.
 *
 * When implementing a database backend, the following five functions must be
 * implemented:
 *
 *    a2aclr_dbopen: Initialize a database backend and return a database
 *    context. If the database did not exist it must be created.
 *
 *    a2aclr_dbclose: Close a database backend.
 *
 *    a2aclr_count: Update "count" to the total number of rules in the database.
 *
 *    a2aclr_delrights: Delete one or more "rights" given a realm, selector,
 *    rights, resource and optionally a resource instance. If "rightssize" >
 *    MAXRIGHTSSIZE then the implementation must not delete any rights and must
 *    return -1. "rights" must be treated case-insensitive.
 *
 *    a2aclr_getrights: Fetch rights based on a realm, selector, resource and
 *    optionally a resource instance. "rights" will be allocated by the caller.
 *    "rightssize" is a value/result parameter. In case no rights are found then
 *    "rights" must be left untouched, "rightssize" must be set to 0 and 0 must
 *    be returned. In case rights are found but do not fit in "rights", -1 must
 *    be returned. The implementation must guarantee any succesful lookup will
 *    never yield a string longer than MAXRIGHTSSIZE. Also, every letter in the
 *    returned rights string must be unique and in uppercase.
 *
 *    a2aclr_putrights: Store one or more "rights" given a realm, selector,
 *    rights, resource and optionally a resource instance. If "rightssize" >
 *    MAXRIGHTSSIZE then the implementation must not save any rights and must
 *    return -1 in order to ensure the correct functioning of a2aclr_getrights.
 *    "rights" must be treated case-insensitive.
 *
 *    a2aclr_printdb: Write all keys and values in the database to "descriptor".
 *
 * Except for a2aclr_dbopen, all other six functions must return 0 on success
 * and -1 on failure.
 */

#include <stddef.h>

#define MAXRIGHTSSIZE 31

void *a2aclr_dbopen(const char *path);
int a2aclr_dbclose(void *ctx);
int a2aclr_count(void *ctx, size_t *count);
int a2aclr_printdb(void *ctx, int descriptor);
int a2aclr_getrights(void *ctx,
    char *rights, size_t *rightssize,
    const char *realm, size_t realmsize,
    const char *selector, size_t selectorsize,
    const char *resource, size_t resourcesize,
    const char *instance, size_t instancesize);
int a2aclr_delrights(void *ctx,
    const char *realm, size_t realmsize,
    const char *selector, size_t selectorsize,
    const char *resource, size_t resourcesize,
    const char *instance, size_t instancesize,
    const char *rights, size_t rightssize);
int a2aclr_putrights(void *ctx,
    const char *realm, size_t realmsize,
    const char *selector, size_t selectorsize,
    const char *resource, size_t resourcesize,
    const char *instance, size_t instancesize,
    const char *rights, size_t rightssize);
