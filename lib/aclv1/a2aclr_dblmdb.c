/*
 * Copyright (c) 2018, 2019 Tim Kuijsten
 *
 * Permission to use, copy, modify, and/or distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES WITH
 * REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY
 * AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT,
 * INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM
 * LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR
 * OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
 * PERFORMANCE OF THIS SOFTWARE.
 */

#include <ctype.h>
#include <limits.h>
#include <netinet/in.h>  // for htons
#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include <lmdb.h>

#include "a2aclr_db.h"
#include "blake2.h"
#include "arpa2/a2common/util.h"

#define MAXINSTANCELEN ((size_t)((1 << 14) - 1))

/* XXX temp static 128-bit secret, should be loaded from the database */
static const char DBPROTKEY[16] = {
    0x7c, 0x66, 0xd2, 0x1a, 0xe9, 0x94, 0x0c, 0x7c,
    0x3e, 0x62, 0x99, 0xa4, 0x4c, 0xd1, 0x94, 0xd5
};
static const char RESOURCEACL[14]         = "RESOURCE ACL x";
static const char RESOURCEINSTANCEACL[23] = "RESOURCE INSTANCE ACL x";
static const char KEYTRAILER[24]          = " DATABASE KEY ENCRYPTION";

/*
 * LMDB database backend for ARPA2 RESOURCE ACL.
 */

struct dblmdb_key {
	MDB_val mdb;
	uint8_t data[BLAKE2S_OUTBYTES];
	uint8_t preselcache[BLAKE2S_OUTBYTES];
};

struct dbenv {
	MDB_env *env;
	MDB_txn *txn;
	MDB_dbi dbi;
};

/*
 * Convert "rc" to a descriptive MDB error message and write to stderr,
 * optionally with a prefix.
 */
static void
dblmdb_printerr(int rc, const char *prefix)
{
	if (prefix)
		fprintf(stderr, "%s ", prefix);

	fprintf(stderr, "%s\n", mdb_strerror(rc));
}

/*
 * Print an MDB error and exit.
 */
static void
dblmdb_printerrx(int rc, int e)
{
	dblmdb_printerr(rc, NULL);
	exit(e);
}

static void
dblmdb_printkey(int d, MDB_val *key)
{
	dprintf(d, "key: %zu ", key->mv_size);
	hexdump(d, key->mv_data, key->mv_size, 40);
}

static void
dblmdb_printval(int d, MDB_val *val)
{
	dprintf(d, "val: %zu %.*s\n", val->mv_size, (int)val->mv_size,
	    (char*)(val->mv_data));
}

/*
 * Initialize a database backend.
 *
 * Returns a newly allocated db context on success, NULL on failure.
 */
void *
a2aclr_dbopen(const char *path)
{
	struct dbenv *dbe;
	int rc;

	if (path == NULL)
		return NULL;

	if ((dbe = malloc(sizeof(struct dbenv))) == NULL)
		return NULL;

	if ((rc = mdb_env_create(&dbe->env)) != 0)
		dblmdb_printerrx(rc, 1);

	if ((rc = mdb_env_open(dbe->env, path, MDB_NOSUBDIR, 0600)) != 0)
		dblmdb_printerrx(rc, 1);

	/*
	 * Open a new database handle and commit the transaction so that the
	 * handle becomes available in the shared environment where subsequent
	 * transactions can use it.
	 */
	if ((rc = mdb_txn_begin(dbe->env, NULL, 0, &dbe->txn)) != 0)
		dblmdb_printerrx(rc, 1);

	if ((rc = mdb_dbi_open(dbe->txn, NULL, 0, &dbe->dbi)) != 0)
		dblmdb_printerrx(rc, 1);

	if ((rc = mdb_txn_commit(dbe->txn)) != 0)
		dblmdb_printerrx(rc, 1);

	return dbe;
}

/*
 * Close a database backend by purging everything from memory.
 *
 * Must return 0 on success, -1 on failure.
 */
int
a2aclr_dbclose(void *ctx)
{
	struct dbenv *dbe = ctx;

	if (ctx == NULL)
		return -1;

	mdb_dbi_close(dbe->env, dbe->dbi);
	mdb_env_close(dbe->env);

	return 0;
}

/*
 * Update "count" to the total number of rules in the database.
 *
 * Return 0 on success, -1 on failure.
 */
int
a2aclr_count(void *ctx, size_t *count)
{
	struct dbenv *dbe = ctx;
	MDB_stat st;

	if (ctx == NULL)
		return -1;

	if (mdb_txn_begin(dbe->env, NULL, MDB_RDONLY, &dbe->txn) != 0)
		return -1;

	if (mdb_stat(dbe->txn, dbe->dbi, &st) != 0)
		return -1;
	*count = st.ms_entries;

	mdb_txn_abort(dbe->txn);

	return 0;
}

/*
 * Return newly allocated space for a key on success. It should be freed by the
 * caller using dblmdb_freekey when no longer needed.
 *
 * Return NULL on failure.
 */
static struct dblmdb_key *
dblmdb_allockey(void)
{
	struct dblmdb_key *key;

	if ((key = malloc(sizeof(*key))) == NULL)
		return NULL;

	key->mdb.mv_data = key->data;
	key->mdb.mv_size = sizeof(key->data);

	return key;
}

static void
dblmdb_freekey(struct dblmdb_key *key)
{
	free(key);
}

/*
 * Calculate and set the key for a given set of inputs.
 *
 * TODO
 *  - Let generalization happen here so that we can use an intermediate hash
 *    result for improved performance.
 *  - Make the hashing method pluggable at compile time. Currently uses blake2
 *    hashing because it's easy to embed and thus portable.
 *
 * Return 0 on success, -1 on failure.
 */
static int
dblmdb_setkey(struct dblmdb_key *key, const char *realm, size_t realmsize, const char *selector,
    size_t selectorsize, const char *resource, size_t resourcesize,
    const char *instance, size_t instancesize)
{
	blake2s_state state;
	const char space = ' ';
	uint16_t instancesizebe;

	/*
	 * Key format:
	 * resource constant realm [instancelen instance] selector trailer
	 */

	if (instancesize > MAXINSTANCELEN)
		return -1;

	if (blake2s_init_key(&state, BLAKE2S_OUTBYTES, DBPROTKEY,
	    sizeof(DBPROTKEY)) == -1)
		return -1;

	blake2s_update(&state, resource, resourcesize);

	/* TODO use proper x padding */
	if (instancesize > 0) {
		blake2s_update(&state, RESOURCEINSTANCEACL,
		    sizeof(RESOURCEINSTANCEACL) - 1);
	} else {
		blake2s_update(&state, RESOURCEACL,
		    sizeof(RESOURCEACL) - 1);
	}

	/* TODO canonicalize realm */
	blake2s_update(&state, realm, realmsize);
	blake2s_update(&state, &space, sizeof(space));

	if (instancesize > 0) {
		instancesizebe = htons(instancesize);

		blake2s_update(&state, &instancesizebe, sizeof(instancesizebe));
		blake2s_update(&state, instance, instancesize);
	}

	/* TODO fill pre-selector cache */

	blake2s_update(&state, selector, selectorsize);
	blake2s_update(&state, KEYTRAILER, sizeof(KEYTRAILER) - 1);

	if (blake2s_final(&state, key->mdb.mv_data, key->mdb.mv_size) == -1)
		return -1;

	return 0;
}

/*
 * Ensure "rights" are stored for the given conditions. Any pre-existing rights
 * are not altered. Lower-case letters are automatically converted to
 * upper-case and dupes are removed.
 *
 * Return 0 on success, -1 on failure.
 */
int
a2aclr_putrights(void *ctx, const char *realm, size_t realmsize,
    const char *selector, size_t selectorsize, const char *resource,
    size_t resourcesize, const char *instance, size_t instancesize,
    const char *rights, size_t rightssize)
{
	char newrights[MAXRIGHTSSIZE];
	struct dbenv *dbe = ctx;
	struct dblmdb_key *key;
	MDB_val val;
	size_t i, newrightslen;
	int rc;

	if (ctx == NULL)
		return -1;

	if (realm == NULL || selector == NULL || resource == NULL ||
	    rights == NULL || realmsize == 0 || selectorsize == 0 ||
	    resourcesize == 0 || rightssize == 0)
		return -1;

	if (rightssize > MAXRIGHTSSIZE)
		return -1;

	/* verify rights */
	for (i = 0; i < rightssize; i++) {
		switch (rights[i]) {
		case 'A':
		case 'S':
		case 'D':
		case 'C':
		case 'W':
		case 'R':
		case 'P':
		case 'K':
		case 'O':
		case 'V':
		case 'a':
		case 's':
		case 'd':
		case 'c':
		case 'w':
		case 'r':
		case 'p':
		case 'k':
		case 'o':
		case 'v':
			break;
		default:
			return -1;
		}
	}

	if ((key = dblmdb_allockey()) == NULL)
		return -1;

	if (dblmdb_setkey(key, realm, realmsize, selector, selectorsize, resource,
	    resourcesize, instance, instancesize) == -1) {
		dblmdb_freekey(key);
		return -1;
	}

	if ((rc = mdb_txn_begin(dbe->env, NULL, 0, &dbe->txn)) != 0)
		dblmdb_printerrx(rc, 1);

	/* merge existing rights with new rights */

	rc = mdb_get(dbe->txn, dbe->dbi, &key->mdb, &val);
	if (rc == 0) {
		if (val.mv_size > sizeof(newrights))
			abort();

		memcpy(newrights, val.mv_data, val.mv_size);
		newrightslen = val.mv_size;
	} else {
		if (rc != MDB_NOTFOUND)
			dblmdb_printerrx(rc, 1);

		newrightslen = 0;
	}

	/* copy unique new rights, ensure upper-case */
	for (i = 0; i < rightssize; i++) {
		if (memchr(newrights, toupper(rights[i]), newrightslen)
		    == NULL) {
			if (newrightslen >= sizeof(newrights))
				break;

			newrights[newrightslen] = toupper(rights[i]);
			newrightslen++;
		}
	}

	/* make sure all rights were evaluated */
	if (i < rightssize) {
		mdb_txn_abort(dbe->txn);
		dblmdb_freekey(key);
		return -1;
	}

	val.mv_size = newrightslen;
	val.mv_data = newrights;

	rc = mdb_put(dbe->txn, dbe->dbi, &key->mdb, &val, 0);
	if (rc != 0) {
		dblmdb_printerr(rc, __func__);
		mdb_txn_abort(dbe->txn);
		dblmdb_freekey(key);
		return -1;
	}

	if ((rc = mdb_txn_commit(dbe->txn)) != 0)
		dblmdb_printerrx(rc, 1);

	dblmdb_freekey(key);

	return 0;
}

/*
 * Ensure "rights" are removed. Any other rights for the given conditions
 * are not removed.
 *
 * Return 0 on success, -1 on failure.
 */
int
a2aclr_delrights(void *ctx, const char *realm, size_t realmsize,
    const char *selector, size_t selectorsize, const char *resource,
    size_t resourcesize, const char *instance, size_t instancesize,
    const char *rights, size_t rightssize)
{
	char newrights[MAXRIGHTSSIZE];
	struct dbenv *dbe = ctx;
	struct dblmdb_key *key;
	MDB_val val;
	size_t i, newrightslen;
	int rc;
	char *cp;

	if (ctx == NULL)
		return -1;

	if (realm == NULL || selector == NULL || resource == NULL ||
	    rights == NULL || realmsize == 0 || selectorsize == 0 ||
	    resourcesize == 0 || rightssize == 0)
		return -1;

	if (rightssize > MAXRIGHTSSIZE)
		return -1;

	if ((key = dblmdb_allockey()) == NULL)
		return -1;

	if (dblmdb_setkey(key, realm, realmsize, selector, selectorsize,
	    resource, resourcesize, instance, instancesize) == -1) {
		dblmdb_freekey(key);
		return -1;
	}

	if ((rc = mdb_txn_begin(dbe->env, NULL, 0, &dbe->txn)) != 0)
		dblmdb_printerrx(rc, 1);

	rc = mdb_get(dbe->txn, dbe->dbi, &key->mdb, &val);
	if (rc != 0) {
		if (rc == MDB_NOTFOUND) {
			/* done */
			dblmdb_freekey(key);
			return 0;
		} else {
			dblmdb_printerrx(rc, 1);
		}
	}

	if (val.mv_size > sizeof(newrights)) {
		dblmdb_freekey(key);
		return -1;
	}

	memcpy(newrights, val.mv_data, val.mv_size);
	newrightslen = val.mv_size;

	for (i = 0; i < rightssize; i++) {
		cp = memchr(newrights, rights[i], newrightslen);
		cp = memchr(newrights, toupper(rights[i]), newrightslen);
		if (cp != NULL) {
			/* remove *cp and coalesce any subsequent rights */
			while (cp + 1 < newrights + newrightslen) {
				*cp = *(cp + 1);
				cp++;
			}

			newrightslen--;
		}
	}

	val.mv_size = newrightslen;
	val.mv_data = newrights;

	if (newrightslen > 0) {
		rc = mdb_put(dbe->txn, dbe->dbi, &key->mdb, &val, 0);
	} else {
		rc = mdb_del(dbe->txn, dbe->dbi, &key->mdb, NULL);
	}

	if (rc != 0) {
		dblmdb_printerr(rc, __func__);
		mdb_txn_abort(dbe->txn);
		dblmdb_freekey(key);
		return -1;
	}

	if ((rc = mdb_txn_commit(dbe->txn)) != 0)
		dblmdb_printerrx(rc, 1);

	dblmdb_freekey(key);

	return 0;
}

/*
 * Fetch ACL Resource rights.
 *
 *    For implementers: Fetch rights based on a realm, selector, resource and
 *      optionally a resource instance. "rights" will be allocated by the
 *      caller. "rightssize" is a value/result parameter. In case no ACL rule is
 *      found then "rights" must be left untouched, "rightssize" must be set to
 *      0 and 0 must be returned. In case rights are found but do not fit in
 *      "rights", -1 must be returned. If "rightssize" >= MAXRIGHTSSIZE then the
 *      implementation must guarantee any successful lookup will always fit in
 *      "rights". "rights" is *not* terminated with a nul.
 *
 * Must return 0 on success, -1 on error. If no "rights" are found, 0 is
 * returned and *rightssize is set to 0.
 */
int
a2aclr_getrights(void *ctx, char *rights, size_t *rightssize, const char *realm,
    size_t realmsize, const char *selector, size_t selectorsize,
    const char *resource, size_t resourcesize, const char *instance,
    size_t instancesize)
{
	MDB_val val;
	struct dblmdb_key *key;
	struct dbenv *dbe = ctx;
	int rc;

	if (ctx == NULL)
		return -1;

	if (rights == NULL || rightssize == NULL || realm == NULL ||
	    selector == NULL || resource == NULL || realmsize == 0 ||
	    selectorsize == 0 || resourcesize == 0)
		return -1;

	if ((key = dblmdb_allockey()) == NULL)
		return -1;

	if (dblmdb_setkey(key, realm, realmsize, selector, selectorsize, resource,
	    resourcesize, instance, instancesize) == -1) {
		dblmdb_freekey(key);
		return -1;
	}

	if ((rc = mdb_txn_begin(dbe->env, NULL, MDB_RDONLY, &dbe->txn)) != 0)
		dblmdb_printerrx(rc, 1);

	rc = mdb_get(dbe->txn, dbe->dbi, &key->mdb, &val);
	dblmdb_freekey(key);

	if (rc != 0) {
		mdb_txn_abort(dbe->txn);
		if (rc == MDB_NOTFOUND) {
			*rightssize = 0;
			return 0;
		} else {
			dblmdb_printerrx(rc, 1);
		}
	}

	if (val.mv_size > *rightssize) {
		mdb_txn_abort(dbe->txn);
		return -1;
	}

	/*
	 * We only store through a2aclr_putrights and trust that it
	 * does conform and check MAXRIGHTSSIZE before storing.
	 */

	memcpy(rights, val.mv_data, val.mv_size);
	*rightssize = val.mv_size;

	mdb_txn_abort(dbe->txn);

	return 0;
}

/*
 * Return 0 on success, -1 on failure.
 */
int
a2aclr_printdb(void *ctx, int d)
{
	struct dbenv *dbe = ctx;
	MDB_val key, val;
	MDB_cursor *cursor;
	int rc;

	if (ctx == NULL)
		return -1;

	if ((rc = mdb_txn_begin(dbe->env, NULL, MDB_RDONLY, &dbe->txn)) != 0)
		dblmdb_printerrx(rc, 1);

	if ((rc = mdb_cursor_open(dbe->txn, dbe->dbi, &cursor)) != 0)
		dblmdb_printerrx(rc, 1);

	if ((rc = mdb_cursor_get(cursor, &key, &val, MDB_FIRST)) != 0)
		dblmdb_printerrx(rc, 1);

	do {
		if (key.mv_size > SIZE_MAX)
			continue;
		if (val.mv_size > SIZE_MAX)
			continue;
		dblmdb_printkey(d, &key);
		dblmdb_printval(d, &val);
	} while ((rc = mdb_cursor_get(cursor, &key, &val, MDB_NEXT)) == 0);
	if (rc != MDB_NOTFOUND)
		dblmdb_printerrx(rc, 1);

	mdb_cursor_close(cursor);
	mdb_txn_abort(dbe->txn);

	return 0;
}
