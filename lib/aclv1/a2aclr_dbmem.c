/*
 * Copyright (c) 2018, 2019 Tim Kuijsten
 *
 * Permission to use, copy, modify, and/or distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES WITH
 * REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY
 * AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT,
 * INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM
 * LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR
 * OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
 * PERFORMANCE OF THIS SOFTWARE.
 */

#include <ctype.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "a2aclr_db.h"

/*
 * Extremely simple memory-only database implementation for ARPA2 RESOURCE ACL.
 *
 * Time complexity:
 *   find: O(log(n))
 *   insert/delete: O(n)
 *
 * Space complexity:
 *   O(n)
 */

struct dbmem_entry {
	void *realm;
	void *selector;
	void *resource;
	void *instance;
	void *rights;
	size_t realmsize;
	size_t selectorsize;
	size_t resourcesize;
	size_t instancesize;
	size_t rightssize;
};

struct list {
	size_t listsize;
	struct dbmem_entry **head;
};

static void
dbmem_printentry(int d, const struct dbmem_entry *de)
{
	dprintf(d, "%.*s %.*s %.*s %.*s %.*s\n",
	    (int)de->realmsize,
	    de->realm,
	    (int)de->selectorsize,
	    de->selector,
	    (int)de->rightssize,
	    de->rights,
	    (int)de->resourcesize,
	    de->resource,
	    (int)de->instancesize,
	    de->instance);
}

/*
 * Free dbmem_entry structure.
 */
static void
dbmem_free(struct dbmem_entry *ep)
{
	if (ep == NULL)
		return;

	free(ep->realm);
	ep->realm = NULL;

	free(ep->selector);
	ep->selector = NULL;

	free(ep->resource);
	ep->resource = NULL;

	free(ep->instance);
	ep->instance = NULL;

	free(ep->rights);
	ep->rights = NULL;

	free(ep);
	ep = NULL;
}

/*
 * Allocate a new dbmem_entry structure.
 *
 * Return a newly allocated structure on success that should be freed with
 * dbmem_free by the caller when done. Return NULL on error with errno set.
 */
static struct dbmem_entry *
dbmem_alloc(const void *realm, size_t realmsize, const void *selector,
    size_t selectorsize, const void *resource, size_t resourcesize,
    const void *instance, size_t instancesize, const void *rights,
    size_t rightssize)
{
	struct dbmem_entry *ep = NULL;

	if ((ep = malloc(sizeof(*ep))) == NULL)
		goto err; /* errno set */

	if ((ep->realm = malloc(realmsize)) == NULL)
		goto err; /* errno set */

	if ((ep->selector = malloc(selectorsize)) == NULL)
		goto err; /* errno set */

	if ((ep->resource = malloc(resourcesize)) == NULL)
		goto err; /* errno set */

	if ((ep->instance = malloc(instancesize)) == NULL)
		goto err; /* errno set */

	if ((ep->rights = malloc(rightssize)) == NULL)
		goto err; /* errno set */

	memcpy(ep->realm, realm, realmsize);
	memcpy(ep->selector, selector, selectorsize);
	memcpy(ep->resource, resource, resourcesize);
	memcpy(ep->instance, instance, instancesize);
	memcpy(ep->rights, rights, rightssize);

	ep->realmsize = realmsize;
	ep->selectorsize = selectorsize;
	ep->resourcesize = resourcesize;
	ep->instancesize = instancesize;
	ep->rightssize = rightssize;

	return ep;

err:
	dbmem_free(ep);
	return NULL;
}

/*
 * Initialize a database backend.
 *
 * Returns a newly allocated db context on success, NULL on failure.
 */
void *
a2aclr_dbopen(const char *path)
{
	struct list *lp;

	/* silence compiler */
	path = NULL;

	if ((lp = malloc(sizeof(struct list))) == NULL)
		return NULL;

	lp->listsize = 0;
	lp->head = NULL;

	return lp;
}

/*
 * Close a database backend by purging everything from memory.
 *
 * Must return 0 on success, -1 on failure.
 */
int
a2aclr_dbclose(void *ctx)
{
	struct list *lp = ctx;

	if (lp == NULL)
		return -1;

	while (lp->listsize > 0) {
		dbmem_free(lp->head[lp->listsize - 1]);
		lp->head[lp->listsize - 1] = NULL;
		lp->listsize--;
	}

	if (lp->listsize != 0)
		return -1;

	lp->head = NULL;

	free(lp);
	lp = NULL;

	return 0;
}

/*
 * Update "count" to the total number of rules in the database.
 *
 * Return 0 on success, -1 on failure.
 */
int
a2aclr_count(void *ctx, size_t *count)
{
	const struct list *lp = ctx;

	if (ctx == NULL)
		return -1;

	*count = lp->listsize;
	return 0;
}

/*
 * Find an entry by realm, selector, resource and optionally a resource
 * instance.
 *
 * Return the index in "lp" if found or < 0 if not found.
 */
static ssize_t
dbmem_index(void *ctx, const char *realm, size_t realmsize, const char *selector,
    size_t selectorsize, const char *resource, size_t resourcesize,
    const char *instance, size_t instancesize)
{
	const struct list *lp = ctx;
	size_t i;

	if (lp == NULL)
		return -1;

	if (realm == NULL || selector == NULL || resource == NULL ||
	    realmsize == 0 || selectorsize == 0 || resourcesize == 0)
		return -1;

	for (i = 0; i < lp->listsize; i++) {
		if (lp->head[i]->realmsize != realmsize)
			continue;

		if (lp->head[i]->selectorsize != selectorsize)
			continue;

		if (lp->head[i]->resourcesize != resourcesize)
			continue;

		if (lp->head[i]->instancesize != instancesize)
			continue;

		if (memcmp(lp->head[i]->realm, realm, realmsize) != 0)
			continue;

		if (memcmp(lp->head[i]->selector, selector, selectorsize) != 0)
			continue;

		if (memcmp(lp->head[i]->resource, resource, resourcesize) != 0)
			continue;

		if (memcmp(lp->head[i]->instance, instance, instancesize) != 0)
			continue;

		return i;
	}

	return -1;
}

/*
 * Fetch ACL Resource rights.
 *
 *    a2aclr_getrights: Fetch rights based on a realm, selector, resource and
 *      optionally a resource instance. "rights" will be allocated by the
 *      caller. "rightssize" is a value/result parameter. In case no ACL rule is
 *      found then "rights" must be left untouched, "rightssize" must be set to
 *      0 and 0 must be returned. In case rights are found but do not fit in
 *      "rights", -1 must be returned. If "rightssize" >= MAXRIGHTSSIZE then the
 *      implementation must guarantee any successful lookup will always fit in
 *      "rights". "rights" is *not* terminated with a nul.
 *
 * Must return 0 on success, -1 on error. If no "rights" are found, 0 is
 * returned and *rightssize is set to 0.
 */
int
a2aclr_getrights(void *ctx, char *rights, size_t *rightssize, const char *realm,
    size_t realmsize, const char *selector, size_t selectorsize,
    const char *resource, size_t resourcesize, const char *instance,
    size_t instancesize)
{
	const struct list *lp = ctx;
	struct dbmem_entry *de;
	ssize_t rc;

	if (ctx == NULL)
		return -1;

	if (rights == NULL || rightssize == NULL)
		return -1;

	rc = dbmem_index(ctx, realm, realmsize, selector, selectorsize,
	    resource, resourcesize, instance, instancesize);

	if (rc < 0) {
		*rightssize = 0;
		return 0;
	}

	de = lp->head[rc];

	/*
	 * We only store through a2aclr_putrights and trust that it
	 * does conform and check MAXRIGHTSSIZE before storing.
	 */

	if (de->rightssize > *rightssize)
		return -1;

	memcpy(rights, de->rights, de->rightssize);
	*rightssize = de->rightssize;

	return 0;
}

/*
 * Ensure "rights" are stored for the given conditions. Any pre-existing rights
 * are not altered. Lower-case letters are automatically converted to
 * upper-case and dupes are removed.
 *
 * Must return 0 on success, -1 on failure.
 */
int
a2aclr_putrights(void *ctx, const char *realm, size_t realmsize,
    const char *selector, size_t selectorsize, const char *resource,
    size_t resourcesize, const char *instance, size_t instancesize,
    const char *rights, size_t rightssize)
{
	char newrights[MAXRIGHTSSIZE], *newrightsp;
	struct dbmem_entry *de;
	struct list *lp = ctx;
	ssize_t rc;
	size_t i, newrightslen;

	if (ctx == NULL)
		return -1;

	if (rightssize == 0)
		return 0;

	if (rights == NULL)
		return -1;

	if (rightssize > MAXRIGHTSSIZE)
		return -1;

	/* verify rights */
	for (i = 0; i < rightssize; i++) {
		switch (rights[i]) {
		case 'A':
		case 'S':
		case 'D':
		case 'C':
		case 'W':
		case 'R':
		case 'P':
		case 'K':
		case 'O':
		case 'V':
		case 'a':
		case 's':
		case 'd':
		case 'c':
		case 'w':
		case 'r':
		case 'p':
		case 'k':
		case 'o':
		case 'v':
			break;
		default:
			return -1;
		}
	}

	rc = dbmem_index(ctx, realm, realmsize, selector, selectorsize,
	    resource, resourcesize, instance, instancesize);

	if (rc >= 0) {
		if (lp->head[rc]->rightssize > sizeof(newrights))
			abort();

		memcpy(newrights, lp->head[rc]->rights,
		    lp->head[rc]->rightssize);

		newrightslen = lp->head[rc]->rightssize;
	} else {
		newrightslen = 0;
	}

	/* copy unique new rights, ensure upper-case */
	for (i = 0; i < rightssize; i++) {
		if (memchr(newrights, toupper(rights[i]), newrightslen)
		    == NULL) {
			if (newrightslen >= sizeof(newrights))
				break;

			newrights[newrightslen] = toupper(rights[i]);
			newrightslen++;
		}
	}

	/* make sure all rights were evaluated */
	if (i < rightssize)
		return -1;

	if (rc < 0) {
		/*
		 * Create new entry.
		 */

		if ((lp->listsize * sizeof(de)) >= ((lp->listsize + 1) *
		    sizeof(de)))
			return -1; /* overflow */

		de = dbmem_alloc(realm, realmsize, selector, selectorsize,
		    resource, resourcesize, instance, instancesize, newrights,
		    newrightslen);

		if (de == NULL)
			return -1;

		lp->head = realloc(lp->head, (lp->listsize + 1) * sizeof(de));
		if (lp->head == NULL) {
			dbmem_free(de);
			de = NULL;
			return -1;
		}

		lp->head[lp->listsize] = de;
		lp->listsize++;
	} else {
		/*
		 * Update existing entry.
		 */

		de = lp->head[rc];

		newrightsp = realloc(de->rights, newrightslen);
		if (newrightsp == NULL)
			return -1;

		memcpy(newrightsp, newrights, newrightslen);

		de->rights = newrightsp;
		de->rightssize = newrightslen;
	}

	return 0;
}

/*
 * Ensure "rights" are removed. Any remaining rights for the given conditions
 * are not removed. "rights" is case-insensitive.
 *
 * Return 0 on success, -1 on failure.
 */
int
a2aclr_delrights(void *ctx, const char *realm, size_t realmsize,
    const char *selector, size_t selectorsize, const char *resource,
    size_t resourcesize, const char *instance, size_t instancesize,
    const char *rights, size_t rightssize)
{
	struct list *lp = ctx;
	struct dbmem_entry *de;
	ssize_t rc;
	size_t i;
	char *cp;

	if (rights == NULL)
		return -1;

	rc = dbmem_index(ctx, realm, realmsize, selector, selectorsize,
	    resource, resourcesize, instance, instancesize);

	if (rc < 0)
		return 0;

	de = lp->head[rc];

	for (i = 0; i < rightssize; i++) {
		cp = memchr(de->rights, toupper(rights[i]), de->rightssize);
		if (cp != NULL) {
			/* remove *cp and coalesce any subsequent rights */
			while (cp + 1 < (char *)de->rights + de->rightssize) {
				*cp = *(cp + 1);
				cp++;
			}

			de->rightssize--;
		}
	}

	/*
	 * Either shrink the entry's rights or remove the complete entry from
	 * the list if there are no rights left.
	 */

	if (de->rightssize > 0) {
		de->rights = realloc(de->rights, de->rightssize);
		if (de->rights == NULL) {
			/* how can memory shrinking be a problem? */
			abort();
		}
	} else {
		for (i = rc; i + 1 < lp->listsize; i++)
			lp->head[i] = lp->head[i + 1];

		lp->listsize--;
		lp->head = realloc(lp->head, lp->listsize * sizeof(de));
		if (lp->head == NULL)
			abort();

		dbmem_free(de);
		de = NULL;
	}

	return 0;
}

int
a2aclr_printdb(void *ctx, int d)
{
	const struct list *lp = ctx;
	size_t i;

	if (ctx == NULL)
		return -1;

	for (i = 0; i < lp->listsize; i++)
		dbmem_printentry(d, lp->head[i]);

	return 0;
}
