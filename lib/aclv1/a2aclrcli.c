/*
 * Copyright (c) 2018, 2019 Tim Kuijsten
 *
 * Permission to use, copy, modify, and/or distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES WITH
 * REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY
 * AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT,
 * INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM
 * LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR
 * OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
 * PERFORMANCE OF THIS SOFTWARE.
 */

#include <errno.h>
#include <libgen.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include "a2aclr.h"
#include "a2id.h"

static const char *progname;
static int verbose;

/*
 * Lookup the rights of a certain id to a resource, withing a realm.
 *
 * Reads a policy from a text file and echoes the rights on stdout. If no rights
 * are found, an empty line is echoed.
 */

static int printopt;

static void
printusage(int d)
{
	dprintf(d, "usage: %s [-pv] [-d dbarg] [-i policyfile] realm id "
	    "resource [instance]\n",
	    progname);
}

int
main(int argc, char *argv[])
{
	a2id a2id;
	const char *dbdebug, *dbfile, *dbarg, *importpolicyfile, *realm, *id,
	    *resource, *instance, emptystr[] = "";
	char rights[MAXRIGHTSSIZE + 1];
	char errstr[100];
	ssize_t u;
	size_t t, rightssize;
	void *ctx;
	int c;

	if ((progname = basename(argv[0])) == NULL) {
		perror("basename");
		exit(1);
	}

	dbarg = importpolicyfile = NULL;
	while ((c = getopt(argc, argv, "d:i:hpv")) != -1) {
		switch (c) {
		case 'd':
			dbarg = optarg;
			break;
		case 'i':
			importpolicyfile = optarg;
			break;
		case 'p':
			printopt = 1;
			break;
		case 'h':
			printusage(STDOUT_FILENO);
			exit(0);
		case 'v':
			verbose++;
			break;
		default:
			printusage(STDERR_FILENO);
			exit(1);
		}
	}

	argc -= optind;
	argv += optind;

	if (argc < 3 || argc > 4) {
		printusage(STDERR_FILENO);
		exit(1);
	}

	realm = argv[0];
	id = argv[1];
	resource = argv[2];

	if (argc == 4) {
		instance = argv[3];
	} else {
		instance = emptystr;
	}

	/* check validity of a2id */
	if (a2id_fromstr(&a2id, id, strlen(id), 0) == -1) {
		fprintf(stderr, "%s: illegal ARPA2 ID\n", id);
		exit(4);
	}

	if (verbose > 1)
		a2id_dprint(STDOUT_FILENO, &a2id);

	dbfile = NULL;

	if (dbarg == NULL)
		dbarg = "mem";

	if (strcmp(dbarg, "mem") == 0) {
		dbdebug = "memory";
	} else if (strncmp(dbarg, "lmdb:", 5) == 0) {
		dbfile = &dbarg[5];
		dbdebug = dbfile;
	} else {
		fprintf(stderr, "unknown database type: %s\n", dbarg);
		exit(4);
	}

	if ((ctx = a2aclr_dbopen(dbfile)) == NULL) {
		fprintf(stderr, "error opening database: %s\n", dbdebug);
		exit(4);
	}

	if (importpolicyfile) {
		u = a2aclr_importpolicyfile(ctx, importpolicyfile, errstr,
		    sizeof(errstr));

		if (u == -1) {
			fprintf(stderr, "%s: %s, %s\n", importpolicyfile,
			    strerror(errno), errstr);
			exit(4);
		}

		fprintf(stdout, "newly imported ACL rules %zu\n", u);
	}

	if (a2aclr_count(ctx, &t) == -1) {
		fprintf(stderr, "count error\n");
		exit(4);
	}

	if (t == 0) {
		fprintf(stderr, "database contains no rules: %s\n", dbdebug);
		exit(4);
	}

	if (verbose > 0)
		fprintf(stdout, "total number of ACL rules: %zu\n", t);

	if (printopt)
		a2aclr_printdb(ctx, STDOUT_FILENO);

	rightssize = sizeof(rights);
	if (a2aclr_whichrights(ctx, rights, &rightssize, realm, strlen(realm), id,
	    strlen(id), resource, strlen(resource), instance, strlen(instance))
	    == -1) {
		fprintf(stderr, "internal error\n");
		exit(4);
	}

	/* assert(rightssize <= MAXRIGHTSSIZE) */
	rights[rightssize] = '\0';
	fprintf(stdout, "%.*s\n", (int)rightssize, rights);

	if (a2aclr_dbclose(ctx) == -1) {
		fprintf(stderr, "dbclose error\n");
		exit(4);
	}

	return 0;
}
