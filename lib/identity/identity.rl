/* ARPA2 Identity and Selector handling
 *
 * This parses strings with identities, and derives an array of
 * monotonically rising offsets.  Ranges between these offsets
 * define substrings, which may be empty for absent optionals
 * and which may include separator symbols like @ and + which
 * can be used for accurate matching.
 *
 * This module parses identities, allows them to be edited with
 * added aliases or simple context-sensitive signatures.  The
 * core concepts are an ARPA2 Identity, representative of an
 * individual user, and an ARPA2 Selector, which represents a
 * pattern that matches multiple of these.  An ARPA2 Identity
 * is always an ARPA2 Selector, namely the most specific form.
 *
 * SPDX-License-Identifier: BSD-2-Clause
 * SPDX-FileCopyrightText: Copyright 2020 Rick van Rein <rick@openfortress.nl>
 */


#include <stdlib.h>
#include <stdint.h>
#include <stdbool.h>
#include <string.h>
#include <ctype.h>
#include <assert.h>
#include <limits.h>

#include <unistd.h>

#include <errno.h>
#include <com_err.h>
#include <com_err/arpa2identity.h>

#include "arpa2/except.h"
#include "arpa2/digest.h"
#include "arpa2/identity.h"



/* The BASE32 table can be used to map numbers in the
 * range 0..31 to their uppercase encoding.  The table
 * can be searched with strchr() to learn the value,
 * where lowercase indexes will be >= 32 but the higher
 * bits can then be cut off.
 */
static const char base32table [] = "ABCDEFGHIJKLMNOPQRSTUVWXYZ234567abcdefghijklmnopqrstuvwxyz";


/* The key store is based on global variables.  This can
 * be setup while the program is in a more privileged
 * mode, and can still access key files.  After dropping
 * privileges, it would quite lame to pass around pointers
 * to the key material if it could also be concealed.
 *
 * The keys are in a list, to support trying again when
 * a2id_verify() fails.  In a2id_sign(), the first key is
 * always tried.  The procedure clones the seed and adds
 * whatever entropy it needs from there on.
 */
static struct storedkey {
	struct a2md_state seed;
	struct storedkey *next;
} *_storedkeys, *_recycledkeys;


/* Internal routine for parsing support.  Map a BASE32 character to its
 * integer (mod 32) value.
 */
static uint8_t _b32charval (char ch) {
	if (ch == '0') {
		ch = 'O';
	} else if (ch == '1') {
		ch = 'I';
	}
	char *pos = strchr (base32table, ch);
	assert (pos != NULL);
	uint8_t retval = (pos - base32table);
	retval &= 0x1f;
	return retval;
}


/* Internal routine for parsing support, to find the purpose of a '+'.
 */
enum {
	SWITCH_ARG,
	SWITCH_SIG,
	SWITCH_OPEN,
};
static int _plustype (const char *p) {
	assert (*p == '+');
	p++;
	char *next = strchr (p, '+');
	if (next == NULL) {
		if (*p == '@') {
			//
			// Last '+' followed by '@' --> open end
			log_debug ("_plustype (%s) --> LAST --> SWITCH_OPEN", p-1);
			return SWITCH_OPEN;
		} else {
			//
			// Last '+' followed by local chars --> aliasing
			log_debug ("_plustype (%s) --> LAST --> SWITCH_ARG", p-1);
			return SWITCH_ARG;
		}
	}
	assert (*next == '+');
	next++;
	if (*next == '@') {
		//
		// Next '+' followed by '@' --> signature
		log_debug ("_plustype (%s) --> SWITCH_SIG", p-1);
		return SWITCH_SIG;
	} else if (next == p + 1) {
		//
		// Next '+' follows right after tested '+' --> open end
		log_debug ("_plustype (%s) --> SWITCH_OPEN", p-1);
		return SWITCH_OPEN;
//TODO// ook voor ++@ zou echter SWITCH_SIG moeten zijn
	} else {
		//
		// Next '+' followed by local chars --> arg
		log_debug ("_plustype (%s) --> SWITCH_ARG", p-1);
		return SWITCH_ARG;
	}
}


/* Internal macro for parsing support.  Set the offset to the offset
 * of the current pointer (p) relative to the identity a2id.
 */
#define SETOFS(a2id,p,ofsidx) { \
	(a2id)->ofs [A2ID_OFS_ ## ofsidx] = ((p) - (a2id)->txt) ; \
	log_detail ("Offset #%d / %s set to %3d, \"%s\"", \
		A2ID_OFS_ ## ofsidx, \
		#ofsidx, (int) ((p) - (a2id)->txt), (p <= pe) ? p : "<EOF>"); }


/* Log a remark from a Ragel trigger, mentioning the parser's file:line.
 */
#define ragel_detail(descr) \
	log_detail ("%s at %s:%d, %s", descr, __FILE__, __LINE__, p);


%%{

	# Following is the machinery for ARPA2 Identity & Selector.
	#
	machine arpa2identity ;

	# Size restrictions: Maximum 64 or 255 characters in local-part
	# and domain.  This ought avoid the Cartesian products of
	#	... & utf8char{1,255}
	#	... & any{,64}
	# because these conditions can be imposed on all states without
	# introducing new states.  The form then becomes
	#	... >{ max64  = 64;  }
	#	    ${ max64-- > 0 }
	#	... >{ max255 = 255; }
	#	    ${ ((fc & 0xc0) == 0x80) || (max255-- > 0) }
	# Note the latter form, which counts UTF-8 characters and not
	# bytes.  It is a property of UTF-8 codes that extension bytes
	# all have bits 10xxxxxx.  Counting should be done on the other
	# codes, and this is also where filtering starts.
	#
	# The definitions below are conveniences, they can be added
	# with & but will not cause a state explosion.
	#
	max64bytes = ( any when { max64-- > 0 } )*
		;
	#
	action max255utf8 { ((fc & 0xc0) == 0x80) || (max255-- > 0) }
	max255chars = ( any when max255utf8 )*
		;

	# Signature fields: Flag, Expiration, Value are BASE32 sequences.
	# The Flag and Expiration uses the high bit to signal more to come
	# This means a starts with high values and ends with one low value.
	# The last value must not be zero ('A'), except when it occurs as
	# the first and only value.
	#
	# BASE32 values 0..31 encode to "ABCDEFGHIJKLMNOPQRSTUVWXYZ234567".
	# We are gentle enough to map '0' to 'O' and '1' to 'I' for typers.
	#
	b32field = [A-Za-z0-7]*
		${ *p = toupper (fc); }
		;
	b32subfield = b32field & ( [Aa] | [Q-Zq-z0-7]*[B-Pb-p] ) ;
	#
	sigflg = b32subfield
		>{ out->sigflags = 0; b32shift = 0; }
		${ out->sigflags |= (_b32charval (fc) & 0x0f) << b32shift;
		   b32shift += 4; }
%{ log_debug ("sigflags are 0x%04x", out->sigflags); }
		;
	sigexp = b32subfield when { (out->sigflags & A2ID_SIGFLAG_EXPIRATION) != 0 }
		>{ out->expireday = 0; b32shift = 0; }
		${ out->expireday |= (_b32charval (fc) & 0x0f) << b32shift;
		   b32shift += 4; }
%{ log_debug ("expireday at %d", out->expireday); }
		#TODO# find a way around the double test
		| "" when { (out->sigflags & A2ID_SIGFLAG_EXPIRATION) == 0 }
		;
	sigval = b32field ;

	# Optional signature: Must start and end with a '+' and have an
	# intermediate flag, optional expiration field and signature
	# value.  When expiration is not used, it can be combined into
	# the signature before proceeding, but this link is not made in
	# syntax but with some custom code right afterwards.
	#
	#TODO# Can this second call "_plustype (p)" be avoided with ":>" ???
	#
	opt_sig = ( '+' when { _plustype (p) == SWITCH_SIG }
			%{ SETOFS (out, p-1, OPEN_END );
			   SETOFS (out, p-1, PLUS_SIG );
			   SETOFS (out, p,   SIGFLAGS ); }
	            . sigflg
			%{ SETOFS (out, p,   SIGEXPIRE); }
	            . sigexp
			%{ SETOFS (out, p,   SIGVALUE ); }
	            . sigval
			%{ SETOFS (out, p,   SIGPLUS  ); }
	            . '+'
	          )
	          | ( ""
			%{ /* Setup offsets that were skipped */
			   SETOFS (out, p, OPEN_END );
			   SETOFS (out, p, PLUS_SIG );
			   SETOFS (out, p, SIGFLAGS );
			   SETOFS (out, p, SIGEXPIRE);
			   SETOFS (out, p, SIGVALUE );
			   SETOFS (out, p, SIGPLUS  ); }
	          ) ;

	# UTF-8 character, except ASCII, with shortest possible length.
	# See https://tools.ietf.org/html/rfc7542#section-2.1
	#
	# UTF8-xtra-char  =   UTF8-2 / UTF8-3 / UTF8-4
	#
	# UTF8-2          =   %xC2-DF UTF8-tail
	#
	# UTF8-3          =   %xE0 %xA0-BF UTF8-tail /
	#                     %xE1-EC 2( UTF8-tail ) /
	#                     %xED %x80-9F UTF8-tail /
	#                     %xEE-EF 2( UTF8-tail )
	#
	# UTF8-4          =   %xF0 %x90-BF 2( UTF8-tail ) /
	#                     %xF1-F3 3( UTF8-tail ) /
	#                     %xF4 %x80-8F 2( UTF8-tail )
	#
	# UTF8-tail       =   %x80-BF
	#
	utf8tail   = 0x80..0xbf ;
	#
	utf8char_2 = 0xc2..0xdf            utf8tail
	           ;
	utf8char_3 = 0xe0       0xa0..0xbf utf8tail
	           | 0xe1..0xec            utf8tail utf8tail
	           | 0xed       0x80..0x9f utf8tail
	           | 0xee..0xef            utf8tail utf8tail
	           ;
	utf8char_4 = 0xf0       0x90..0xbf utf8tail utf8tail
	           | 0xf1..0xf3            utf8tail utf8tail utf8tail
	           | 0xf4       0x80..0x8f utf8tail utf8tail
	           ;
	#
	utf8char = utf8char_2 | utf8char_3 | utf8char_4 ;

	# Word in the local-part, between '+' separators.
	#TODO# charset, utf-8, tolower_l()
	#
	word = ( [a-zA-Z0-9\!\#\$\%\'\*\/\=\?\^\_\`\{\|\}\~] | utf8char )+
		${ *p = tolower (fc); }
		;

 	# Label in the domain, between '.' separators.
	# See https://tools.ietf.org/html/rfc7542#section-2.2
	#
	# label          =  utf8-rtext *(ldh-str)
	# ldh-str        =  *( utf8-rtext / "-" ) utf8-rtext
	# utf8-rtext     =  ALPHA / DIGIT / UTF8-xtra-char
	#
	#TODO# tolower_l()
 	#
	labelchar = [a-zA-Z0-9] | utf8char
 		${ *p = tolower (fc); }
		;
	#
	label = labelchar . ( '-'? . labelchar )* ;

	# Argument and Alias: Same grammar but different use cases.
	# It starts with a '+' and is followed by a non-empty word.
	#
	pluswords = ( '+' when { _plustype (p) == SWITCH_ARG } . word )* ;
	svcargs = pluswords
		>{ SETOFS (out, p, PLUS_SVCARGS);
		   char *q = p;
		   if ((fc == '+') && _plustype (p) == SWITCH_ARG) q++;
		   SETOFS (out, q, SVCARGS); }
		;
	aliases = pluswords ;

	# Service and User: Similar grammar but different use cases.
	# Services start with a '+'.  Either may end in a signature.
	# The grammar requires backtracking to parse this, but only
	# once; an opt_sig starts and ends in a '+' if it exists,
	# but is easily mistaken for an extra arg or alias.  It is
	# the final '+' that causes this process to fail and track
	# back to an alternative interpretation with one less arg
	# or alias and with an opt_sig.
	#
	svcname = word
		>{ SETOFS (out, p, SERVICE); }
		;
	service = ( '+' . svcname . svcargs )
		>{ SETOFS (out, p, PLUS_SERVICE); }
		%{ /* Set offsets that will be skipped */
		   SETOFS (out, p,       USERID);
		   SETOFS (out, p, PLUS_ALIASES);
		   SETOFS (out, p,      ALIASES); }
		;
	#
	userid = word
		%{ SETOFS (out, p, PLUS_ALIASES);
		   char *q = p;
		   if ((fc == '+') && (_plustype (p) == SWITCH_ARG)) q++;
		   SETOFS (out, q, ALIASES); }
		>{ SETOFS (out, p, USERID);
		   /* Set offsets that were skipped */
		   SETOFS (out, p,      SVCARGS);
		   SETOFS (out, p, PLUS_SVCARGS);
		   SETOFS (out, p,      SERVICE);
		   SETOFS (out, p, PLUS_SERVICE); }
		;
	sepdots =    [^.] . ( '.'? . [^.] )*
		;
	user    =  ( userid . aliases ) & sepdots
		;

	# Local part: Either a service or a user, and an email-given
	# maximum length of 64 chars.
	#
	#TODO# may need to avoid double dots for NAI with -- ".."
	local = ( ( service | user ) . opt_sig ) & max64bytes
		;

	# Domain: Internationalised sequences with dots between words.
	# Punycode is sufficiently complex that it is not tested here.
	# A domain might be constructed that simply does not fit in DNS.
	# We do however care for the maximum number of 255 characters,
	# even if we ignore that some may be internatialised.
	# The "postdot_domain" entry point is used for parsing Selectors.
	#
	postdot_domain = ( label . ( '.' . label )* ) & max255chars
		>{ SETOFS (out, p,      DOMAIN); }
		;
	inidot_domain = postdot_domain
		>{ SETOFS (out, p-2, AT_DOMAIN); }
		;
	notdot_domain = postdot_domain
		>{ SETOFS (out, p-1, AT_DOMAIN); }
		;
	domain_with_dot = any* . '.' . any* ;
	domain = notdot_domain & domain_with_dot
		>{ SETOFS (out, p,  DOT_DOMAIN); }
		;

	# Identity: Composed of a local part, '@' and domain.
	# This is the main expression for an ARPA2 Identity.
	#
	identity := ( local . '@' . domain )
		>{ out->expireday = ~(uint32_t) 0; }
		%{ SETOFS (out, p+0,       END);
		   SETOFS (out, p+1,   BUFSIZE); }
		;

	# Remote Identity: The local part is not know to have
	# ARPA2 structure, and is not parsed (nor constrained)
	# by its format.  The entire localpart ends up in the
	# userid field (A2ID_OFS_USERID to _PLUS_ALIASES) and
	# none of the other semantic fields are used.  The
	# domain is still subject to iteration for remotes.
	#
	userid_remote = word & max64bytes
		>{ SETOFS (out, p, PLUS_SERVICE);
		   SETOFS (out, p, SERVICE     );
		   SETOFS (out, p, PLUS_SVCARGS);
		   SETOFS (out, p, SVCARGS     );
		   SETOFS (out, p, USERID      ); }
		%{ SETOFS (out, p, PLUS_ALIASES);
		   SETOFS (out, p, ALIASES     );
		   SETOFS (out, p, OPEN_END    );
		   SETOFS (out, p, PLUS_SIG    );
		   SETOFS (out, p, SIGFLAGS    );
		   SETOFS (out, p, SIGEXPIRE   );
		   SETOFS (out, p, SIGVALUE    );
		   SETOFS (out, p, SIGPLUS     ); }
		;

	identity_remote := userid_remote . '@' . domain
		>{ out->expireday = ~(uint32_t) 0;
		   out->sigflags = 0; }
		%{ SETOFS (out, p+0,        END);
		   SETOFS (out, p+1,    BUFSIZE); }
		;

	# Selector: Written in a mostly linear form, to allow for
	# better recovery due to the many optional parts.  Note that
	# the number of plusses in the local-part can be confusing:
	#
	#     "@." matches every local-part
	#    "+@." matches every local-part with an alias
	#   "++@." matches every local-part with a signature
	#  "+++@." matches every local-part with alias and signature
	#
	# Service names must always be mentioned to be matched, so
	# the form "+@" will not match to a service.
	#
	# Signatures may be presented as a prefix, so any BASE32
	# construction can be specified, up to full signatures.
	# There is no protection for desiring invalid matches.
	# The most likely use is to require certain sigflags.
	# Less likely, but still possible, is to exactly match
	# the entropy of a uniquely qualified identity.
	#
	# Aliases may be mentioned, but the last may be kept open
	# by specifying a trailing '+' separator that is not part
	# of a signature.  This last open alias may allow multiple
	# levels of aliasing.  This is why "+@." and "+++@." can
	# match every local-parth with an alias.  It is feeble to
	# specify an alias name when no username was given, and it
	# is invalid because it clutters up service matching.
	#

	sel_open_end = ('+' when { _plustype (p) == SWITCH_OPEN } )?
		>{ SETOFS (out, p, OPEN_END); }
		;

	sel_nodomain = ""
		>{ /* current pointing after "@." */
		   SETOFS (out, p,      DOMAIN);
		   SETOFS (out, p-2, AT_DOMAIN); }
		;

	sel_domain = ( ( notdot_domain | '.' . ( inidot_domain? | sel_nodomain ) ) & max255chars )
		>{ SETOFS (out, p,  DOT_DOMAIN); }
		;

	# Signatures in selectors can serve things like...
	#  - sigflg        -->  require minimum flags
	#  - 'A' . sigval  -->  exactly match an entropy
	# These can be met with a prefix, but we still want
	# a _bit_ of structure and set the proper offsets.
	sel_sig = ( '+' when { _plustype (p) == SWITCH_SIG }
			%{ SETOFS (out, p-1, PLUS_SIG );
			   SETOFS (out, p,   SIGFLAGS );
			   SETOFS (out, p,   SIGEXPIRE);
			   SETOFS (out, p,   SIGVALUE );
			   SETOFS (out, p,   SIGPLUS  ); }
	            . ( sigflg
			%{ SETOFS (out, p,   SIGEXPIRE); }
	            . ( sigexp
			%{ SETOFS (out, p,   SIGVALUE ); }
		    . ( sigval
			%{ SETOFS (out, p,   SIGPLUS  ); }
		      )?
		      )?
		      )?
		      # ) | "" %{ SETOFS (out, p, SIGVALUE ); }
		      # ) | "" %{ SETOFS (out, p, SIGEXPIRE); }
		      # )
	            . '+'
	          )
	          | ( ""
			%{ /* Setup offsets that were skipped */
			   SETOFS (out, p, PLUS_SIG );
			   SETOFS (out, p, SIGFLAGS );
			   SETOFS (out, p, SIGEXPIRE);
			   SETOFS (out, p, SIGVALUE );
			   SETOFS (out, p, SIGPLUS  ); }
	          ) ;

#	selector_remote := userid_remote? . '@' . domain
#		>{ out->expireday = ~(uint32_t) 0;
#		   out->sigflags = 0; }
#		%{ SETOFS (out, p+0,        END);
#		   SETOFS (out, p+1,    BUFSIZE); }
#		;

	sel_short = "+" ?
		>{ SETOFS (out, p, PLUS_SERVICE); }
		%{ SETOFS (out, p, SERVICE     );
		   SETOFS (out, p, PLUS_SVCARGS);
		   SETOFS (out, p, SVCARGS     );
		   SETOFS (out, p, USERID      );
		   SETOFS (out, p, PLUS_ALIASES);
		   SETOFS (out, p, ALIASES     ); }
		;

	sel_local = ( ( service | user | sel_short ) . sel_open_end . sel_sig ) & max64bytes ;
	selector := ( sel_local . '@' . sel_domain )
		>{ out->expireday = ~(uint32_t) 0; }
		%{ SETOFS (out, p+0,       END);
		   SETOFS (out, p+1,   BUFSIZE); }
		;

	# Remote Selector strings cannot match anything intelligent
	# in the low part?  TODO: NOT SURE, IT'S IN THE EYE OF THE
	# BEHOLDER [S/HE WHO WANTS TO IMPOSE A SELECTOR] ANYWAY.
	#TODO# For now, this copies selector for local identities.
	#
#	selector_remote := ( sel_local . '@' . sel_domain )
#		>{ out->expireday = ~(uint32_t) 0; }
#		%{ SETOFS (out, p+0,       END);
#		   SETOFS (out, p+1,   BUFSIZE); }
#		;

}%%


%% write data;


/* Initialise the ARPA2 Identity system.  This has a
 * mirror image for cleaup in a2id_fini().
 */
void a2id_init (void) {
	initialize_A2ID_error_table ();
}


/* Finalise the ARPA2 Identity system.  This may do
 * cleanup operations.
 */
void a2id_fini (void) {
	a2id_dropkeys ();
}

static bool a2id_parse_generic (a2id_t *out, const char *in, unsigned inlen, bool is_a2id) {
	assert ((in != NULL) && (out != NULL));
	//
	// Reset all offsets, and the trailing NUL
	memset (out, 0, sizeof (a2id_t));
	char *txt = out->txt;
	//
	// Avoid strings over the maximum available size
	if (inlen == 0) {
		inlen = strlen (in);
	}
	if (inlen > A2ID_MAXLEN) {
		errno = A2ID_ERR_SIZE;
		return false;
	}
	//
	// Fill out the string in the a2id_t structure
	memcpy (txt, in, inlen);
	txt [inlen] = '\0';
	//
	// Run the Ragel parser for "identity"
	int cs = is_a2id ? arpa2identity_en_identity : arpa2identity_en_identity_remote;
	char *p   = txt;
	char *pe  = txt + inlen;
	char *eof = pe;
	int max255 = 255;
	int max64 = 64;
	uint8_t b32shift = 0;
	%%{
		machine arpa2identity ;
		write init nocs ;
		write exec ;
	}%%
	bool ok = (cs >= arpa2identity_first_final) && (p == pe);
	//
	// Return whether we were successful
	if (!ok) errno = A2ID_ERR_IDENTITY_SYNTAX;
	return ok;
}


/** @brief Parse and normalise a string into an ARPA2 Identity.
 *
 * Return true if the input follows the correct grammar.
 * After this, offsets are setup in the .ofs field and
 * the normalised string, including NUL terminator, is
 * written into the .txt field.
 *
 * Normalised identities are in lowercase, except for
 * an optional +SIG+ signature part that will be in
 * upper case.  When we normalise such things beyond
 * ASCII, this is where c16n mappings should go.
 *
 * When inlen is set to 0, its value will be computed
 * with strlen() -- if it is higher than 0 then the
 * in string does not need NUL-termination and may be
 * part of a larger context.  The selector is copied
 * into the out value.
 */
bool a2id_parse (a2id_t *out, const char *in, unsigned inlen) {
	return a2id_parse_generic (out, in, inlen, true);
}


/** @brief Parse and normalise a string into a remote ARPA2 Identity.
 *
 * Return true if the input follows the correct grammar, where
 * the userid is not interpreted as for a local ARPA2 Identity.
 * The whole part before '@' ends up in the userid string.
 * The corresponding offsets are setup in the .ofs field and
 * the normalised string, including NUL terminator, is
 * written into the .txt field.
 *
 * Local parts are not case-normalised and no signatures can be
 * assumed to be part of the name.  The only parts that are
 * handled cleverly are the domain names.
 *
 * When inlen is set to 0, its value will be computed
 * with strlen() -- if it is higher than 0 then the
 * in string does not need NUL-termination and may be
 * part of a larger context.  The selector is copied
 * into the out value.
 */
bool a2id_parse_remote (a2id_t *out, const char *in, unsigned inlen) {
	return a2id_parse_generic (out, in, inlen, false);
}


/* Parse and normalise a string into an ARPA2 Selector.
 * Return true if the input follows the correct grammar.
 * After this, offsets are setup in the .ofs field and
 * the normalised string, including NUL terminator, is
 * written into the .txt field.
 *
 * Normalised identities are in lowercase, except for
 * an optional +SIG+ signature flags that will be in
 * upper case.  When we normalise such things beyond
 * ASCII, this is where c16n mappings should go.  As
 * part of signature validation, the normalisation
 * trims off anything beyond the signature _flags_
 * by looking at its self-termination marker.  This
 * can be used to compare against ARPA2 Identity with
 * a signature, to select the signature on strength.
 *
 * When inlen is set to 0, its value will be computed
 * with strlen() -- if it is higher than 0 then the
 * in string does not need NUL-termination and may be
 * part of a larger context.  The selector is copied
 * into the out value.
 */
bool a2sel_parse (a2sel_t *out, const char *in, unsigned inlen) {
	assert ((in != NULL) && (out != NULL));
	//
	// Reset all offsets, and the trailing NUL
	memset (out, 0, sizeof (a2sel_t));
	char *txt = out->txt;
	//
	// Avoid strings over the maximum available size
	if (inlen == 0) {
		inlen = strlen (in);
	}
	if (inlen > A2SEL_MAXLEN) {
		errno = A2ID_ERR_SIZE;
		return false;
	}
	//
	// Fill out the string in the a2sel_t structure
	memcpy (txt, in, inlen);
	txt [inlen] = '\0';
	//
	// Run the Ragel parser for "selector"
	int cs = arpa2identity_en_selector;
	char *p   = txt;
	char *pe  = txt + inlen;
	char *eof = pe;
	int max255 = 255;
	int max64 = 64;
	uint8_t b32shift = 0;
	%%{
		machine arpa2identity ;
		write init nocs ;
		write exec ;
	}%%
	bool ok = (cs >= arpa2identity_first_final) && (p == pe);
	//
	// Return whether we were successful
	if (!ok) errno = A2ID_ERR_SELECTOR_SYNTAX;
	return ok;
}


/* Internal macro to point strings at an offset in both
 * a general and special a2sel_t or a2id_t variable.
 */
#define SG_POINTERS(svar,gvar,idx) \
	const char const *svar = &special->txt [special->ofs [A2ID_OFS_ ## idx]]; \
	const char const *gvar = &general->txt [general->ofs [A2ID_OFS_ ## idx]];


/* Test if the left ARPA2 Selector is a specialised form of
 * the right ARPA2 Selector.
 *
 * One might read this as a subseteq operation placed as
 * a testing condition between the (sets of identities
 * that would match) the ARPA2 Selectors.
 *
 * Note that ARPA2 Selectors form partially ordered
 * subsetting relations; if the left is not more specific
 * than the right than it is not automatically true that
 * the right is more specific than the left.  They might
 * simply have incomplete overlap in both tests.
 *
 * Also note that the overlap can be perfect in both
 * tests when the two ARPA2 Selectors are identical.
 *
 * Return true when the first is more specific than
 * the second.  Return false if this is not the case.
 */
bool a2sel_special (const a2sel_t *special, const a2sel_t *general) {
	assert ((special != NULL) && (general != NULL));
	//
	// Compare signature flags and expiration (but ignore the value)
	if ((general->sigflags & ~special->sigflags) != 0) {
		//
		// Signature flags from general are missing in special
		errno = A2ID_ERR_SIGFLAGS_LOOSENESS;
		return false;
	}
	if ((general->sigflags & A2ID_SIGFLAG_EXPIRATION) != 0) {
		//
		// Both are aware of expiration
		if (general->expiration < special->expiration) {
			//
			// General expires before special, so less restrictive
			errno = A2ID_ERR_EXPIREDAY_LOOSENESS;
			return false;
		}
	}
	//
	// Compare domains -- special could be a domain or .domain
	SG_POINTERS (sdom0, gdom0, DOT_DOMAIN);
	SG_POINTERS (sdom1, gdom1,     DOMAIN);
	SG_POINTERS (sdomN, gdomN,        END);
	int gdomlen = gdomN - gdom0;
	int sdomlen = sdomN - sdom0;
	if (gdomlen > sdomlen) {
		//
		// Generic domain cannot be longer than special domain
		errno = A2ID_ERR_INCOMPATIBLE_DOMAIN;
		return false;
	} else if (gdomlen == sdomlen) {
		//
		// Same length means the domains must match exactly
		if (0 != memcmp (sdom0, gdom0, sdomlen)) {
			errno = A2ID_ERR_INCOMPATIBLE_DOMAIN;
			return false;
		}
	} else if (gdom0 == gdom1) {
		//
		// No '.' before shorter general domain, so no pattern
		errno = A2ID_ERR_INCOMPATIBLE_DOMAIN;
		return false;
	} else if ((gdomlen == 1) && (*gdom0 == '.')) {
		//
		// Only "@." found -- trivially accepted
		;
	} else /* (gdomlen < sdomlen) && ( gdom0 != gdom1 ) */ {
		//
		// Shorter general domain with initial '.', match special end
		if (0 != memcmp (sdomN - gdomlen, gdom0, gdomlen)) {
			errno = A2ID_ERR_INCOMPATIBLE_DOMAIN;
			return false;
		}
	}
	//
	// Compare the user+aliases part
	SG_POINTERS (susr0, gusr0, USERID  );
	SG_POINTERS (susrN, gusrN, PLUS_SIG);
	int susrlen = susrN - susr0;
	int gusrlen = gusrN - gusr0;
	if (gusrlen == 0) {
		//
		// Empty user, so "@..." form -- trivially accept
		;
	} else if (gusrlen > susrlen) {
		//
		// Generic user+aliases cannot be longer than special
		errno = A2ID_ERR_INCOMPATIBLE_USER;
		return false;
	} else if (gusrlen == susrlen) {
		//
		// Same length means the user+aliases must match exactly
		if (0 != memcmp (susr0, gusr0, susrlen)) {
			errno = A2ID_ERR_INCOMPATIBLE_USER;
			return false;
		}
	} else /* gusrlen < susrlen */ {
		//
		// Shorter general user+aliases must match the special start
		if (0 != memcmp (susr0, gusr0, gusrlen)) {
			errno = A2ID_ERR_INCOMPATIBLE_USER;
			return false;
		}
		//
		// Shorter general parts require a '+' in the special
		if (susr0 [gusrlen] != '+') {
			errno = A2ID_ERR_INCOMPATIBLE_USER;
			return false;
		}
	}
	//
	// Compare the service+args part
	SG_POINTERS (ssvc0, gsvc0, PLUS_SERVICE);
	SG_POINTERS (ssvcN, gsvcN, USERID      );
	int ssvclen = ssvcN - ssvc0;
	int gsvclen = gsvcN - gsvc0;
	if (gsvclen > ssvclen) {
		//
		// Generic service+svcargs cannot be longer than special
		errno = A2ID_ERR_INCOMPATIBLE_SERVICE;
		return false;
	} else if (gsvclen == ssvclen) {
		//
		// Same length means the service+svcargs must match exactly
		if (0 != memcmp (ssvc0, gsvc0, ssvclen)) {
			errno = A2ID_ERR_INCOMPATIBLE_SERVICE;
			return false;
		}
	} else /* gsvclen < ssvclen */ {
		//
		// Shorter general service+svcargs must match the special start
		if (0 != memcmp (ssvc0, gsvc0, gsvclen)) {
			errno = A2ID_ERR_INCOMPATIBLE_SERVICE;
			return false;
		}
		//
		// Shorter general parts require a '+' in the special
		if (susr0 [gsvclen] != '+') {
			errno = A2ID_ERR_INCOMPATIBLE_SERVICE;
			return false;
		}
	}
	//
	// Gosh, nothing in general was more specific than in special
	// So we can say that special is "subset or equal" of general
	return true;
}


/*
 * SIGNATURE LOGIC
 *
 * Signing and verifying is done by the same server, so the same key
 * can be assumed.  Replicated services must share keys.  We can use
 * symmetric algorithms: Hashes and encryption.
 *
 * Signing consists of blocks, each of which is prefixed with a length
 * in 16 bits.  These fields are added, in this order:
 *  - The domain name or dot-prefixed domain name pattern
 *  - The base user identity or +service name (TODO: encryption?)
 *  - The BASE32 string of sigflags and, if so flagged, expiration
 *  - The length of the signature field encode in 1 byte
 *  - The flagged extensions in order of appearance
 *    (except for the expiration which is already included)
 *
 * The expiration is relative to a day that is provided for the signing
 * key.  This helps to reduce the size of the expiration-days number.
 *
 * The domain and preceding local-part are included to mitigate the risk
 * use of signatures outside their original context.  This is generally
 * useful, but may be liften to application-logic with the sigflags.
 *
 * The inclusion of the size of the signature to be produced avoids
 * validity of a signature of reduced size.  This means that the signer
 * is in full control of the security level, and since that only requires
 * literal passing of addresses, this is not a problem at all.
 *
 * TODO: What is the meaning when sigflags are 0?
 *  - We might use only the static settings above; addresses can not
 *    be guessed anymore;
 *  - We might just setup a sigvalue with pure entropy; this allows
 *    many random addresses; however, this would require storing many
 *    addresses and not just computing them; a breach with the idea.
 */


/* Internal macro to retrieve hash input as a pointer and size,
 * going from one offset to another, based on an a2id_t pointer.
 */
#define IDBUF_DECLARE(pvar,lvar) \
	const uint8_t *pvar; uint16_t lvar;
#define IDBUF_FETCH(tbs,pvar,lvar,idx0,idxN) \
	pvar = tbs->txt                     + tbs->ofs [A2ID_OFS_ ## idx0]; \
	lvar = tbs->ofs [A2ID_OFS_ ## idxN] - tbs->ofs [A2ID_OFS_ ## idx0];


/* Internal routine for inserting a base32 string for an uint32_t,
 * using the self-descriptive ends of the sigflg and sigexp fields,
 * that is, set bit.4 to indicate more, and only the first may be 0.
 *
 * The output never exceeds 8 bytes and will not be NUL-terminated.
 * Instead, the *ofs is updated.
 */
static bool _int2base32 (char *str, uint16_t *ofs, uint32_t in) {
	do {
		uint8_t digit = in & 0x0000000f;
		in >>= 4;
		if (in != 0) {
			digit += 0x10;
		}
		str [ (*ofs)++ ] = base32table [digit];
	} while (in != 0);
	return true;
}


/* Shift text in an ARPA2 Selector.  The A2ID_OFS_xxx index into
 * the internal offset array is given, along with the value to
 * write to that offset.  The text will be shifted forward or
 * backward as required, so text is removed and/or a known size
 * is available to write from an earlier starting pointing.
 * Offsets are updated; later ones receive an offset and when
 * earlier ones are overwritten they will be squeezed down.
 * Use the before flag to indicate that the work should be
 * considered to be done before the indexed offset.
 */
bool a2sel_textshift (a2id_t *shifty, a2id_offset_t ofsidx,
			uint16_t newofs, bool before) {
	//
	// Figure out the change to the string length
	uint16_t oldofs = shifty->ofs [ofsidx];
	int change = ((int) newofs) - ((int) oldofs);
	int bufsz = shifty->ofs [A2ID_OFS_BUFSIZE];
	//
	// Be sure that we have that much space
	if (bufsz + change > (int) sizeof (shifty->txt)) {
		errno = A2ID_ERR_SIZE;
		return false;
	}
	//
	// Move the text in the buffer -- in eiher direction
	memmove (shifty->txt + newofs, shifty->txt + oldofs, bufsz - oldofs);
	//
	// Maintain zeroes in any space beyond the buffer end
	if (change < 0) {
		memset (shifty->txt + bufsz + change, 0, -change);
	}
	//
	// Update later and overwritten offsets to follow the moved text
	if (!before) {
		ofsidx++;
	}
	while (shifty->ofs [ofsidx-1] > newofs) {
		ofsidx--;
		shifty->ofs [ofsidx] = oldofs;	/* Effectively set to newofs */
	}
	while (ofsidx <= A2ID_OFS_BUFSIZE) {
		shifty->ofs [ofsidx] += change;
		ofsidx++;
	}
	//
	// We can return success
	return true;
}


/* Add a key to be used for signed identities.  The last
 * key added will be used for signatures.  Keys added
 * before it only serve as a validation option.  Call
 * this function as part of your initialisation, then
 * try to drop privileges to reach the key material.
 *
 * The key is read from a file descriptor, and should
 * not exceed PIPE_BUF from <limits.h> in size.  This
 * construct allows you to load from a pipe or fifo,
 * which can usually be read only once.  Keys should
 * fit easily into even the minimum PIPE_BUF in POSIX
 * specifications.
 *
 * Keys are obtained in a single read, and should end
 * in a newline (a simple completeness check).  Small
 * sizes are considered an error.  The key starts as
 * "arpa2id", comma, 3-level semantic version for the
 * first software to define the signature computation,
 * comma, algorithm name, comma, and finally the key's
 * entropy in any desired form desired.  We like to
 * use BASE32 but are mindful of case sensitivity of
 * this input.
 *
 * The file descriptor is not closed in this function;
 * you may continue to use it for other purposes.
 */
bool a2id_addkey (int keyfd) {
	//
	// Allocate the storage space for the key
	struct storedkey *newkey = _recycledkeys;
	if (newkey != NULL) {
		_recycledkeys = newkey->next;
		newkey->next = NULL;
	} else {
		newkey = calloc (1, sizeof (struct storedkey));
		if (newkey == NULL) {
			errno = ENOMEM;
			return false;
		}
	}
	bool ok = true;
	//
	// Start a digest in this buffer
	ok = ok && a2md_open (&newkey->seed);
	//
	// Consume the data to be hashed (even if not ok)
	char buf [PIPE_BUF + 1];
	ssize_t buflen = read (keyfd, buf, PIPE_BUF);
	ok = ok && (buflen > 50);
	ok = ok && (buf [buflen - 1] == '\n');
	//
	// Verify the beginning of the key: alg,a2idversion,
	char keystart [55];
	int  keystartlen = snprintf (keystart, 52,
				"arpa2id,%s,sigkey,%s,", "2.0.0", A2MD_ALGORITHM);
	ok = ok && (0 == strncmp (buf, keystart, keystartlen));
	//
	// Digest the entire message, including key day zero
	ok = ok && a2md_write_msg (&newkey->seed, buf, buflen);
	//
	// Wipe the buffer, and use it below to copy elsewhere
	memset (buf, 0, sizeof (buf));
	//
	// Enqueue if all went well
	if (ok) {
		newkey->next = _storedkeys;
		_storedkeys = newkey;
	} else {
		/* Check that we are not leaking stack */
		assert (sizeof (newkey) <= sizeof (buf));
		memcpy (newkey, buf, sizeof (struct storedkey));
		newkey->next = _recycledkeys;
		_recycledkeys = newkey;
	}
	//
	// Return success or failure
	if (!ok) errno = A2ID_ERR_KEY_ADD;
	return ok;
}


/* Wipe all keys clean.  Do not return the memory to the pool,
 * but on a recycling list so those zeroes seem really important
 * to the compiler.
 */
void a2id_dropkeys (void) {
	struct storedkey *key = _storedkeys;
	while (key != NULL) {
		a2md_close (&key->seed, NULL);
		key = key->next;
	}
	_recycledkeys = _storedkeys;
	_storedkeys = NULL;
}


/* Internal routine to compute a signature of the given
 * number of characters.  Uses a callback to the caller,
 * along with the already-existing a2id_t to sign.  The
 * information piles up in the mdstate.
 */
static bool _compute_signature (a2id_t *tbs,
		struct storedkey *key,
		a2id_sigdata_cb cb, void *cbdata,
		char *sigflg_sigexp, uint8_t siglen,
		char b32sig [64]) {
	bool ok = true;
	//
	// Setup a message digest buffer
	//TODO// Change to a2md_open_key()
	struct a2md_state mdstate;
	ok = ok && a2md_clone (&mdstate, &key->seed);
	//
	// Hash the domain name or dot-prefixed domain name pattern
	IDBUF_DECLARE (pdomain, ldomain);
	IDBUF_FETCH (tbs, pdomain, ldomain, DOT_DOMAIN, END);
	ok = ok && a2md_write_msg (&mdstate, pdomain, ldomain);
	//
	// Hash the local-part before the signature (TODO: encryption?)
	// Hash the base user identity or +service name (TODO: encryption?)
	IDBUF_DECLARE (plocal, llocal);
	IDBUF_FETCH (tbs, plocal, llocal, PLUS_SERVICE, PLUS_SVCARGS);
	if (llocal == 0) {
		IDBUF_FETCH (tbs, plocal, llocal, USERID, PLUS_ALIASES);
	}
	ok = ok && a2md_write_msg (&mdstate, plocal, llocal);
	//
	// Hash the BASE32 signature flags and, if flagged, expireday
	uint8_t *psigpfix = sigflg_sigexp;
	uint16_t lsigpfix = strlen (sigflg_sigexp);
	ok = ok && a2md_write_msg (&mdstate, psigpfix, lsigpfix);
	//
	// Hash the length of the signature field in 1 byte
	uint8_t *psiglen = &siglen;
	uint16_t lsiglen = 1;
	ok = ok && a2md_write_msg (&mdstate, psiglen, lsiglen);
	//
	// Hash the extensions in their numeric order (but skip expdays)
	for (a2id_sigdata_t sd = 1; sd < A2ID_SIGDATA_COUNT; sd++) {
		if ((tbs->sigflags & ( 1 << sd )) != 0) {
			char databuf [1024 + 3];
			uint16_t datalen = 1024;
			memset (databuf, 0, sizeof (databuf));
			ok = ok && (cb != NULL);
			ok = ok && cb (sd, cbdata, tbs, databuf, &datalen);
			if (ok) {
				if (datalen > 1024) {
					datalen = 1024;
				}
				ok = ok && a2md_write_msg (&mdstate, databuf, datalen);
				memset (databuf, 0, datalen);
			}
		}
	}
	//
	// Finish the hash computation -- where we determine the size
	uint8_t hashout [siglen];
	a2md_read_key (&mdstate, A2MD_STDKEY_SIGN, hashout, siglen);
	a2md_close (&mdstate, NULL);
	//
	// Map the hash to signature data
	for (int i = 0; i < siglen; i++) {
		b32sig [i] = base32table [hashout [i] & 0x1f];
	}
	//
	// Return success or failure
	return ok;
}


/* Callback functionality for a signed ARPA2 Identity.
 * Doubles as example code for a callback function.
 *
 * This function behaves as may be expected from a
 * a2id_sigdata_cb typed function and can indeed be
 * provided as such a callback to a2id_sign() and
 * a2id_verify() functions.  More elaborate callback
 * functions may also use the code provided herein
 * to derive the intended signature data.
 *
 * This function expects the remote as an a2id_t in
 * its cbdata, and it receives the local ARPA2 Identity
 * from the a2id_sign() or a2id_verify() operation,
 * and with that it can provide this sigdata:
 *
 *  - A2ID_SIGDATA_REMOTE_DOMAIN
 *  - A2ID_SIGDATA_REMOTE_USERID
 *  - A2ID_SIGDATA_LOCAL_ALIASES
 */
bool a2id_sigdata_base (a2id_sigdata_t sd,
			void *cbdata_rid, const a2id_t *lid,
			uint8_t *buf, uint16_t *buflen) {
	a2id_t *rid = (a2id_t *) cbdata_rid;
	//
	// Have a pointer and length as source reference
	IDBUF_DECLARE (dataptr, datalen);
	//
	// Fill the dataptr, datalen fields as requested by sd
	switch (sd) {
	//
	case A2ID_SIGDATA_REMOTE_DOMAIN:
		IDBUF_FETCH (rid, dataptr, datalen, DOMAIN, END);
		break;
	//
	case A2ID_SIGDATA_REMOTE_USERID:
		IDBUF_FETCH (rid, dataptr, datalen, PLUS_SERVICE, AT_DOMAIN);
		break;
	//
	case A2ID_SIGDATA_LOCAL_ALIASES:
		IDBUF_FETCH (lid, dataptr, datalen, PLUS_ALIASES, PLUS_SIG);
		if (datalen > 0) {
			break;
		}
		IDBUF_FETCH (lid, dataptr, datalen, PLUS_SVCARGS, PLUS_SIG);
		break;
	//
	default:
		log_error ("Request for invalid sigdata type in a2id_sigdata_base()");
		errno = A2ID_ERR_SIGDATA;
		return false;
	}
	//
	// Copy the data found, but no more than permitted
	if (buf == NULL) {
		;  /* Caller is length-probing and does not want data yet */
	} else if (datalen <= *buflen) {
		memcpy (buf, dataptr, datalen);
	} else if (*buflen > 0) {
		memcpy (buf, dataptr, *buflen);
	}
	//
	// In any case, we set the buflen to our ideal
	// (The caller will never take in more than its original value)
	*buflen = datalen;
	//
	// We have not failed if we got this far
	return true;
}


/* Possibly attach a signature to an ARPA2 Identity, in extra
 * characters in its username.  This silently succeeds when
 * there no key was loaded, and/or when the identity has no
 * signature flags requesting for data to be incorporated.
 *
 * The intention is to make it straightforward to integrate
 * this call in _every_ outgoing path, so that services will
 * facilitate signatures and leave the choice to clients.
 * Signature flags are gathered from an address provided by
 * the local client, and both it and the expiration may be
 * clipped to minimum and maximum values by the software
 * before passing it in here.
 *
 * A service that always calls a2id_sign() on outgoing traffic
 * should also always call a2id_verify() on incoming traffic.
 * This function is similarly permissive; users may require
 * signatures (on some aliases) through their ACL.
 * 
 * A signature would expand the length of the identity so its
 * local-part may span up to the 64 available characters.
 *
 * When the signature was requested and could be constructed,
 * return true and modify the tbs Identity.  When signing
 * failed, return false and unmodified buffer contents.
 *
 * The callback function will be called, with its private
 * data pointer, to retrieve context-dependent data from
 * the surrounding application.  This data may involve the
 * (stylised) content of subject headers, topics, ... as
 * defined in the sigdata enumeration type.
 *
 * Services will want to constrain the sigflags field to the
 * values that they can support before calling a2id_sign().
 * The sigflags is an OR-ed combination of A2ID_SIGFLAG_xxx.
 *
 * When flagging A2ID_SIGFLAG_EXPIRATION, provide a time on
 * the last valid day after today in expiration.
 * The a2id_sign() routine trims the value to a day number
 * since the 18000'th day after Jan 1st, 1970.
 *
 * Keys are supposed to be fixed in a server, and they are
 * found through static global variables.  You initially
 * load them with a2id_addkey() before dropping privileges.
 */
bool a2id_sign (a2id_t *tbs,
		a2id_sigdata_cb cb, void *cbdata) {
	bool ok = true;
	//
	// Quietly skip if no signature was requested
	if (tbs->sigflags == 0) {
		return true;
	}
	//
	// Quietly skip when no key was loaded; we shall
	// assume that any failure at key loading time was
	// intentionally waived.  Given how simple it is
	// to do this well, service software can choose to
	// be rather pedantic about this.
	//
	struct storedkey *key = _storedkeys;
	if (key == NULL) {
		// errno = A2ID_ERR_NOKEY;
		// return false;
		return true;
	}
	//
	// Produce the signature flags
	char sigflg_sigexp [20];
	uint16_t sigpos = tbs->ofs [A2ID_OFS_PLUS_SIG];
	uint16_t sigexp = 0;       // offset relative to sigflg
	ok = ok && _int2base32 (sigflg_sigexp, &sigexp, tbs->sigflags);
	//
	// Only include the sigexp chars when it was flagged
	uint16_t sigval = sigexp;  // offset relative to sigflg
	if (ok && ((tbs->sigflags & A2ID_SIGFLAG_EXPIRATION) != 0)) {
		//
		// The user may have modified the expiration time
		uint32_t usertime;
		if ((tbs->expiration == 0) && (tbs->expireday > 0)) {
			usertime = time (NULL) / 86400 + tbs->expireday;
		} else {
			usertime = tbs->expiration / 86400;
		}
		if (usertime < 18000) {
			/* Mark as expired... strange behaviour but passable */
			usertime = 0;
		} else {
			usertime -= 18000;
		}
		ok = ok && _int2base32 (sigflg_sigexp, &sigval, usertime);
	}
	sigflg_sigexp [sigval] = '\0';
	//
	// Compute the signature bytes
	int siglen = 64 - (sigpos + 1 + sigval + 1);
	char sigout [64];
	ok = ok && _compute_signature (tbs,
			key,
			cb, cbdata,
			sigflg_sigexp, siglen, sigout);
	//
	// Shift the part after the signature
	ok = ok && a2sel_textshift (tbs, A2ID_OFS_AT_DOMAIN, 64, true);
	//
	// We collected all data -- else return failure
	if (!ok) {
		errno = A2ID_ERR_SIGN;
		return false;
	}
	//
	// Successful -- commit data and offsets to a2id_t *tbs
	tbs->ofs [A2ID_OFS_PLUS_SIG ] = sigpos;
	tbs->txt [sigpos++] = '+';
	memcpy (tbs->txt + sigpos, sigflg_sigexp, sigval);
	tbs->ofs [A2ID_OFS_SIGFLAGS ] = sigpos;
	tbs->ofs [A2ID_OFS_SIGEXPIRE] = sigpos + sigexp;
	sigpos += sigval;
	tbs->ofs [A2ID_OFS_SIGVALUE ] = sigpos;
	memcpy (tbs->txt + sigpos, sigout, siglen);
	sigpos += siglen;
	tbs->ofs [A2ID_OFS_SIGPLUS  ] = sigpos;
	tbs->txt [sigpos++] = '+';
	assert (sigpos == tbs->ofs [A2ID_OFS_AT_DOMAIN]);
	assert (tbs->txt [sigpos] == '@');
	//
	// Report our success -- the a2id is consistent!
	return true;
}


/* Try to verify a signature on an ARPA2 Identity, where
 * an absent signature returns successfully because it is
 * left to later ACL filters to decide if a signature is
 * required.  When this service did not add keys it is
 * assumed that verification is done elsewehre.
 *
 * After returning successfully, the value in sigflags
 * indicates the flags that have been validated; this
 * is zeroed when nothing was validated.  Likewise, the
 * value returned in expiration is set after the day at
 * which the signature expires.
 *
 * Because it is so lenient, it is safe to always call
 * a2id_verify() on the incoming path.  This pairs up with
 * always calling a2id_sign() on the outgoing path.  To
 * the client, signatures are a feature that they can
 * choose to configure, if and where they desire it.
 *
 * Situations that do lead to errors being returned are
 * those where an address holds a signed Identity and at
 * least one key has been added to this server.  Multiple
 * keys may be loaded to allow for rollover procedures,
 * but when none validate the signature then a failure is
 * reported.  In this case, the sigflags are also zeroed
 * to spread no certainty.
 *
 * Some applications will want to enforce signatures.
 * They might look at the returned value in *sigflags
 * and at least test it to be non-zero, or they might
 * use an ARPA2 Selector that contains the signature
 * flags portion FLG in its grammar.  When sigflags
 * contiains A2ID_SIGFLAG_EXPIRATION, then expiration
 * is checked to not have passed.
 *
 * Keys are supposed to be fixed in a server, and they are
 * found through static global variables.  You initially
 * load them with a2id_addkey() before dropping privileges.
 */
bool a2id_verify (a2id_t *tbv,
		a2id_sigdata_cb cb, void *cbdata) {
	bool ok = true;
	tbv->expiration = 0;
	//
	// Retrieve lengths and verify their validity
	// The parser ensured order and size limits
	uint16_t sigflg_ofs = tbv->ofs [A2ID_OFS_SIGFLAGS ];
	uint16_t sigexp_ofs = tbv->ofs [A2ID_OFS_SIGEXPIRE];
	uint16_t sigval_ofs = tbv->ofs [A2ID_OFS_SIGVALUE ];
	uint16_t sigend_ofs = tbv->ofs [A2ID_OFS_SIGPLUS  ];
	uint16_t atsign_ofs = tbv->ofs [A2ID_OFS_AT_DOMAIN];
	//
	// Quietly agree to a completely absent signature
	if (sigflg_ofs == atsign_ofs) {
		return true;
	}
	//
	// Load the first key
	// Quietly accept if no keys were loaded into this server
	struct storedkey *key = _storedkeys;
	if (key == NULL) {
		// errno = A2ID_ERR_NOKEY;
		// return false;
		return true;
	}
	//
	// Require 4 sigflag bits and 40 bits of entropy
	uint16_t sigflg_len = sigexp_ofs - sigflg_ofs;
	uint16_t sigexp_len = sigval_ofs - sigexp_ofs;
	uint16_t sigval_len = sigend_ofs - sigval_ofs;
	ok = ok && (sigflg_len >= 1);
	ok = ok && (sigval_len >= 8);
	//
	// Retrieve the sigflg_sigexp bytes
	char sigflgexp [20];
	uint16_t  sigflgexp_len = sigflg_len + sigexp_len;
	ok = ok && (sigflgexp_len < sizeof (sigflgexp));
	if (ok) {
		memcpy (sigflgexp,
			tbv->txt + sigflg_ofs,
			sigflgexp_len);
		sigflgexp [sigflgexp_len] = '\0';
	}
	//
	// Iterate over the keys until one succeeds
	while (ok && (key != NULL)) {
		//
		// Compute the signature bytes
		char sigout [64];
		bool ok2 = true;
		ok2 = ok2 && _compute_signature (tbv,
				key,
				cb, cbdata,
				sigflgexp, sigval_len, sigout);
printf ("Computed %.*s\n", sigval_len, sigout);
printf ("Original %.*s\n", sigval_len, tbv->txt + sigval_ofs);
		//
		// Compare the signature bytes and fail if wrong
		ok2 = ok2 && (0 == memcmp (tbv->txt + sigval_ofs, sigout, sigval_len));
		//
		// If we were successful, do a final check on the expdays
		if (ok2) {
			if ((tbv->sigflags & A2ID_SIGFLAG_EXPIRATION) == 0) {
				tbv->expireday = 0;
				tbv->expiration = 0;
				return true;
			}
			/* Add an extra day to time expiration right _after_ the day */
			tbv->expiration  = (tbv->expireday + 18001);
			tbv->expiration *= 86400;
			tbv->expiration -= 1;
			if (tbv->expiration >= time (NULL)) {
				return true;
			} else {
				errno = A2ID_ERR_EXPIRED;
				return false;
			}
		}
		//
		// Try the next key
		key = key->next;
	}
	//
	// If we end up here, we failed to validate the signature
	key = NULL;
	errno = A2ID_ERR_INVALID_SIGNATURE;
	return false;
}


/* Iterate an ARPA2 Selector or ARPA2 Identity by
 * turning into an ever more abstract structure, but
 * the first bet is always the original structure.
 *
 * Signatures are always removed during iteration.
 * Just asking for presence of a signature adds
 * nothing.  And the signature flags are unsuitable
 * for iteration because that would call for the
 * (expensive) iteration over all possible subsets.
 * You can use the a2id_match() and variants to set
 * a lower bound on signatures.
 *
 * Domains are iterated by removing one label at a
 * time and starting with the dot after it.  The
 * final domain is "." or the most abstract one.
 *
 * Users for which we rely on ARPA2 semantics can
 * have aliases, so we can iterate by removing
 * one of these in turns, but leaving the + that
 * precedes it as an "open end".  The last of the
 * user patterns mentions just the user, and no
 * user at all.
 *
 * Services are always represented with their
 * name, but the arguments may be stripped one
 * by one, as for user aliases, and the + is
 * left as an "open end" in that case too.
 *
 * The outer loop of iteration (slow changes) are
 * for domain abstraction.  The inner loop (fast
 * changes) are for user/service iteration.  In
 * every iteration, the most abstract form is the
 * last and will be the "@." selector.
 *
 * The iterated value is stored in the cursor,
 * and updated on every _next invocation.  The
 * routines can be used like this (example for
 * an a2id_t, but a2sel_t works the same):
 *
 *	a2id_t me;
 *	a2sel_t crs;
 *	if (a2id_iterate_init (&me, &crs)) do {
 *		...process (crs)...
 *	} while (a2id_iterate_next (&me, &crs));
 *
 */
bool a2sel_iterate_init (const a2sel_t *start, a2sel_t *cursor) {
	bool ok = true;
	//
	// Clone the structure completely
	memcpy (cursor, start, sizeof (a2sel_t));
	//
	// Cut out the signature content, if any
	ok = ok && a2sel_textshift (cursor,
				// Leave the signature's plus signs...
				//NO// A2ID_OFS_SIGPLUS,
				//NO// cursor->ofs [A2ID_OFS_SIGFLAGS],
				// ...or drop the signature's plus signs too
				A2ID_OFS_AT_DOMAIN,
				cursor->ofs [A2ID_OFS_PLUS_SIG],
				true);
	//
	// Return success (or not)
	errno = A2ID_ERR_ITERATOR_FAILED;
	return ok;
}
//
bool a2sel_iterate_next (const a2sel_t *start, a2sel_t *cursor) {
	//
	// Differentiate between user and service
	uint16_t ofsidx;
	uint16_t lowidx;
	if (cursor->ofs [A2ID_OFS_SERVICE] != cursor->ofs [A2ID_OFS_PLUS_SERVICE]) {
		/* We shall try to reduce +svcargs */
		ofsidx = A2ID_OFS_USERID;
		lowidx = A2ID_OFS_PLUS_SVCARGS;
	} else {
		/* We shall try to reduce +aliases */
		ofsidx = A2ID_OFS_PLUS_SIG;
		lowidx = A2ID_OFS_PLUS_ALIASES;
	}
	//
	// Try to remove an alias/svcarg -- return if that worked
	uint16_t newofs = cursor->ofs [ofsidx];
	uint16_t minofs = cursor->ofs [lowidx];
#ifdef ITERATOR_OPEN_ENDED
	if (newofs > 0) {
		/* Avoid matching the same '+' open end as before */
		newofs--;
	}
#endif
	while (newofs > minofs) {
		//
		// Search until we find a separating '+'
		if (cursor->txt [--newofs] != '+') {
			continue;
		}
#ifdef ITERATOR_OPEN_ENDED
		//
		// This will be our new open end
		bool ok = a2sel_textshift (cursor, ofsidx, newofs + 1, true);
#else
		//
		// This will be our new cut-down alias
		bool ok = a2sel_textshift (cursor, ofsidx, newofs + 0, true);
#endif
		if (ok) {
			while (lowidx <= A2ID_OFS_OPEN_END) {
				if (cursor->ofs [lowidx] > newofs) {
					cursor->ofs [lowidx] = newofs;
				}
				lowidx++;
			}
			return true;
		} else {
			/* Return on succes; otherwise, break out */
			break;
		}
	}
	//
	// If this is a user, we can remove the local-part completely
	if ((lowidx == A2ID_OFS_PLUS_ALIASES) && (minofs > 0)) {
		bool ok = a2sel_textshift (cursor, A2ID_OFS_AT_DOMAIN, 0, true);
		if (ok) {
			/* Return on succes; otherwise, continue below */
			return true;
		}
	}
#ifdef USERNAME_REITERATION
	//
	// Restore the original username and/or service (all before _PLUS_SIG)
	assert (cursor->ofs [A2ID_OFS_PLUS_SIG] == cursor->ofs [A2ID_OFS_PLUS_SIG]);
	bool ok = true;
	ok = ok && (start->ofs [A2ID_OFS_PLUS_SIG] > cursor->ofs [A2ID_OFS_PLUS_SIG]);
	ok = ok && a2sel_textshift (cursor, A2ID_OFS_PLUS_SIG,
				start->ofs [A2ID_OFS_PLUS_SIG], true);
	if (ok) {
		/* Copy txt and ofs from start to cursor, up to _PLUS_SIG */
		memcpy (cursor->txt, start->txt, start->ofs [A2ID_OFS_PLUS_SIG]);
		memcpy (cursor->ofs, start->ofs, sizeof (uint16_t [A2ID_OFS_PLUS_SIG]));
		//TODO// Check that something changed (to avoid infinite looping)
		/* In either case, ontinue below */
	}
#endif
	//
	// Try to remove a domain level -- return if that worked
	char *old = cursor->txt + cursor->ofs [A2ID_OFS_DOT_DOMAIN];
	char *dot = cursor->txt + cursor->ofs [A2ID_OFS_DOMAIN    ];
	dot = strchr (dot, '.');
	if (dot != NULL) {
		int diffofs = dot - old;
		while (*dot != '\0') {
			*old++ = *dot++;
		}
		*old++ = '\0';
		cursor->ofs [A2ID_OFS_DOMAIN ]  = 1 + cursor->ofs [A2ID_OFS_DOT_DOMAIN];
		cursor->ofs [A2ID_OFS_END    ] -=     diffofs;
		cursor->ofs [A2ID_OFS_BUFSIZE] -=     diffofs;
		return true;
	}
	//
	// If we still have a last level left, remove that to form "@."
	if (old [1] != '\0') {
		*old++ = '.';
		*old   = '\0';
		cursor->ofs [A2ID_OFS_END    ] = 0 + cursor->ofs [A2ID_OFS_DOMAIN];
		cursor->ofs [A2ID_OFS_BUFSIZE] = 1 + cursor->ofs [A2ID_OFS_DOMAIN];
		return true;
	}
	//
	// We have no more iteration variables left -- return that we are done
	errno = A2ID_ERR_ITERATOR_DONE;
	return false;
}


/* Lightweight / Lightning-fast ARPA2 Selector and Identity
 * iteration.  This uses a simple structure with a pointer
 * to the userid and domain, with a length field for the
 * former and NUL termination for the latter.  This means
 * that a little more work is done by the user, but there
 * is less pressure on the iterator in terms of strings
 * being copied and offsets of the a2sel_t being kept up
 * to date.
 *
 * This mechanism is perfect for fast implementations, and
 * is specifically interesting for database lookups such as
 * the ACL logic of <arpa2/access.h>.
 *
 * You are supposed to fill the src value and ensure its
 * stability througout iteration.  The use is like:
 *
 *	a2sel_quickiter qiter;
 *	qiter.src = ...ref...;
 *	if (a2sel_iterate_init (&qiter)) do {
 *		...printf ("%.*s@%s", qiter.uidlen, qiter.uid, qiter.dom)...
 *	} while (a2sel_iterate_next (&qiter));
 */
bool a2sel_quickiter_init (a2sel_quickiter *iter) {
	iter->uid    = iter->src->txt;
	iter->uidlen = iter->src->ofs [A2ID_OFS_PLUS_SIG    ] -
	               iter->src->ofs [A2ID_OFS_PLUS_SERVICE];
	iter->dom    = iter->src->txt +
	               iter->src->ofs [A2ID_OFS_DOT_DOMAIN  ];
	return true;
}
//
bool a2sel_quickiter_next (a2sel_quickiter *iter) {
	//
	// Try to remove an alias/svcarg -- return if that worked
	uint16_t newofs = iter->uidlen;
	if (newofs > 0) {
		/* Avoid matching the same '+' open end as before */
		newofs--;
	}
	while (newofs > 1) {
		//
		// Search until we find a separating '+'
		if (iter->uid [--newofs] != '+') {
			continue;
		}
#ifdef ITERATOR_OPEN_ENDED
		//
		// This will be our new open end
		iter->uidlen = newofs + 1;
#else
		//
		// This will be our new cut-down alias
		iter->uidlen = newofs;
#endif
		return true;
	}
	//
	// If this is a user, we can remove the local-part completely
	if ((iter->uidlen > 0) && (*iter->uid != '+')) {
		iter->uidlen = 0;
		return true;
	}
#ifdef USERNAME_REITERATION
	//
	// Restore the original username and/or service (all before _PLUS_SIG)
	iter->uidlen = iter->src->ofs [A2ID_OFS_PLUS_SIG    ] -
	               iter->src->ofs [A2ID_OFS_PLUS_SERVICE];
#endif
	//
	// Try to remove a domain level -- return if that worked
	const char *dom = iter->dom;
	assert (*dom != '\0');
	if (*dom == '.') {
		dom++;
	}
	dom = strchr (dom, '.');
	if (dom != NULL) {
		iter->dom = dom;
		return true;
	}
	//
	// If we still have a last level left, remove that to form "@."
	if (iter->dom [1] != '\0') {
		iter->dom = ".";
		return true;
	}
	//
	// We have no more iteration variables left -- return that we are done
	errno = A2ID_ERR_ITERATOR_DONE;
	return false;
}


/* Count the number of abstractions to go from one ARPA2 Selector (or Identity)
 * to another ARPA2 Selector.  This is done as fast as possible, and no other
 * information is derived.  The return value is false when it is impossible
 * to make these steps, so it is another mechanism to test specialisation.
 *
 * Counting is not linear; to ensure that username abstractions count as less
 * influential, they are greatly outnumbered by domain name abstractions.
 * You can exploit this to infer the number of abstraction steps for the
 * domain and username, by passing a2sel_abstractions_domain() and _steps()
 * over the steps output here.
 *
 * The purpose of this function is to handle multiple matches, and learn
 * which is the closest match.  This can be practical when comparing an
 * Access Rule, such as one loaded from LDAP or in an application that defines
 * Access Rules in a static configuration.
 *
 * On success, the function returns true.  On failure, it returns false and
 * sets errno to a com_err code.  In either case, it sets steps to a value,
 * but failure will set the highest possible value, which is unreachable in
 * any other way.
 */
bool a2sel_abstractions (const a2sel_t *specific, const a2sel_t *generic, uint16_t *steps) {
	//
	// Initialise to ~0 steps, a safe bail-out default
	*steps = ~0;
	uint16_t ret_steps = 0;
	//
	// Map the generic one, for better comparison
	a2sel_quickiter target;
	target.src = generic;
	if (!a2sel_quickiter_init (&target)) {
		/* Replay error code */
		return false;
	}
	//
	// Iterate from specific to generic
	a2sel_quickiter qiter;
	qiter.src = specific;
	bool ok = a2sel_quickiter_init (&qiter);
	//
	// Move up until the userid has an interesting size
	while (ok && (qiter.uidlen > target.uidlen)) {
		ret_steps++;
		ok = a2sel_quickiter_next (&qiter);
	}
	if ((!ok) || (qiter.uidlen < target.uidlen)) {
		errno = (*qiter.uid == '+')
				? A2ID_ERR_INCOMPATIBLE_SERVICE
				: A2ID_ERR_INCOMPATIBLE_USER;
		return false;
	}
	//
	// Compare the uids which have the same length
	if (memcmp (qiter.uid, target.uid, qiter.uidlen) != 0) {
		errno = (*qiter.uid == '+')
				? A2ID_ERR_INCOMPATIBLE_SERVICE
				: A2ID_ERR_INCOMPATIBLE_USER;
		return false;
	}
	//
	// Move up until the domain has an interesting size
	const char *qiteryond = qiter.dom
				+  qiter.src->ofs [A2ID_OFS_END]
				-  qiter.src->ofs [A2ID_OFS_DOT_DOMAIN]
				- target.src->ofs [A2ID_OFS_END]
				+ target.src->ofs [A2ID_OFS_DOT_DOMAIN];
	while (ok && (qiter.dom [1] != '\0') && (qiter.dom < qiteryond)) {
		/* 128 is more than a 64-byte username could count
		 * and still fits the 255-byte domain abstraction
		 * maximum in the returned uint16_t space.
		 */
		ret_steps += 128;
		ok = a2sel_quickiter_next (&qiter);
	}
	//
	// Finish now if we received the "." domain
	if (target.src->ofs [A2ID_OFS_DOMAIN] == target.src->ofs [A2ID_OFS_END]) {
		goto success;
	}
	if ((!ok) || (qiter.dom != qiteryond)) {
		errno = A2ID_ERR_INCOMPATIBLE_DOMAIN;
		return false;
	}
	//
	// Compare the domains
	if (strcmp (qiter.dom, target.dom) != 0) {
		errno = A2ID_ERR_INCOMPATIBLE_DOMAIN;
		return false;
	}
	//
	// We succeeded.  Set the requested value and return success.
success:
	*steps = ret_steps;
	return true;
}

