#
#   SPDX-License-Identifier: BSD-2-Clause
#   SPDX-FileCopyrightText: Copyright 2020 Adriaan de Groot <groot@kde.org>

add_subdirectory(util)
add_subdirectory(tool)
