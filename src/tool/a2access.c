/* a2xs-tool.c -- Tool to work on Access Control Lists in a database.
 *
 * This tool can exchanges files with Access Control Lists with the
 * database used by the <arpa2/acess.h> functionality.
 *
 * The tool should be installed as "a2xs" or "a2access", or even "access".
 *
 * SPDX-License-Identifier: BSD-2-Clause
 * SPDX-FileCopyrightText: Copyright 2020 Rick van Rein <rick@openfortress.nl>
 */


#include <stdlib.h>
#include <stdint.h>
#include <stdbool.h>
#include <string.h>
#include <stdio.h>
#include <assert.h>

#include <fcntl.h>

#include <lmdb.h>

#include <arpa2/except.h>
#include <arpa2/digest.h>
#include <arpa2/identity.h>
#include <arpa2/access.h>
#include <arpa2/access_comm.h>
#include <arpa2/util/snoprintf.h>

#include "parse.h"
#include "grammar.h"

#include "arpa2/rules_db.h"



/***** Grammars for commands *****/


static const struct grammar comm_add_grammar = {
	.keywords = {
		"dbenv",	// dbenv
				// NO_dbname
		"dbserial",	// dbserial
		"dbkey",	// dbkey
				// NO_objtype
		"local",	// local
		"remote",	// remote
		NULL,		// iterate
		"rights",	// rights
		"list",		// list
		"alias",	// alias
		"signflags",	// signflags
	},
	.listwords = 0,
	.combinations = {
		0,
	},
};


static const struct grammar comm_del_grammar = {
	.keywords = {
		"dbenv",	// dbenv
				// NO_dbname
		"dbserial",	// dbserial
		"dbkey",	// dbkey
				// NO_objtype
		"local",	// local
		"remote",	// remote
		NULL,		// iterate
		"rights",	// rights
		"list",		// list
		"alias",	// alias
		"signflags",	// signflags
	},
	.listwords = 0,
	.combinations = {
		0,
	},
};


static const struct grammar comm_get_grammar = {
	.keywords = {
		"dbenv",	// dbenv
				// NO_dbname
		"dbserial",	// dbserial
		"dbkey",	// dbkey
				// NO_objtype
		"local",	// local
		"remote",	// remote
		NULL,		// iterate
		NULL,		// rights
		NULL,		// list
		NULL,		// alias
		NULL,		// signflags
	},
	.listwords = 0,
	.combinations = {
		0,
	},
};



/***** Helper routines *****/


/* Check RIGHTS syntax.  Map a LIST keyword to a RIGHTS value.
 * Return true on success, false on syntax error.
 */
bool comm_list2rights (struct parser *prs) {
	bool ok = true;
	if (prs->VAL_LIST == NULL) {
		ok = ok && ( prs->VAL_RIGHTS != NULL  );
		ok = ok && (*prs->VAL_RIGHTS != '\x00');
		for (char *rp = prs->VAL_RIGHTS; ok && (*rp != '\x00'); rp++) {
			ok = ok && (*rp >= 'A') && (*rp <= 'Z');
		}
	} else {
		ok = ok && (prs->VAL_RIGHTS == NULL);
		static const char *kv [] = {
			"white",    "W",
			"grey",     "C",
			"gray",     "C",
			"black",    "V",
			"honeypot", "K",
			NULL
		};
		if (ok) {
			ok = false;
			for (const char **kvp = kv; *kvp != NULL; kvp += 2) {
				if (0 == strcmp (*kvp, prs->VAL_LIST)) {
					prs->VAL_RIGHTS = (char *) kvp [1];
					ok = true;
				}
			}
		}
	}
	return ok;
}



/***** Subcommand routines *****/



// "communicate add", "communicate set"
int main_comm_add (int argc, char *argv []) {
	//
	// Parse argv[]
	struct parser prs = NEW_PARSER (&comm_add_grammar);
	bool ok = parse_argv (&prs, argc-3, argv+3);
	ok = ok && (prs.VAL_LOCAL  != NULL);
	ok = ok && (prs.VAL_REMOTE != NULL);
	ok = ok && comm_list2rights (&prs);
	if (!ok) {
		fprintf (stderr, "Usage: %s comm add [dbenv DIR] [dbserial NUM] [dbkey FILE] local ARPA2ID remote ARPA2SEL (list LIST|rights LETTERS) [alias ALIAS] [signflags LETTERS]\n", argv [0]);
		return 1;
	}
	bool test_first = (*argv[1] == 'a');
	//
	// Parse the local and remote identities
	a2id_t lid, rid;
	ok = ok && a2id_parse  (&lid, prs.VAL_LOCAL , strlen (prs.VAL_LOCAL ));
	ok = ok && a2sel_parse (&rid, prs.VAL_REMOTE, strlen (prs.VAL_REMOTE));
	//
	// The Access Name is different for user and service
	char     *ltxt = lid.txt;
	uint16_t *lofs = lid.ofs;
	char    *nameptr;
	unsigned namelen;
	if (ltxt [lofs [A2ID_OFS_PLUS_SERVICE]] == '+') {
		nameptr = ltxt                         + lofs [A2ID_OFS_PLUS_SERVICE];
		namelen = lofs [A2ID_OFS_PLUS_SVCARGS] - lofs [A2ID_OFS_PLUS_SERVICE];
	} else {
		nameptr = ltxt                    + lofs [A2ID_OFS_USERID];
		namelen = lofs [A2ID_OFS_ALIASES] - lofs [A2ID_OFS_USERID];
	}
	//
	// Copy the Access Name on stack to use as the Rules Name
	char name [namelen + 1];
	memcpy (name, nameptr, namelen);
	name [namelen] = '\0';
	//
	// Open ACLdb, using dbenv, dbserial, dbkey as needed
	struct rules_db ruledb;
	memset (&ruledb, 0, sizeof (ruledb));
	ok = ok && rules_dbopen (&ruledb, false, RULES_TRUNK_ANY);
	bool gotdb = ok;
	//
	// Compute the ACLdb lookup key
	rules_dbkey lookup1;
	rules_dbkey lookup2;
	rules_dbkey lookup3;
	MDB_val ruledb_key = {
		.mv_data = lookup2,
		.mv_size = A2MD_OUTPUT_SIZE,
	};
	char *dbkey = prs.VAL_DBKEY;
	int dbkeylen = 0;
	if (dbkey != NULL) {
		dbkeylen = strlen (dbkey);
	}
	log_detail ("lookup1 %p, dbkey %p, lid.txt %p + lid.ofs %d", lookup1, dbkey, lid.txt, lid.ofs [A2ID_OFS_DOMAIN]);
	ok = ok && rules_dbkey_domain (lookup1,
			dbkey, dbkeylen,
			lid.txt + lid.ofs [A2ID_OFS_DOMAIN]);
	ok = ok && rules_dbkey_service (lookup2, lookup1, sizeof (lookup1),
			access_type_comm);
	ok = ok && rules_dbkey_selector (lookup3, lookup2, sizeof (lookup2),
			name, &rid);
	//
	// Fill a buffer with the newly added rule
	ssize_t newrulelen = ok ? 0 : -1;
	char newrule [1024];
	newrulelen = snoprintf (newrule, sizeof (newrule), newrulelen,
			"#a2xs");
	//TODO// Optionally insert " =a<ALIAS><@-for-<NOALIASES>>"
	//TODO// Optionally insert " =s<SIGFLAGS>"
	//TODO// Optionally insert " =n<NATUID>"
	//TODO// Optionally insert " =o<OVRALI>"
	//TODO// Optionally insert " =g<GROUPID>"
	newrulelen = snoprintf (newrule, sizeof (newrule), newrulelen,
			" %%%s", prs.VAL_RIGHTS);
	ok = ok && (((unsigned long) newrulelen++) < sizeof (newrule));
	//
	// Read old data in the current database location
	MDB_val olddata;
	ok = ok && rules_dbget (&ruledb, lookup3, &olddata);
	//
	// Write to the database location
	MDB_val newdata = {
		.mv_data = newrule,
		.mv_size = newrulelen,
	};
	printf ("Adding \"%.*s\" to #%d old bytes\n", (int) newdata.mv_size, (char *) newdata.mv_data, (int) olddata.mv_size);
	log_data ("New data", newdata.mv_data, newdata.mv_size, 0);
	log_data ("Old data", olddata.mv_data, olddata.mv_size, 0);
	ok = ok && rules_dbset (&ruledb, &olddata, &newdata);
	printf ("Added  \"%.*s\" to #%d old bytes\n", (int) newdata.mv_size, (char *) newdata.mv_data, (int) olddata.mv_size);
	//
	// Commit the changes
	ok = ok && rules_dbcommit (&ruledb);
	//
	// Close ACLdb, if it is open
	if (gotdb) {
		rules_dbclose (&ruledb);
	}
	//
	// Return overall result
	return (ok ? 0 : 1);
}


// "communicate get"
int main_comm_get (int argc, char *argv []) {
	//
	// Parse argv[]
	struct parser prs = NEW_PARSER (&comm_get_grammar);
	bool ok = parse_argv (&prs, argc-3, argv+3);
	ok = ok && (prs.VAL_LOCAL  != NULL);
	printf ("ok=%d at %s:%d\n", ok, __FILE__, __LINE__);
	ok = ok && (prs.VAL_REMOTE != NULL);
	printf ("ok=%d at %s:%d\n", ok, __FILE__, __LINE__);
	if (!ok) {
		fprintf (stderr, "Usage: %s comm get [dbenv DIR] [dbserial NUM] [dbkey FILE] local ARPA2ID remote ARPA2SEL\n", argv [0]);
		return 1;
	}
	bool test_first = (*argv[2] == 'a');
	//
	// Parse the local and remote identities
	a2id_t lid, rid;
	ok = ok && a2id_parse  (&lid, prs.VAL_LOCAL , strlen (prs.VAL_LOCAL ));
	printf ("ok=%d at %s:%d\n", ok, __FILE__, __LINE__);
	ok = ok && a2sel_parse (&rid, prs.VAL_REMOTE, strlen (prs.VAL_REMOTE));
	printf ("ok=%d at %s:%d\n", ok, __FILE__, __LINE__);
	//
	// The Access Name is different for user and service
	char     *ltxt = lid.txt;
	uint16_t *lofs = lid.ofs;
	char    *nameptr;
	unsigned namelen;
	if (ltxt [lofs [A2ID_OFS_PLUS_SERVICE]] == '+') {
		nameptr = ltxt                         + lofs [A2ID_OFS_PLUS_SERVICE];
		namelen = lofs [A2ID_OFS_PLUS_SVCARGS] - lofs [A2ID_OFS_PLUS_SERVICE];
	} else {
		nameptr = ltxt                    + lofs [A2ID_OFS_USERID];
		namelen = lofs [A2ID_OFS_ALIASES] - lofs [A2ID_OFS_USERID];
	}
	//
	// Copy the Access Name on stack to use as the Rules Name
	char name [namelen + 1];
	memcpy (name, nameptr, namelen);
	name [namelen] = '\0';
	//
	// Open ACLdb, using dbenv, dbserial, dbkey as needed
	struct rules_db ruledb;
	memset (&ruledb, 0, sizeof (ruledb));
	ok = ok && rules_dbopen (&ruledb, true, RULES_TRUNK_ANY);
	printf ("ok=%d at %s:%d\n", ok, __FILE__, __LINE__);
	bool gotdb = ok;
	//
	// Compute the ACLdb lookup key
	rules_dbkey lookup1;
	rules_dbkey lookup2;
	rules_dbkey lookup3;
	MDB_val ruledb_key = {
		.mv_data = lookup2,
		.mv_size = A2MD_OUTPUT_SIZE,
	};
	char *dbkey = prs.VAL_DBKEY;
	int dbkeylen = 0;
	if (dbkey != NULL) {
		dbkeylen = strlen (dbkey);
	}
	ok = ok && rules_dbkey_domain (lookup1,
			dbkey, dbkeylen,
			lid.txt + lid.ofs [A2ID_OFS_DOMAIN]);
	printf ("ok=%d at %s:%d\n", ok, __FILE__, __LINE__);
	ok = ok && rules_dbkey_service (lookup2, lookup1, sizeof (lookup1),
			access_type_comm);
	printf ("ok=%d at %s:%d\n", ok, __FILE__, __LINE__);
	ok = ok && rules_dbkey_selector (lookup3, lookup2, sizeof (lookup2),
			name, &rid);
	printf ("ok=%d at %s:%d\n", ok, __FILE__, __LINE__);
	//
	// Read from the database location
	MDB_val stored;
	printf ("ok=%d at %s:%d\n", ok, __FILE__, __LINE__);
	ok = ok && rules_dbget (&ruledb, lookup3, &stored);
	printf ("ok=%d at %s:%d\n", ok, __FILE__, __LINE__);
	log_debug ("Retrieved #%d bytes from the database", (int) stored.mv_size);
	//
	// Print the value(s) retrieved
	if (ok) {
		char *rule;
		if (rules_dbloop (&stored, &rule)) do {
			printf ("Retrieved value: \"%s\"\n", rule);
		} while (rules_dbnext (&stored, &rule));
	} else {
		fprintf (stderr, "Failed to retrieve data\n");
	}
	//
	// Close ACLdb, if it is open
	if (gotdb) {
		rules_dbclose (&ruledb);
	}
	//
	// Return overall result
	return (ok ? 0 : 1);
}


// "communicate del"
int main_comm_del (int argc, char *argv []) {
	//
	// Parse argv[]
	struct parser prs = NEW_PARSER (&comm_add_grammar);
	bool ok = parse_argv (&prs, argc-3, argv+3);
	ok = ok && (prs.VAL_LOCAL  != NULL);
	ok = ok && (prs.VAL_REMOTE != NULL);
	ok = ok && comm_list2rights (&prs);
	if (!ok) {
		fprintf (stderr, "Usage: %s comm del [dbenv DIR] [dbserial NUM] [dbkey FILE] local ARPA2ID remote ARPA2SEL (list LIST|rights LETTERS) [alias ALIAS] [signflags LETTERS]\n", argv [0]);
		return 1;
	}
	bool test_first = (*argv[1] == 'a');
	//
	// Parse the local and remote identities
	a2id_t lid, rid;
	ok = ok && a2id_parse  (&lid, prs.VAL_LOCAL , strlen (prs.VAL_LOCAL ));
	ok = ok && a2sel_parse (&rid, prs.VAL_REMOTE, strlen (prs.VAL_REMOTE));
	//
	// The Access Name is different for user and service
	char     *ltxt = lid.txt;
	uint16_t *lofs = lid.ofs;
	char    *nameptr;
	unsigned namelen;
	if (ltxt [lofs [A2ID_OFS_PLUS_SERVICE]] == '+') {
		nameptr = ltxt                         + lofs [A2ID_OFS_PLUS_SERVICE];
		namelen = lofs [A2ID_OFS_PLUS_SVCARGS] - lofs [A2ID_OFS_PLUS_SERVICE];
	} else {
		nameptr = ltxt                    + lofs [A2ID_OFS_USERID];
		namelen = lofs [A2ID_OFS_ALIASES] - lofs [A2ID_OFS_USERID];
	}
	//
	// Copy the Access Name on stack to use as the Rules Name
	char name [namelen + 1];
	memcpy (name, nameptr, namelen);
	name [namelen] = '\0';
	//
	// Open ACLdb, using dbenv, dbserial, dbkey as needed
	struct rules_db ruledb;
	memset (&ruledb, 0, sizeof (ruledb));
	ok = ok && rules_dbopen (&ruledb, false, RULES_TRUNK_ANY);
	bool gotdb = ok;
	//
	// Compute the ACLdb lookup key
	rules_dbkey lookup1;
	rules_dbkey lookup2;
	rules_dbkey lookup3;
	MDB_val ruledb_key = {
		.mv_data = lookup2,
		.mv_size = A2MD_OUTPUT_SIZE,
	};
	char *dbkey = prs.VAL_DBKEY;
	int dbkeylen = 0;
	if (dbkey != NULL) {
		dbkeylen = strlen (dbkey);
	}
	log_detail ("lookup1 %p, dbkey %p, lid.txt %p + lid.ofs %d", lookup1, dbkey, lid.txt, lid.ofs [A2ID_OFS_DOMAIN]);
	ok = ok && rules_dbkey_domain (lookup1,
			dbkey, dbkeylen,
			lid.txt + lid.ofs [A2ID_OFS_DOMAIN]);
	ok = ok && rules_dbkey_service (lookup2, lookup1, sizeof (lookup1),
			access_type_comm);
	ok = ok && rules_dbkey_selector (lookup3, lookup2, sizeof (lookup2),
			name, &rid);
	//
	// Read old data in the current database location
	MDB_val olddata;
	ok = ok && rules_dbget (&ruledb, lookup3, &olddata);
	//
	// Write to the database location
	MDB_val newdata = {
		.mv_data = "",
		.mv_size = 0,
	};
	//
	// Look for the rule that needs to go
	if (ok) {
		char *rule;
		bool notfound = true;
		if (rules_dbloop (&olddata, &rule)) do {
			notfound = (0 != strcmp (rule, "tralala"));
		} while (notfound && rules_dbnext (&olddata, &rule));
		//
		// Error when not found, else setup for removal
		if (notfound) {
			fprintf (stderr, "Failed to find the rule that was to be removed\n");
			ok = false;
		} else {
			newdata.mv_data = rule + strlen (rule) + 1;
			newdata.mv_size = ((char *) olddata.mv_data) + olddata.mv_size - ((char *) newdata.mv_data);
			olddata.mv_size = rule - ((char *) olddata.mv_data);
		}
	}
	if (ok) {
		printf ("Writing %d + %d bytes\n", (int) olddata.mv_size, (int) newdata.mv_size);
		ok = ok && rules_dbset (&ruledb, &olddata, &newdata);
		printf ("Written %d + %d bytes\n", (int) olddata.mv_size, (int) newdata.mv_size);
	}
	//
	// Commit the changes
	ok = ok && rules_dbcommit (&ruledb);
	//
	// Close ACLdb, if it is open
	if (gotdb) {
		rules_dbclose (&ruledb);
	}
	//
	// Return overall result
	return (ok ? 0 : 1);
}



/***** Main program *****/


struct subsubcmdfun {
	char *subsubcmd;
	int (*subsubmain) (int argc, char *argv []);
};

struct subcmdfun {
	char *subcmd;
	struct subsubcmdfun *subsub;
};


struct subsubcmdfun main_comm [] = {
	{ "add", main_comm_add },
	{ "del", main_comm_del },
	{ "put", main_comm_add },
	{ "get", main_comm_get },
		/* end marker */
	{ NULL, NULL }
};

struct subcmdfun main_ [] = {
	{ "communicate", main_comm },
		/* end marker */
	{ NULL, NULL }
};



int main (int argc, char *argv []) {
	//
	// Require enough parameters
	if (argc < 3) {
		goto parser_error;
	}
	//
	// Look for argv [1]
	struct subcmdfun *subfun = main_;
	while (subfun->subcmd != NULL) {
		if (strncmp (argv [1], subfun->subcmd, strlen (argv [1])) == 0) {
			break;
		}
		subfun ++;
	}
	if (subfun->subcmd == NULL) {
		goto parser_error;
	}
	//
	// Look for argv [2]
	struct subsubcmdfun *subsubfun = subfun->subsub;
	while (subsubfun->subsubcmd != NULL) {
		if (strncmp (argv [2], subsubfun->subsubcmd, strlen (argv [2])) == 0) {
			break;
		}
		subsubfun++;
	}
	if (subfun->subcmd == NULL) {
		goto parser_error;
	}
	//
	// We found a function to call for the subsubmain
	int retval = subsubfun->subsubmain (argc, argv);
	return retval;
	//
	// We hit a parser error
parser_error:
	fprintf (stderr, "Usage: %s comm add|set|get ...\n", argv [0]);
	return 1;
}

