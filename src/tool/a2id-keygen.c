/* a2id-keygen -- Generate symmetric keys for signed identities.
 *
 * This program generates keys in the <arpa2/digest.h> format
 * and uses its state size to know how much to generate.  It
 * chooses to use BASE32 to represent the key.
 *
 * These keys follow a simple format:
 *  - the text "arpa2id"
 *  - comma
 *  - semantic version, 3 levels, of ARPA2 Identity to first release a computation
 *  - the text "sigkey"
 *  - comma
 *  - name for the algorithm
 *  - comma
 *  - comma
 *  - key material
 *  - newline
 * The newline is a check that the complete key came in.  The best
 * use is therefore to not include a newline in the key material.
 * The comma must not occur in the fields that have one following,
 * and any spacing are controls are literally handled.
 *
 * SPDX-License-Identifier: BSD-2-Clause
 * SPDX-FileCopyrightText: Copyright 2020 Rick van Rein <rick@openfortress.nl>
 */


#include <stdlib.h>
#include <stdio.h>

#include <sys/stat.h>
#include <sys/random.h>
#include <unistd.h>
#include <fcntl.h>
#include <limits.h>

#include <arpa2/digest.h>
#include <arpa2/entropy.h>


static const char base32table [] = "ABCDEFGHIJKLMNOPQRSTUVWXYZ234567";


int main (int argc, char *argv []) {
	//
	// Parse the commandline
	if (argc != 2) {
		fprintf (stderr, "Usage: %s /path/to/file|keyfile\n", argv [0]);
		exit (1);
	}
	char keyfilebuf [103];
	char *keyfile = argv [1];
	if (*keyfile != '/') {
		if (strchr (argv [1], '/') != NULL) {
			fprintf (stderr, "%s: Either use a path or leave out the slashes: %s\n", argv [0], keyfile);
			exit (1);
		}
		snprintf (keyfilebuf, 101, "/var/lib/arpa2/identity/%s.key", keyfile);
		keyfile = keyfilebuf;
	}
	//
	// Refuse to overwrite an existing file
	struct stat st;
	if ((stat (keyfile, &st) == 0) || (errno != ENOENT)) {
		fprintf (stderr, "%s: Refuse to overwrite existing key file %s\n", argv [0], keyfile);
		exit (1);
	}
	//
	// Construct random material
	a2entropy_init ();
	int  rndbufsize = (sizeof (struct a2md_state) * 8 + 4) / 5;
	char rndbuf [rndbufsize];
	if (!a2entropy_fast (rndbuf, rndbufsize)) {
		perror ("Failure generating entropy");
		exit (1);
	}
	for (int i = 0; i < rndbufsize; i++) {
		rndbuf [i] = base32table [rndbuf [i] & 0x1f];
	}
	a2entropy_fini ();
	//
	// Create the key in a buffer
	char outbuf [PIPE_BUF + 5];
	int outbuflen = snprintf (outbuf, PIPE_BUF + 1, "arpa2id,2.0.0,sigkey,%s,%.*s\n", A2MD_ALGORITHM, rndbufsize, rndbuf);
	//
	// Write the key to the desired file -- only owner-readable
	int outfile = open (keyfile, O_CREAT | O_EXCL | O_WRONLY, S_IRUSR);
	if (outfile < 0) {
		fprintf (stderr, "%s: Failed to create keyfile %s: %s\n", argv [0], keyfile, strerror (errno));
		exit (1);
	}
	ssize_t written = write (outfile, outbuf, outbuflen);
	if (written < 0) {
		fprintf (stderr, "%s: Failed to write keyfile %s: %s\n", argv [0], keyfile, strerror (errno));
		unlink (keyfile);
		exit (1);
	}
	if (written != outbuflen) {
		fprintf (stderr, "%s: Written %d bytes instead of %d to %s\n", argv [0], (int) written, outbuflen, keyfile);
		unlink (keyfile);
		exit (1);
	}
	close (outfile);
	//
	// Print that we wrote the file
	printf ("%s: Success.  We wrote the key to %s\n", argv [0], keyfile);
	exit (0);
}

