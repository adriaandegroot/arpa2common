/* a2xs tools grammar definitions
 *
 * SPDX-License-Identifier: BSD-2-Clause
 * SPDX-FileCopyrightText: Copyright 2020 Rick van Rein <rick@openfortress.nl>
 */



enum parse_variables {

	/* For database selection: */
	VAR_DBENV,	/* Database envdir, default "/var/lib/arpa2/access/" */
	//NO_DBNAME,	/* Database name in envdir, default NULL */
	VAR_DBSERIAL,	/* Default 0, overlay database versions for DUP_SORT */
	VAR_DBKEY,	/* Scatter/encryption key, default none */

	/* For selecting the database lookup keys: */
	//NO_OBJTYPE,	/* For instance "communication" (or abbreviations) */
	VAR_LOCAL,	/* Local identity: Source of domain (and userid) */
	VAR_REMOTE,	/* Remote identity: Starting point for iteration */
	VAR_ITERATE,	/* Remote identity, but intended for looping */

	/* Stored information and added constraints: */
	VAR_RIGHTS,	/* Access rights */
	VAR_LIST,	/* Comm list: black, white, gr[ae]y, honey */
	VAR_ALIAS,	/* Comm setup for particular local aliases/svcarg */
	VAR_SIGNFLAGS,	/* Comm: Required signature flags */

	/* End marker */
	VAR_ENDMARKER

};

#define VAR_FIRST	 VAR_DBENV
#define VAR_LAST	(VAR_ENDMARKER-1)


/* Values to be quickly referenced in the array */
#define VAL_DBENV	values [VAR_DBENV]
// #define VAL_DBNAME	values [VAR_DBNAME]
#define VAL_DBSERIAL	values [VAR_DBSERIAL]
#define VAL_DBKEY	values [VAR_DBKEY]
// #define VAL_OBJTYPE	values [VAR_OBJTYPE]
#define VAL_LOCAL	values [VAR_LOCAL]
#define VAL_REMOTE	values [VAR_REMOTE]
#define VAL_ITERATE	values [VAR_ITERATE]
#define VAL_RIGHTS	values [VAR_RIGHTS]
#define VAL_LIST	values [VAR_LIST]
#define VAL_ALIAS	values [VAR_ALIAS]
#define VAL_SIGNFLAGS	values [VAR_SIGNFLAGS]


/* Flags to compose in the combinations */
#define FLAG_DBENV	(1 << VAR_DBENV    )
// #define FLAG_DBNAME	(1 << VAR_DBNAME   )
#define FLAG_DBSERIAL	(1 << VAR_DBSERIAL )
#define FLAG_DBKEY	(1 << VAR_DBKEY    )
// #define FLAG_OBJTYPE	(1 << VAR_OBJTYPE  )
#define FLAG_LOCAL	(1 << VAR_LOCAL    )
#define FLAG_REMOTE	(1 << VAR_REMOTE   )
#define FLAG_ITERATE	(1 << VAR_ITERATE  )
#define FLAG_RIGHTS	(1 << VAR_RIGHTS   )
#define FLAG_LIST	(1 << VAR_LIST     )
#define FLAG_ALIAS	(1 << VAR_ALIAS    )
#define FLAG_SIGNFLAGS	(1 << VAR_SIGNFLAGS)



