/* Parse requests exchanged between RANtast and RANtasia processes.
 * The same logic can also parse the command line, with overlapping
 * storage variables, to simplify movements back and forth.
 *
 * The format is always a keyword and a value.  Later we may also allow
 * space-separated values.  Parsers may work on commandline args or on
 * lines in an ASCII or UTF-8 file.  The output is an array of strings
 * and the grammar is checked to be one of a set of provided strings.
 *
 * SPDX-License-Identifier: BSD-2-Clause
 * SPDX-FileCopyrightText: Copyright 2020 Rick van Rein <rick@openfortress.nl>
 */


#include <stdlib.h>
#include <stdbool.h>
#include <stdio.h>
#include <string.h>

#include <errno.h>

#include <arpa2/except.h>

#include "parse.h"
#include "grammar.h"



/* Move a string pointer to the next word, and return its length:
 *  - Add the previous length (initially this should be 0)
 *  - Add the number of spaces and tabs
 *  - At the end of line, return 0
 *  - Otherwise, return the length until space, tab or newline
 *
 * Note that an empty line returns length 0, as does the string end.
 *
 * Typical use in iteration:
 *
 *	unsigned wlen = 0;
 *	char *words = "... ... ...";
 *	while (wlen = parse_word (&words, wlen), wlen > 0) {
 *		printf ("Word: \"%.*s\"\n", wlen, words);
 *	}
 */
unsigned parse_word (char const **str, unsigned prevlen) {
	*str += prevlen;
	*str += strspn (*str, " \t");
	return (unsigned) strcspn (*str, " \t\n");
}


/* Add a keyword/value pair to a parser structure.
 *
 * Return true on success or false on failure for this isolated step.
 * Parsing may continue; failures accumulate in the parser structure.
 * Or you may use:  ok = ok && parse_keyword_value (...);
 *
 * Values must not occur more than once, except with the same value.
 * This enables things like setting the "reqid" field, use it to find
 * more and then load a file containing that same value.  It also
 * allows merging of a static request with its runtime data.
 */
bool parse_keyword_value (struct parser *prs, char *keyword, char *value) {
	log_debug ("Parsing keyword \"%s\" with value \"%s\"", keyword, value);
	//
	// Iterate over keywords in the grammar
	const char * const *kwdv = prs->grammar->keywords;
	for (int kwdi = 0; kwdi < 32 ; kwdi++, kwdv++) {
		//
		// See if the keyword is a match
		if (*kwdv == NULL) {
			continue;
		}
		if (strcasecmp (keyword, *kwdv) != 0) {
			continue;
		}
		//
		// Avoid repeated keywords with different values
		if ((prs->values [kwdi] != NULL) &&
					(0 != strcmp (prs->values [kwdi], value))) {
			//MODEST// log_error ("Attempt to change keyword %s value", keyword);
			log_error ("Attempt to change keyword %s value from \"%s\" to \"%s\"", keyword, prs->values [kwdi], value);
			goto fail;
		}
		//
		// Avoid non-printable-ASCII and only allow spaces in values
		// when the keyword is flagged in grammar->listwords
		for (char *cursor = value; *cursor != '\0'; cursor++) {
			if (*cursor == ' ') {
				if ((prs->grammar->listwords & (1 << kwdi)) == 0) {
					log_error ("Not a spaced-list keyword: %s", keyword);
					goto fail;
				}
			} else if ((*cursor < 32) || (*cursor >= 127)) {
				log_error ("Value contains illegal character code 0x%02x", (int) *cursor);
				goto fail;
			}
		}
		//
		// Take note of the keyword
		prs->values [kwdi] = value;
		//
		// Take note of the keyword presence
		prs->combination |= (1 << kwdi);
	}
	return true;
	//
	// Report failure
fail:
	prs->error = true;
	return false;
}


/* Find the grammar combination parsed through key/value paris.
 * The return value is < MAX_COMBINATIONS when properly matched.
 *
 * The return value is >= MAX_COMBINATIONS on error, or otherwise
 * a valid index into the combinations array of the grammar, to be
 * used to quickly decide how to proceed.  The caller who provides
 * a static set of combinations can statically match the outcome
 * and report a grammar problem as a fallback.
 */
unsigned parse_combination (struct parser *prs) {
	//
	// Return an invalid value when the parser accumulated an error
	if (prs->error) {
		return ~0;
	}
	//
	// Start in the list of combinations
	const uint32_t *combilist = prs->grammar->combinations;
	//
	// The empty combination is only accepted as a first combination
	if (prs->combination == 0) {
		if (*combilist != 0) {
			log_error ("Missing keyword/value pairs");
			return ~0;
		}
	}
	//
	// Iterate over all combinations to see which one matces
	for (int combi = 0; combi < MAX_COMBINATIONS ; combi++) {
		if (*combilist++ == prs->combination) {
			return combi;
		}
	}
	//
	// Unrecognised combination, report syntax error
	log_error ("Unsupported keyword combination");
	return ~0;
}


/* Parse an argv[] array with argc entries, starting at argv [0].
 * Require an even number in argc.  Return true on success.
 */
bool parse_argv (struct parser *prs, int argc, char **argv) {
	if ((argc & 1) != 0) {
		log_error ("Keywords and values do not occur in pairs");
		return false;
	}
	for (int argi = 0; argi < argc; argi += 2) {
		parse_keyword_value (prs, argv [argi+0], argv [argi+1]);
	}
	return !prs->error;
}


/* Parse the presented text string.  This must be formatted
 * as produced by unparse(), with pretty printing if so desired.
 * The return value is an individual success, and information is
 * logged and accumulated in the parser object.  Note that the
 * parsed format ends in an empty line to mark completeness.
 *
 * This function updates the backstore with NUL characters to
 * separate keywords and values.  Parsing again would fail.
 */
bool parse_string (struct parser *prs, char *text) {
	//
	// Iterate over the text
	char *keyword = text;
	while (*keyword != '\n') {
		//
		// If this is the string end, something broke down
		if (*keyword == '\0') {
			goto fail;
		}
		//
		// Find the space or tab between keyword and value
		char *value = keyword + strcspn (keyword, " \t\n");
		if ((value == NULL) || (*value == '\n')) {
			goto fail;
		}
		*value++ = '\0';
		//
		// Quietly skip over superfluous spaces and tabs
		value += strspn (value, " \t");
		//
		// Find the next line and current line size
		char *nextline = strchr (value, '\n');
		if (nextline == NULL) {
			goto fail;
		}
		*nextline++ = '\0';
		//
		// We now have a keyword and a value
		if (!parse_keyword_value (prs, keyword, value)) {
			goto fail;
		}
		//
		// Move on to the next line
		keyword = nextline;
	}
	//
	// After parsing the empty line, ensure that the string ends
	if (keyword [1] != '\0') {
		goto fail;
	}
	//
	// Terminate succesfully
	return true;
	//
	// Report parser failure
fail:
	log_error ("String parsing failed");
	prs->error = true;
	return false;
}


/* Parse the textual backstore content.  This must be formatted
 * as produced by unparse(), with pretty printing if so desired.
 * The return value is an individual success, and information is
 * logged and accumulated in the parser object.  Note that the
 * parsed format ends in an empty line to mark completeness.
 *
 * This function updates the backstore with NUL characters to
 * separate keywords and values.  Parsing again would fail.
 */
bool parse_backstore (struct parser *prs) {
	return parse_string (prs, prs->backstore);
}


/* Fill a string buffer with request, ending with a NUL character.
 *
 * The return value is like for snprintf(), and indicates the number of
 * characters excluding the NUL terminator.  Also like snprintf(), writes
 * never run outside the buffer space.  You can safely supply a length 0
 * and/or a buffer NULL to only determine the number of characters needed:
 *
 *	struct parser *prs = ...fill.data...;
 *	unsigned txtsz = unparse (prs, NULL, 0);
 *	unsigned bufsz = txtsz + 1;
 *      char buf [bufsz];
 *	unparse (prs, buf, bufsz);
 *	fputs (bufsz, stdout);
 *
 * The produced string ends in an empty line, which can be used to check
 * that a request arrived completely, rather than having been cut short
 * in transmission.
 *
 * The pretty flag aligns the keywords in straight tabular form.
 */
unsigned unparse (const struct parser *prs, char *opt_buf, unsigned buflen, bool pretty) {
	unsigned totlen = 0;
	int restlen = buflen;
	//
	// Iterate over all variables
	for (int varnr = VAR_FIRST; varnr <= VAR_LAST; varnr++) {
		//
		// Skip printing variables without a value
		if (prs->values [varnr] == NULL) {
			continue;
		}
		//
		// Be sure to have buffer space to print to
		char tmpbuf [10];
		if ((opt_buf == NULL) || (restlen <= 0)) {
			opt_buf =         tmpbuf ;
			restlen = sizeof (tmpbuf);
		}
		//
		// Strict program correctness: Do not write NULL keys
		assertxt (prs->grammar->keywords [varnr] != NULL,
				"Grammar has no keywords[%d] for value %s",
				varnr, prs->values [varnr]);
		//
		// Print a single field to the buffer
		int eltlen = snprintf (opt_buf, restlen,
					pretty ? "%-9s %s\n" : "%s %s\n",
					prs->grammar->keywords [varnr],
					prs->values [varnr]);
		if (eltlen < 0) {
			log_errno ("Failure to unparse RANtast request");
			return 0;
		}
		//
		// Update lengths and buffer position
		totlen  += eltlen;
		restlen -= eltlen;
		opt_buf += eltlen;
	}
	//
	// Append an empty line to allow incompleteness detection
	totlen += 1;
	if (restlen >= 2) {
		opt_buf [0] = '\n';
		opt_buf [1] = '\0';
	}
	//
	// Return the number of bytes that were / would have been printed
	return totlen;
}

