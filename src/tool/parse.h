/* Parse requests exchanged between RANtast and RANtasia processes.
 * The same logic can also parse the command line, with overlapping
 * storage variables, to simplify movements back and forth.
 *
 * The format is always a keyword and a value.  Later we may also allow
 * space-separated values.  Parsers may work on commandline args or on
 * lines in an ASCII or UTF-8 file.  The output is an array of strings
 * and the grammar is checked to be one of a set of provided strings.
 *
 * SPDX-License-Identifier: BSD-2-Clause
 * SPDX-FileCopyrightText: Copyright 2020 Rick van Rein <rick@openfortress.nl>
 */


#include <stdint.h>
#include <stdbool.h>


#define MAX_COMBINATIONS 10



/* Parse up to 32 different keywords.  The words are in the position
 * in the output array.  Non-parsed entries are set to NULL.
 * The FLAG_kwd bits in listwords mark keywords that welcome values
 * with spaces to separate list items, usable with parse_word().
 *
 * Recognise up to MAX_COMBINATIONS of keywords or otherwise report
 * an error in the grammar.  Though combinations may be valued 0
 * to indicate acceptance of nothing at all, this is only correct
 * in the first position; this allows simple static initialisation.
 */
struct grammar {
	const char *keywords [32];
	const uint32_t listwords;
	const uint32_t combinations [MAX_COMBINATIONS];
};


/* Parse and bind up to 32 different values, at indexes marked by
 * keywords.  The structure must be initialised with zeroes and then
 * the grammar pointer should be set.  At that point, the parser can
 * be used to pile up keyword/value pairs.  When done, a test for
 * the occurring combinations completes parsing.  This can be done
 * in one stroke with the NEW_PARSER(_grammar) macro below.
 */
struct parser {
	const struct grammar *grammar;
	char *values [32];
	char *backstore;
	uint32_t combination;
	bool error;
};


#define NEW_PARSER(_grammar) { .grammar = (_grammar), .values = { NULL }, .combination = 0, .error = false, .backstore = NULL }



/* Add a keyword/value pair to a parser structure.
 *
 * Return true on success or false on failure for this isolated step.
 * Parsing may continue; failures accumulate in the parser structure.
 * Or you may use:  ok = ok && parse_keyword_value (...);
 */
bool parse_keyword_value (struct parser *prs, char *keyword, char *value);


/* Find the grammar combination parsed through key/value paris.
 * The return value is < MAX_COMBINATIONS when properly matched.
 *
 * The return value is >= MAX_COMBINATIONS on error, or otherwise
 * a valid index into the combinations array of the grammar, to be
 * used to quickly decide how to proceed.  The caller who provides
 * a static set of combinations can statically match the outcome
 * and report a grammar problem as a fallback.
 */
unsigned parse_combination (struct parser *prs);


/* Parse an argv[] array with argc entries, starting at argv [0].
 * Require an even number in argc.  Return true on success.
 */
bool parse_argv (struct parser *prs, int argc, char **argv);


/* Move a string pointer to the next word, and return its length:
 *  - Add the previous length (initially this should be 0)
 *  - Add the number of spaces and tabs
 *  - At the end of line, return 0
 *  - Otherwise, return the length until space, tab or newline
 *
 * Note that an empty line returns length 0, as does the string end.
 *
 * Typical use in iteration:
 *
 *      unsigned wlen = 0;
 *      char *words = "... ... ...";
 *      while (wlen = parse_word (&words, wlen), wlen > 0) {
 *              printf ("Word: \"%.*s\"\n", wlen, words);
 *      }
 */
unsigned parse_word (const char **str, unsigned prevlen);


/* Parse the presented text string.  This must be formatted
 * as produced by unparse(), with pretty printing if so desired.
 * The return value is an individual success, and information is
 * logged and accumulated in the parser object.  Note that the
 * parsed format ends in an empty line to mark completeness.
 *
 * This function updates the backstore with NUL characters to
 * separate keywords and values.  Parsing again would fail.
 */
bool parse_string (struct parser *prs, char *text);


/* Parse the textual backstore content.  This must be formatted
 * as produced by unparse(), with pretty printing if so desired.
 * The return value is an individual success, and information is
 * logged and accumulated in the parser object.  Note that the
 * parsed format ends in an empty line to mark completeness.
 *
 * This function updates the backstore with NUL characters to
 * separate keywords and values.  Parsing again would fail.
 */
bool parse_backstore (struct parser *prs);


/* Fill a string buffer with request, ending with a NUL character.
 *
 * The return value is like for snprintf(), and indicates the number of
 * characters excluding the NUL terminator.  Also like snprintf(), writes
 * never run outside the buffer space.  You can safely supply a length 0
 * and/or a buffer NULL to only determine the number of characters needed:
 *
 *	struct parser *prs = ...fill.data...;
 *	unsigned txtsz = unparse (prs, NULL, 0);
 *	unsigned bufsz = txtsz + 1;
 *      char buf [bufsz];
 *	unparse (prs, buf, bufsz);
 *	fputs (bufsz, stdout);
 *
 * The produced string ends in an empty line, which can be used to check
 * that a request arrived completely, rather than having been cut short
 * in transmission.
 *
 * The pretty flag aligns the keywords in straight tabular form.
 */
unsigned unparse (const struct parser *prs, char *opt_buf, unsigned buflen, bool pretty);



