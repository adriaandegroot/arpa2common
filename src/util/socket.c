/* Socket utilities, including parsing and sockaddr juggling.
 *
 * SPDX-License-Identifier: BSD-2-Clause
 * SPDX-FileCopyrightText: Copyright 2020 Rick van Rein <rick@openfortress.nl>
 * SPDX-FileCopyrightText: Copyright 2020 Henri Manson <info@mansoft.nl>
 */


#include <stddef.h>
#include <stdlib.h>
#include <limits.h>
#include <assert.h>
#include <string.h>

#include <unistd.h>
#include <fcntl.h>

#include <errno.h>

#include <netinet/in.h>

#include <arpa2/com_err.h>
#include <arpa2/except.h>

#include <arpa2/socket.h>


/* Given a socket address, determine its length.
 *
 * This function does not fail.
 *
 * TODO:inline
 */
socklen_t socket_addrlen (const struct sockaddr_storage *ss) {
	assert ((ss->ss_family == AF_INET6) || (ss->ss_family == AF_INET));
	if (ss->ss_family == AF_INET6) {
		return sizeof (struct sockaddr_in6);
	} else {
		return sizeof (struct sockaddr_in );
	}
}


/* Given a socket address, determine its port.  In case of a parsed
 * address, this may well be a 0 port for default or random allocation.
 *
 * This function does not fail.
 *
 * TODO:inline
 */
uint16_t socket_port (const struct sockaddr_storage *ss) {
	assert ((ss->ss_family == AF_INET6) || (ss->ss_family == AF_INET));
	if (ss->ss_family == AF_INET6) {
		return htons (((struct sockaddr_in6 *) ss)->sin6_port);
	} else {
		return htons (((struct sockaddr_in  *) ss)->sin_port );
	}
}


/* Store a raw address from a given family in a socket address,
 * together with a port that may be set to 0 as a catch-all.
 */
bool socket_address (sa_family_t af, uint8_t *addr, uint16_t portnr, struct sockaddr_storage *ss) {
	ss->ss_family = af;
	memset (ss, 0, socket_addrlen (ss));
	ss->ss_family = af;
	switch (af) {
	case AF_INET6:
		memcpy (&((struct sockaddr_in6 *) ss)->sin6_addr, addr, 16);
		((struct sockaddr_in6 *) ss)->sin6_port = htons (portnr);
		return true;
	case AF_INET:
		log_debug ("socket address (%d.%d.%d.%d, %d)", addr [0], addr [1], addr [2], addr [3], portnr);
		memcpy (&((struct sockaddr_in  *) ss)->sin_addr,  addr,  4);
		((struct sockaddr_in  *) ss)->sin_port  = htons (portnr);
		return true;
	default:
		break;
	}
	errno = EINVAL;
	return false;
}


/* Parse an address and port, and store them in a sockaddr of
 * type AF_INET or AF_INET6.  The space provided is large enough
 * to hold either, as it is defined as a union.
 *
 * The opt_port may be NULL, in which case the port is set to 0
 * in the returned sockaddr; otherwise, its value is rejected
 * if it is 0.
 *
 * We always try IPv6 address parsing first, but fallback to
 * IPv4 if we have to, but that fallback is deprecated.  The
 * port will be syntax-checked and range-checked.
 *
 * Return true on success, or false with errno set on error.
 */
bool socket_parse (char *addr, char *opt_port, struct sockaddr_storage *out_sa) {
	//
	// Optional port parsing
	uint16_t portnr = 0;
	if (opt_port != NULL) {
		long p = strtol (opt_port, &opt_port, 10);
		if (*opt_port != '\0') {
			errno = EINVAL;
			return false;
		}
		if ((p == LONG_MIN) || (p == LONG_MAX) || (p <= 0) || (p > 65535)) {
			/* errno is ERANGE */
			return false;
		}
		portnr = (uint16_t) p;
	}
	//
	// IPv6 address parsing
	uint8_t raw_addr [16];
	switch (inet_pton (AF_INET6, addr, raw_addr)) {
	case 1:
		return socket_address (AF_INET6, raw_addr, portnr, out_sa);
	case 0:
		break;
	default:
		break;
	}
	//
	// IPv4 address parsing
	switch (inet_pton (AF_INET,  addr, raw_addr)) {
	case 1:
		return socket_address (AF_INET,  raw_addr, portnr, out_sa);
	case 0:
		break;
	default:
		break;
	}
	//
	// Report EINVAL as an error condition
	errno = EINVAL;
	return false;
}


/* Open a connection as a client, to the given address.  Do not bind locally.
 * In case we would bind, we should consider SO_LINGER for non-zero ports.
 *
 * Set contype to one SOCK_DGRAM, SOCK_STREAM or SOCK_SEQPACKET.
 *
 * The resulting socket is written to out_sox.
 *
 * Return true on success, or false with errno set on failure.
 * On error, *out_sox is set to -1.
 */
bool socket_client (const struct sockaddr_storage *peer, int contype, int *out_sox) {
	int sox = -1;
	sox = socket (peer->ss_family, contype, 0);
	if (sox < 0) {
		goto fail;
	}
	if (connect (sox, (struct sockaddr *) peer, socket_addrlen (peer)) != 0) {
		goto fail;
	}
#ifdef PART_OF_KXOVER
	int soxflags = fcntl (sox, F_GETFL, 0);
	if (fcntl (sox, F_SETFL, soxflags | O_NONBLOCK) != 0) {
		goto fail;
	}
#endif
	*out_sox = sox;
	return true;
fail:
	*out_sox = -1;
	if (sox >= 0) {
		close (sox);
	}
	return false;
}


/* Open a listening socket as a server, at the given address.
 * When binding to a non-zero port, setup SO_LINGER.
 *
 * Set contype to one of SOCK_DGRAM, SOCK_STREAM or SOCK_SEQPACKET.
 *
 * The resulting socket is written to out_sox.
 *
 * Return true on success, or false with errno set on failure.
 * On error, *out_sox is set to -1.
 */
bool socket_server (const struct sockaddr_storage *mine, int contype, int *out_sox) {
	int sox = -1;
	sox = socket (mine->ss_family, contype, 0);
	if (sox < 0) {
		goto fail;
	}
	if (bind (sox, (struct sockaddr *) mine, socket_addrlen (mine)) != 0) {
		goto fail;
	}
	uint16_t myport = socket_port (mine);
	if (myport != 0) {
		struct linger lo;
		memset (&lo, 0, sizeof (lo));
		lo.l_onoff  = 1;
		lo.l_linger = 15;
		if (setsockopt (sox, SOL_SOCKET, SO_LINGER, &lo, sizeof (lo)) != 0) {
			log_warning ("Server socket on port %d will not linger", myport);
		}
	}
	if ((contype == SOCK_STREAM) || (contype == SOCK_SEQPACKET)) {
		if (listen (sox, 10) != 0) {
			goto fail;
		}
	}
	*out_sox = sox;
	return true;
fail:
	*out_sox = -1;
	if (sox >= 0) {
		close (sox);
	}
	return false;
}


/* Switch a file descriptor to non-blocking mode.
 *
 * Return true on success.
 */
bool socket_nonblock (int sox) {
	int soxflags = fcntl (sox, F_GETFL, 0);
	if (soxflags == -1) {
		return false;
	}
	soxflags |= O_NONBLOCK;
	return (0 == fcntl (sox, F_SETFL, soxflags));
}

