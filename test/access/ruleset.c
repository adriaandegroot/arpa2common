/* Test ruleset upcalls.  (Slightly more abstract than ACL rules.)
 *
 * The commandline provides a file and a sequence of rules.
 * The tool outputs compare-friendly data for upcalls.
 * If the file does not exist, it is created; if it does,
 * it is opened readonly and used for comparison.  In case
 * of failures or differences, the program yields exit(1).
 *
 * SPDX-License-Identifier: BSD-2-Clause 
 * SPDX-FileCopyrightText: Copyright 2021 Rick van Rein <rick@openfortress.nl> 
 */


#include <stdlib.h>
#include <stdbool.h>
#include <stdio.h>
#include <string.h>

#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

#define DEBUG
#define DEBUG_DETAIL
#define LOG_STYLE LOG_STYLE_STDOUT
#include <arpa2/except.h>
#include <arpa2/identity.h>
#include <arpa2/rules.h>


bool logwrite = false;
int logfh = -1;
char buf [2090];


void logfh_open (char *logfname) {
	logwrite = false;
	logfh = open (logfname, O_RDONLY);
	if ((logfh < 0) && (errno == ENOENT)) {
		logwrite = true;
		logfh = creat (logfname, S_IRUSR | S_IRGRP | S_IROTH);
	}
	if (logfh < 0) {
		log_errno ("Could not open logfile");
		exit (1);
	}
}

void logfh_close (void) {
	close (logfh);
	logfh = -1;
}

void logfh_text (char *str) {
	unsigned length = strlen (str);
	if (logwrite) {
		errno = 0;
		if (write (logfh, str, length) != length) {
			log_errno ("Write partial or other failure");
			exit (1);
		}
	} else {
		char buf [length];
		errno = 0;
		if (read (logfh, buf, length) != length) {
			log_errno ("Read partial or other failure");
			exit (1);
		}
		if (memcmp (str, buf, length) != 0) {
			fprintf (stderr, "Failure in data:\nEXP> %.*s\nGOT> %.*s\n",
					length, buf, length, str);
			exit (1);
		}
	}
}

void logfh_varray (struct rules_request *req) {
	for (int vari = 0; vari < 26; vari++) {
		if (req->varray_strlen [vari] == 0) {
			continue;
		}
		snprintf (buf, 2048, " %c = %.*s\n",
				'a' + vari,
				req->varray_strlen [vari],
				req->varray_str    [vari]);
		logfh_text (buf);
	}
}

bool up_flags (struct rules_request *req) {
	snprintf (buf, 2048, "Flags found: %08x for %s\n", req->flags, req->flags_rawstr);
	logfh_text (buf);
	logfh_varray (req);
	return true;
}

bool up_trigger (struct rules_request *req, rules_trigger trigger_name, unsigned trigger_namelen) {
	snprintf (buf, 2048, "Trigger received: %.*s\n", trigger_namelen, trigger_name);
	logfh_text (buf);
	logfh_varray (req);
	return true;
}

bool up_selector (struct rules_request *req, const a2sel_t *selector) {
	snprintf (buf, 2048, "Remote Selector received: %s\n", selector->txt);
	logfh_text (buf);
	a2sel_detail ("Parsed selector", selector);
	logfh_varray (req);
	return true;
}

bool up_endrule (struct rules_request *req) {
	logfh_text ("Rule ends\n");
	return true;
}

bool up_endruleset (struct rules_request *req) {
	logfh_text ("Ruleset ends\n");
	return true;
}


int main (int argc, char *argv []) {
	a2id_init ();
	rules_init ();
	//
	// Parse arguments
	if (argc < 3) {
		fprintf (stderr, "Usage: %s <logfile> <rule>...\n", argv [0]);
		exit (1);
	}
	char *fname = argv [1];
	//
	// Open the logfile
	logfh_open (fname);
	//
	// Collect the rules into one contiguous block
	unsigned ruleslen = 0;
	for (int argi = 2; argi < argc; argi++) {
		ruleslen += strlen (argv [argi]) + 1;
	}
	char *rules = malloc (ruleslen);
	if (rules == NULL) {
		log_errno ("Rules allocation");
		exit (1);
	}
	ruleslen = 0;
	for (int argi = 2; argi < argc; argi++) {
		int onelen = strlen (argv [argi]);
		strcpy (rules + ruleslen, argv [argi]);
		ruleslen += onelen + 1;
	}
	//
	// Setup the rules request structure
	struct rules_request req;
	memset (&req, 0, sizeof (req));
	req.optcb_flags      = up_flags;
	req.optcb_trigger    = up_trigger;
	req.optcb_selector   = up_selector;
	req.optcb_endrule    = up_endrule;
	req.optcb_endruleset = up_endruleset;
	//
	// Process rules, triggering callbacks that dump data
	bool ok = rules_process (&req, rules, ruleslen, true);
	if (!ok) {
		log_errno ("Rules processing failed");
		exit (1);
	}
	//
	// Cleanup and leave in success
	logfh_close ();
	free (rules);
	rules = NULL;
	rules_fini ();
	a2id_fini ();
	exit (0);
}
