/* Test that www.example.com translates to example.com */

#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include "aclv1/a2aclr.h"

int main (int _argc, char *_argv []) {
	const char *realm = "www.example.com";
	const char *hostnm = "example.com";
	const char *derived = a2aclr_hostname2realm (realm);
	if (derived == NULL) {
		printf ("Unexpected result NULL\n");
		exit (1);
	}
	if (strcmp (hostnm, derived) != 0) {
		printf ("Expected %s, got %s\n", hostnm, derived);
		exit (1);
	}
	return 0;
}
