/*
 * Copyright (c) 2018 Tim Kuijsten
 *
 * Permission to use, copy, modify, and/or distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES WITH
 * REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY
 * AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT,
 * INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM
 * LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR
 * OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
 * PERFORMANCE OF THIS SOFTWARE.
 */

#include <assert.h>
#include <err.h>
#include <stdlib.h>
#include <string.h>

#include "aclv1/a2aclc.c"

static char *aclcrule;
static size_t aclcrulesize;
static int fetchcalled;
static int putcalled;

/*
 * DB mock.
 */
int
a2aclc_dbopen(const char *path)
{
	path = NULL;
	return 0;
}

int
a2aclc_dbclose(void)
{
	return 0;
}

int
a2aclc_count(size_t *count)
{
	count = NULL;
	return 0;
}

/*
 * Stores nothing but always returns 0 and increments the global "putcalled".
 *
 * Must return 0 on success, -1 on failure.
 */
int
a2aclc_putaclcrule(const char *aclcrule, size_t aclcrulesize,
     const char *remotesel, size_t remoteselsize, const char *localid,
    size_t localidsize)
{
	/* suppress compiler warnings */
	aclcrule = remotesel = localid = NULL;
	aclcrulesize = remoteselsize = localidsize = 0;

	putcalled++;
	return 0;
}

/*
 * Fetch a communication aclc rule given a remote and local ID.
 *
 * A shim aclc rule k/v implementation.
 *
 * Returns whatever is stored in the global "aclcrule".
 *
 * Return 0 on success, -1 on error.
 */
int
a2aclc_getaclcrule(char *aclcr, size_t *aclcrsize, const char *remotesel,
    size_t remoteselsize, const char *localid, size_t localidsize)
{
	if (aclcrulesize > *aclcrsize)
		return -1;

	memcpy(aclcr, aclcrule, aclcrulesize);
	*aclcrsize = aclcrulesize;

	/* suppress compiler warnings */
	remotesel = localid = NULL;
	remoteselsize = localidsize = 0;

	fetchcalled++;
	return 0;
}

void
test_a2aclc_nextsegment(void)
{
	struct a2aclcseg aclcseg;
	struct a2aclcit *aclcit;
	const char *rule;
	char list;
	int r;

	list = 0;

	/* invalid aclc rule */
	rule = "";
	if ((aclcit = a2aclc_newit(rule, strlen(rule))) == NULL)
		abort();
	assert(a2aclc_nextsegment(&list, &aclcseg, aclcit) == -1);
	free(aclcit);

	rule = "%W +";
	if ((aclcit = a2aclc_newit(rule, strlen(rule))) == NULL)
		abort();
	r = a2aclc_nextsegment(&list, &aclcseg, aclcit);
	assert(r == 1);
	assert(list == 'W');
	assert(aclcseg.segsize == 0);
	assert(aclcseg.reqsigflags == 0);
	assert(a2aclc_nextsegment(&list, &aclcseg, aclcit) == 0);
	free(aclcit);

	rule = "%W + ";
	if ((aclcit = a2aclc_newit(rule, strlen(rule))) == NULL)
		abort();
	assert(a2aclc_nextsegment(&list, &aclcseg, aclcit) == 1);
	assert(list == 'W');
	assert(aclcseg.segsize == 0);
	assert(aclcseg.reqsigflags == 0);
	assert(a2aclc_nextsegment(&list, &aclcseg, aclcit) == 0);
	free(aclcit);

	rule = "%W +foo";
	if ((aclcit = a2aclc_newit(rule, strlen(rule))) == NULL)
		abort();
	assert(a2aclc_nextsegment(&list, &aclcseg, aclcit) == 1);
	assert(list == 'W');
	assert(aclcseg.segsize == 3);
	assert(aclcseg.reqsigflags == 0);
	assert(strncmp(aclcseg.seg, "foo", aclcseg.segsize) == 0);
	assert(a2aclc_nextsegment(&list, &aclcseg, aclcit) == 0);
	free(aclcit);

	rule = "%W +foo ";
	if ((aclcit = a2aclc_newit(rule, strlen(rule))) == NULL)
		abort();
	assert(a2aclc_nextsegment(&list, &aclcseg, aclcit) == 1);
	assert(list == 'W');
	assert(aclcseg.segsize == 3);
	assert(aclcseg.reqsigflags == 0);
	assert(strncmp(aclcseg.seg, "foo", aclcseg.segsize) == 0);
	assert(a2aclc_nextsegment(&list, &aclcseg, aclcit) == 0);
	free(aclcit);

	rule = "%W++";
	if ((aclcit = a2aclc_newit(rule, strlen(rule))) == NULL)
		abort();
	assert(a2aclc_nextsegment(&list, &aclcseg, aclcit) == 1);
	assert(list == 'W');
	assert(aclcseg.segsize == 0);
	assert(aclcseg.reqsigflags == 1);
	assert(a2aclc_nextsegment(&list, &aclcseg, aclcit) == 0);
	free(aclcit);

	rule = "%W ++";
	if ((aclcit = a2aclc_newit(rule, strlen(rule))) == NULL)
		abort();
	assert(a2aclc_nextsegment(&list, &aclcseg, aclcit) == 1);
	assert(list == 'W');
	assert(aclcseg.segsize == 0);
	assert(aclcseg.reqsigflags == 1);
	assert(a2aclc_nextsegment(&list, &aclcseg, aclcit) == 0);
	free(aclcit);

	rule = "%W ++ ";
	if ((aclcit = a2aclc_newit(rule, strlen(rule))) == NULL)
		abort();
	assert(a2aclc_nextsegment(&list, &aclcseg, aclcit) == 1);
	assert(list == 'W');
	assert(aclcseg.segsize == 0);
	assert(aclcseg.reqsigflags == 1);
	assert(a2aclc_nextsegment(&list, &aclcseg, aclcit) == 0);
	free(aclcit);

	rule = "%W bar";
	if ((aclcit = a2aclc_newit(rule, strlen(rule))) == NULL)
		abort();
	assert(a2aclc_nextsegment(&list, &aclcseg, aclcit) == -1);
	free(aclcit);

	rule = "%W +++";
	if ((aclcit = a2aclc_newit(rule, strlen(rule))) == NULL)
		abort();
	assert(a2aclc_nextsegment(&list, &aclcseg, aclcit) == -1);
	free(aclcit);

	rule = "%W +++";
	if ((aclcit = a2aclc_newit(rule, strlen(rule))) == NULL)
		abort();
	assert(a2aclc_nextsegment(&list, &aclcseg, aclcit) == -1);
	free(aclcit);

	rule = "%W+foo +bar+baz %B+foo+bar";
	if ((aclcit = a2aclc_newit(rule, strlen(rule))) == NULL)
		abort();
	assert(a2aclc_nextsegment(&list, &aclcseg, aclcit) == 1);
	assert(list == 'W');
	assert(aclcseg.segsize == 3);
	assert(aclcseg.reqsigflags == 0);
	assert(strncmp(aclcseg.seg, "foo", aclcseg.segsize) == 0);
	r = a2aclc_nextsegment(&list, &aclcseg, aclcit);
	assert(r == 1);
	assert(list == 'W');
	assert(aclcseg.segsize == 7);
	assert(aclcseg.reqsigflags == 0);
	assert(strncmp(aclcseg.seg, "bar+baz", aclcseg.segsize) == 0);
	assert(a2aclc_nextsegment(&list, &aclcseg, aclcit) == 1);
	assert(list == 'B');
	assert(aclcseg.segsize == 7);
	assert(aclcseg.reqsigflags == 0);
	assert(strncmp(aclcseg.seg, "foo+bar", aclcseg.segsize) == 0);
	assert(a2aclc_nextsegment(&list, &aclcseg, aclcit) == 0);
	free(aclcit);

	/* regress */
	rule = "%W +arpa2+";
	if ((aclcit = a2aclc_newit(rule, strlen(rule))) == NULL)
		abort();
	r = a2aclc_nextsegment(&list, &aclcseg, aclcit);
	assert(r == 1);
	assert(list == 'W');
	assert(aclcseg.segsize == 5);
	assert(aclcseg.reqsigflags == 1);
	assert(strncmp(aclcseg.seg, "arpa2", aclcseg.segsize) == 0);
	r = a2aclc_nextsegment(&list, &aclcseg, aclcit);
	assert(r == 0);
	free(aclcit);
}

void
test_a2aclc_whichlist(void)
{
	a2id remoteid, localid;
	int r;
	const char *input;
	char list;

	input = "foo+bar@example.net";
	if (a2id_fromstr(&localid, input, strlen(input), 0) == -1)
		abort();

	aclcrule = "";
	aclcrulesize = strlen(aclcrule);
	fetchcalled = 0;
	input = "baz@example.com";
	if (a2id_fromstr(&remoteid, input, strlen(input), 0) == -1)
		abort();
	r = a2aclc_whichlist(&list, &remoteid, &localid);
	assert(r == 0);
	assert(list == 'G');
	assert(fetchcalled == 5);

	aclcrule = "%W +bar";
	aclcrulesize = strlen(aclcrule);
	fetchcalled = 0;
	input = "baz@example.com";
	if (a2id_fromstr(&remoteid, input, strlen(input), 0) == -1)
		abort();
	r = a2aclc_whichlist(&list, &remoteid, &localid);
	assert(r == 0);
	assert(list == 'W');
	assert(fetchcalled == 1);

	aclcrule = "%W +baz";
	aclcrulesize = strlen(aclcrule);
	fetchcalled = 0;
	input = "baz@example.com";
	if (a2id_fromstr(&remoteid, input, strlen(input), 0) == -1)
		abort();
	r = a2aclc_whichlist(&list, &remoteid, &localid);
	assert(r == 0);
	assert(list == 'G');
	assert(fetchcalled == 5);

	aclcrule = "%W +foo +barbaz %B +foo +bar";
	aclcrulesize = strlen(aclcrule);
	fetchcalled = 0;
	input = "baz@example.com";
	if (a2id_fromstr(&remoteid, input, strlen(input), 0) == -1)
		abort();
	r = a2aclc_whichlist(&list, &remoteid, &localid);
	assert(r == 0);
	assert(list == 'B');
	assert(fetchcalled == 1);

	aclcrule = "%W +foo +barbaz %B +foo+bar";
	aclcrulesize = strlen(aclcrule);
	fetchcalled = 0;
	input = "baz@example.com";
	if (a2id_fromstr(&remoteid, input, strlen(input), 0) == -1)
		abort();
	r = a2aclc_whichlist(&list, &remoteid, &localid);
	assert(r == 0);
	assert(list == 'G');
	assert(fetchcalled == 5);

	aclcrule = "%W +foo +barbaz %B +foo +bar+baz";
	aclcrulesize = strlen(aclcrule);
	fetchcalled = 0;
	input = "baz@example.com";
	if (a2id_fromstr(&remoteid, input, strlen(input), 0) == -1)
		abort();
	r = a2aclc_whichlist(&list, &remoteid, &localid);
	assert(r == 0);
	assert(list == 'G');
	assert(fetchcalled == 5);

	input = "foo+bar+baz@example.net";
	if (a2id_fromstr(&localid, input, strlen(input), 0) == -1)
		abort();

	aclcrule = "%W +foo +barbaz %B +foo +bar+baz";
	aclcrulesize = strlen(aclcrule);
	fetchcalled = 0;
	input = "baz@example.com";
	if (a2id_fromstr(&remoteid, input, strlen(input), 0) == -1)
		abort();
	r = a2aclc_whichlist(&list, &remoteid, &localid);
	assert(r == 0);
	assert(list == 'B');
	assert(fetchcalled == 1);

	aclcrule = "%X +foo";
	aclcrulesize = strlen(aclcrule);
	fetchcalled = 0;
	input = "baz@example.com";
	if (a2id_fromstr(&remoteid, input, strlen(input), 0) == -1)
		abort();
	r = a2aclc_whichlist(&list, &remoteid, &localid);
	assert(r == -1);
}

void
test_a2aclc_parsepolicyline(void)
{
	const char *aclcrule, *remotesel, *localid, *err, *policyline;
	size_t aclcrulesize, remoteselsize, localidsize;
	int r;

	policyline = "";
	r = a2aclc_parsepolicyline(&remotesel, &remoteselsize, &localid,
	    &localidsize, &aclcrule, &aclcrulesize, policyline,
	    strlen(policyline), &err);
	assert(r == -1);
	assert(err == NULL);

	policyline = "a";
	r = a2aclc_parsepolicyline(&remotesel, &remoteselsize, &localid,
	    &localidsize, &aclcrule, &aclcrulesize, policyline,
	    strlen(policyline), &err);
	assert(r == -1);
	assert(err == &policyline[0]);

	policyline = "xx xxx xx";
	r = a2aclc_parsepolicyline(&remotesel, &remoteselsize, &localid,
	    &localidsize, &aclcrule, &aclcrulesize, policyline,
	    strlen(policyline), &err);
	assert(r == -1);
	assert(err == &policyline[0]);

	policyline = "xx xxx xx ";
	r = a2aclc_parsepolicyline(&remotesel, &remoteselsize, &localid,
	    &localidsize, &aclcrule, &aclcrulesize, policyline,
	    strlen(policyline), &err);
	assert(r == 0);
	assert(remotesel == &policyline[0]);
	assert(remoteselsize == 2);
	assert(localid == &policyline[3]);
	assert(localidsize == 3);
	assert(aclcrule == &policyline[7]);
	assert(aclcrulesize == 3);

	policyline = " a@selector.   someone@b  %B + %W ++ ";
	r = a2aclc_parsepolicyline(&remotesel, &remoteselsize, &localid,
	    &localidsize, &aclcrule, &aclcrulesize, policyline,
	    strlen(policyline), &err);
	assert(r == 0);
	assert(remotesel == &policyline[1]);
	assert(remoteselsize == 11);
	assert(localid == &policyline[15]);
	assert(localidsize == 9);
	assert(aclcrule == &policyline[26]);
	assert(aclcrulesize == 11);
}

int
main(void)
{
	test_a2aclc_nextsegment();
	test_a2aclc_whichlist();
	test_a2aclc_parsepolicyline();

	return 0;
}
