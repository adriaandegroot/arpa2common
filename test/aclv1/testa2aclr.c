/*
 * Copyright (c) 2019 Tim Kuijsten
 *
 * Permission to use, copy, modify, and/or distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES WITH
 * REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY
 * AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT,
 * INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM
 * LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR
 * OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
 * PERFORMANCE OF THIS SOFTWARE.
 */

#include <assert.h>
#include <err.h>
#include <stddef.h>
#include <string.h>

#include "aclv1/a2aclr.c"

void
test_parsepolicyline(void)
{
	const char *realm, *selector, *rights, *resource, *instance, *err,
	    *policyline;
	size_t realmsz, selectorsz, rightssz, resourcesz, instancesz;
	int r;

	policyline = "";
	r = parsepolicyline(&realm, &realmsz, &selector,
	    &selectorsz, &rights, &rightssz, &resource, &resourcesz,
	    &instance, &instancesz, policyline, &err);
	assert(r == -1);
	assert(err == policyline);

	policyline = "xx xxx xx";
	r = parsepolicyline(&realm, &realmsz, &selector,
	    &selectorsz, &rights, &rightssz, &resource, &resourcesz,
	    &instance, &instancesz, policyline, &err);
	assert(r == -1);
	assert(err == &policyline[9]);

	policyline = "example.org\tjoe@example.com\tV\t11bc2aa8-4151-4a73-b589-"
	    "0a86442b8502\taResourceInstance";
	r = parsepolicyline(&realm, &realmsz, &selector,
	    &selectorsz, &rights, &rightssz, &resource, &resourcesz,
	    &instance, &instancesz, policyline, &err);
	assert(r == 0);
	assert(realmsz == 11);
	assert(realm == &policyline[0]);
	assert(selectorsz == 15);
	assert(selector == &policyline[12]);
	assert(rightssz == 1);
	assert(rights == &policyline[28]);
	assert(resourcesz == 36);
	assert(resource == &policyline[30]);
	assert(instancesz == 17);
	assert(instance == &policyline[67]);

	policyline = "example.org\tjoe@example.com\tWRD\t11bc2aa8-4151-4a73-b589-0a8"
	    "6442b8502\t";
	r = parsepolicyline(&realm, &realmsz, &selector,
	    &selectorsz, &rights, &rightssz, &resource, &resourcesz,
	    &instance, &instancesz, policyline, &err);
	assert(r == 0);
	assert(realmsz == 11);
	assert(realm == &policyline[0]);
	assert(selectorsz == 15);
	assert(selector == &policyline[12]);
	assert(rightssz == 3);
	assert(rights == &policyline[28]);
	assert(resourcesz == 36);
	assert(resource == &policyline[32]);
	assert(instancesz == 0);

	/* no leading tabs */
	policyline = "\texample.org\tjoe@example.com\tWRD\t11bc2aa8-4151-4a73-b589-0a8"
	    "6442b8502\tsomerandom/string";
	r = parsepolicyline(&realm, &realmsz, &selector,
	    &selectorsz, &rights, &rightssz, &resource, &resourcesz,
	    &instance, &instancesz, policyline, &err);
	assert(r == -1);
	assert(err == &policyline[0]);

	/* no trailing tabs */
	policyline = "example.org\tjoe@example.com\tWRD\t11bc2aa8-4151-4a73-b589-0a8"
	    "6442b8502\tsomerandom/string\t";
	r = parsepolicyline(&realm, &realmsz, &selector,
	    &selectorsz, &rights, &rightssz, &resource, &resourcesz,
	    &instance, &instancesz, policyline, &err);
	assert(r == -1);
	assert(err == &policyline[86]);

	/* only one tab as field separator */
	policyline = "example.org\t\tjoe@example.com\tWRD\t11bc2aa8-4151-4a73-b589-0a8"
	    "6442b8502\tsomerandom/string";
	r = parsepolicyline(&realm, &realmsz, &selector,
	    &selectorsz, &rights, &rightssz, &resource, &resourcesz,
	    &instance, &instancesz, policyline, &err);
	assert(r == -1);
	assert(err == &policyline[12]);

	/* only one tab as field separator, between optional field */
	policyline = "example.org\tjoe@example.com\tWRD\t11bc2aa8-4151-4a73-b589-0a8"
	    "6442b8502\t\tsomerandom/string";
	r = parsepolicyline(&realm, &realmsz, &selector,
	    &selectorsz, &rights, &rightssz, &resource, &resourcesz,
	    &instance, &instancesz, policyline, &err);
	assert(r == -1);
	assert(err == &policyline[69]);
}

void
test_a2aclr_whichrights(void)
{
	const char realm[] = "whichrights.test.example.net";
	const char selector[] = "@example.net";
	const char resource[] = "7a35d76d-a754-35a6-abe7-757c161f0263";
	char rights[MAXRIGHTSSIZE];
	size_t rightssize;
	void *ctx;
	int r;

	if ((ctx = a2aclr_dbopen("testdb")) == NULL)
		abort();

	/*
	 * Test 1
	 *
	 * Store valid lower-case rights with duplicates.
	 */

	if (a2aclr_putrights(ctx, realm, strlen(realm), selector,
	    strlen(selector), resource, strlen(resource), NULL, 0, "koko", 4)
	    == -1)
		abort();

	rightssize = sizeof(rights);
	r = a2aclr_whichrights(ctx, rights, &rightssize, realm,
	    strlen(realm), selector, strlen(selector), resource,
	    strlen(resource), NULL, 0);
	assert(r == 0);

	/* should have removed duplicates and stored as upper case */
	assert(rightssize == 2);
	assert(memcmp(rights, "KO", rightssize) == 0);
}

void
test_a2aclr_hasrights(void)
{
	const char realm[] = "hasrights.test.example.net";
	const char selector[] = "@example.net";
	const char resource[] = "7a35d76d-a754-35a6-abe7-757c161f0263";
	const char instance[] = "/home";
	const char rights[] = "CW";
	char *reqrights;
	void *ctx;
	int r;

	if ((ctx = a2aclr_dbopen("testdb")) == NULL)
		abort();

	if (a2aclr_putrights(ctx, realm, strlen(realm), selector,
	    strlen(selector), resource, strlen(resource), instance,
	    strlen(instance), rights, strlen(rights)) == -1)
		abort();

	reqrights = "CW";
	r = a2aclr_hasrights(ctx, reqrights, strlen(reqrights), realm,
	    strlen(realm), selector, strlen(selector), resource,
	    strlen(resource), instance, strlen(instance));
	assert(r == 1);

	reqrights = "C";
	r = a2aclr_hasrights(ctx, reqrights, strlen(reqrights), realm,
	    strlen(realm), selector, strlen(selector), resource,
	    strlen(resource), instance, strlen(instance));
	assert(r == 1);

	reqrights = "W";
	r = a2aclr_hasrights(ctx, reqrights, strlen(reqrights), realm,
	    strlen(realm), selector, strlen(selector), resource,
	    strlen(resource), instance, strlen(instance));
	assert(r == 1);

	reqrights = "R";
	r = a2aclr_hasrights(ctx, reqrights, strlen(reqrights), realm,
	    strlen(realm), selector, strlen(selector), resource,
	    strlen(resource), instance, strlen(instance));
	assert(r == 0);

	reqrights = "WR";
	r = a2aclr_hasrights(ctx, reqrights, strlen(reqrights), realm,
	    strlen(realm), selector, strlen(selector), resource,
	    strlen(resource), instance, strlen(instance));
	assert(r == 0);

	reqrights = "RW";
	r = a2aclr_hasrights(ctx, reqrights, strlen(reqrights), realm,
	    strlen(realm), selector, strlen(selector), resource,
	    strlen(resource), instance, strlen(instance));
	assert(r == 0);

	reqrights = "CWR";
	r = a2aclr_hasrights(ctx, reqrights, strlen(reqrights), realm,
	    strlen(realm), selector, strlen(selector), resource,
	    strlen(resource), instance, strlen(instance));
	assert(r == 0);

	reqrights = "CRW";
	r = a2aclr_hasrights(ctx, reqrights, strlen(reqrights), realm,
	    strlen(realm), selector, strlen(selector), resource,
	    strlen(resource), instance, strlen(instance));
	assert(r == 0);

	reqrights = "RCW";
	r = a2aclr_hasrights(ctx, reqrights, strlen(reqrights), realm,
	    strlen(realm), selector, strlen(selector), resource,
	    strlen(resource), instance, strlen(instance));
	assert(r == 0);

	reqrights = "";
	r = a2aclr_hasrights(ctx, reqrights, strlen(reqrights), realm,
	    strlen(realm), selector, strlen(selector), resource,
	    strlen(resource), instance, strlen(instance));
	assert(r == 0);

	if (a2aclr_dbclose(ctx) == -1)
		abort();
}

void
test_a2aclr_hasright(void)
{
	const char realm[] = "hasright.test.example.net";
	const char selector[] = "@example.net";
	const char resource[] = "7a35d76d-a754-35a6-abe7-757c161f0263";
	const char instance[] = "/home";
	const char rights[] = "KO";
	char reqright;
	void *ctx;
	int r;

	if ((ctx = a2aclr_dbopen("testdb")) == NULL)
		abort();

	if (a2aclr_putrights(ctx, realm, strlen(realm), selector,
	    strlen(selector), resource, strlen(resource), instance,
	    strlen(instance), rights, strlen(rights)) == -1)
		abort();

	reqright = 'K';
	r = a2aclr_hasright(ctx, reqright, realm, strlen(realm), selector,
	    strlen(selector), resource, strlen(resource), instance,
	    strlen(instance));
	assert(r == 1);

	reqright = 'O';
	r = a2aclr_hasright(ctx, reqright, realm, strlen(realm), selector,
	    strlen(selector), resource, strlen(resource), instance,
	    strlen(instance));
	assert(r == 1);

	reqright = 'W';
	r = a2aclr_hasright(ctx, reqright, realm, strlen(realm), selector,
	    strlen(selector), resource, strlen(resource), instance,
	    strlen(instance));
	assert(r == 0);

	reqright = '\0';
	r = a2aclr_hasright(ctx, reqright, realm, strlen(realm), selector,
	    strlen(selector), resource, strlen(resource), instance,
	    strlen(instance));
	assert(r == 0);

	if (a2aclr_dbclose(ctx) == -1)
		abort();
}

void
test_a2aclr_putrights_hasright(void)
{
	const char realm[] = "putrights_hasright.test.example.net";
	const char selector[] = "@example.net";
	const char resource[] = "7a35d76d-a754-35a6-abe7-757c161f0263";
	const char instance[] = "/home";
	char savedrights[MAXRIGHTSSIZE], reqright;
	size_t savedrightssize;
	void *ctx;
	int r;

	if ((ctx = a2aclr_dbopen("testdb")) == NULL)
		abort();

	/*
	 * Test 1
	 *
	 * Store valid lower-case rights with duplicates.
	 */

	if (a2aclr_putrights(ctx, realm, strlen(realm), selector,
	    strlen(selector), resource, strlen(resource), instance,
	    strlen(instance), "koko", strlen("koko")) == -1)
		abort();

	savedrightssize = sizeof(savedrights);
	r = a2aclr_whichrights(ctx, savedrights, &savedrightssize, realm,
	    strlen(realm), selector, strlen(selector), resource,
	    strlen(resource), instance, strlen(instance));
	assert(r == 0);

	/* should have removed duplicates and stored as upper case */
	assert(savedrightssize == 2);
	assert(memcmp(savedrights, "KO", savedrightssize) == 0);

	/*
	 * Test 2
	 */

	if (a2aclr_putrights(ctx, realm, strlen(realm), selector,
	    strlen(selector), resource, strlen(resource), instance,
	    strlen(instance), "V", strlen("V")) == -1)
		abort();

	/* K should still exist */
	reqright = 'K';
	r = a2aclr_hasright(ctx, reqright, realm, strlen(realm), selector,
	    strlen(selector), resource, strlen(resource), instance,
	    strlen(instance));
	assert(r == 1);

	/* and V should have been added */
	reqright = 'V';
	r = a2aclr_hasright(ctx, reqright, realm, strlen(realm), selector,
	    strlen(selector), resource, strlen(resource), instance,
	    strlen(instance));
	assert(r == 1);

	if (a2aclr_dbclose(ctx) == -1)
		abort();
}

void
test_a2aclr_delrights(void)
{
	const char realm[] = "delrights_hasright.test.example.net";
	const char selector[] = "@example.net";
	const char resource[] = "7a35d76d-a754-35a6-abe7-757c161f0263";
	const char instance[] = "/home";
	const char newrights[] = "CWRKV";
	const char *delrights;
	char savedrights[MAXRIGHTSSIZE];
	size_t savedrightssize;
	void *ctx;
	int r;

	if ((ctx = a2aclr_dbopen("testdb")) == NULL)
		abort();

	if (a2aclr_putrights(ctx, realm, strlen(realm), selector,
	    strlen(selector), resource, strlen(resource), instance,
	    strlen(instance), newrights, strlen(newrights)) == -1)
		abort();

	/*
	 * Test 1, delete multiple rights at once, case insensitive.
	 */

	savedrightssize = sizeof(savedrights);
	r = a2aclr_whichrights(ctx, savedrights, &savedrightssize, realm,
	    strlen(realm), selector, strlen(selector), resource,
	    strlen(resource), instance, strlen(instance));
	assert(r == 0);

	assert(savedrightssize == 5);
	assert(memcmp(savedrights, "CWRKV", savedrightssize) == 0);

	delrights = "crk";
	r = a2aclr_delrights(ctx, realm, strlen(realm), selector,
	    strlen(selector), resource, strlen(resource), instance,
	    strlen(instance), delrights, strlen(delrights));
	assert(r == 0);

	savedrightssize = sizeof(savedrights);
	r = a2aclr_whichrights(ctx, savedrights, &savedrightssize, realm,
	    strlen(realm), selector, strlen(selector), resource,
	    strlen(resource), instance, strlen(instance));
	assert(r == 0);

	assert(savedrightssize == 2);
	assert(memcmp(savedrights, "WV", savedrightssize) == 0);

	/*
	 * Test 2, delete the last right of the sequence and non-existing one.
	 */

	delrights = "KV";
	r = a2aclr_delrights(ctx, realm, strlen(realm), selector,
	    strlen(selector), resource, strlen(resource), instance,
	    strlen(instance), delrights, strlen(delrights));
	assert(r == 0);

	savedrightssize = sizeof(savedrights);
	r = a2aclr_whichrights(ctx, savedrights, &savedrightssize, realm,
	    strlen(realm), selector, strlen(selector), resource,
	    strlen(resource), instance, strlen(instance));
	assert(r == 0);

	assert(savedrightssize == 1);
	assert(memcmp(savedrights, "W", savedrightssize) == 0);

	/*
	 * Test 3, delete the last and only right remaining.
	 */

	delrights = "WV";
	r = a2aclr_delrights(ctx, realm, strlen(realm), selector,
	    strlen(selector), resource, strlen(resource), instance,
	    strlen(instance), delrights, strlen(delrights));
	assert(r == 0);

	savedrightssize = sizeof(savedrights);
	r = a2aclr_whichrights(ctx, savedrights, &savedrightssize, realm,
	    strlen(realm), selector, strlen(selector), resource,
	    strlen(resource), instance, strlen(instance));
	assert(r == 0);

	assert(savedrightssize == 0);

	if (a2aclr_dbclose(ctx) == -1)
		abort();
}

int
main(void)
{
	test_parsepolicyline();
	test_a2aclr_whichrights();
	test_a2aclr_hasrights();
	test_a2aclr_hasright();
	test_a2aclr_putrights_hasright();
	test_a2aclr_delrights();

	return 0;
}
