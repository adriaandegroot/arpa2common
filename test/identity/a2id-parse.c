/* Test parser for ARPA2 Identities.
 *
 * SPDX-License-Identifier: BSD-2-Clause 
 * SPDX-FileCopyrightText: Copyright 2020 Rick van Rein <rick@openfortress.nl> 
 */


#include <stdlib.h>
#include <stdbool.h>
#include <stdio.h>
#include <string.h>

#include <arpa2/identity.h>
#include <arpa2/except.h>


bool test (char *instr) {
	a2id_t tid;
	bool ok1 = a2id_parse (&tid, instr, strlen (instr));
	log_debug ("%s - %s", ok1 ? "OK" : "KO", instr);
	log_debug ("   = %s", tid.txt);
	time_t expts = (18000 + tid.expireday) * 86400;
	struct tm expdy;
	char expdystr [105];
	gmtime_r (&expts, &expdy);
	strftime (expdystr, 102, "%F", &expdy);
	struct tm exptm;
	char exptmstr [105];
	gmtime_r (&tid.expiration, &exptm);
	strftime (exptmstr, 102, "%F", &exptm);
	log_debug ("   @ %s due to expireday=%d", expdystr, tid.expireday);
	log_debug ("   @ %s due to expiration (0 until a2id_verify()", exptmstr);
	if (ok1) a2sel_detail ("   ", &tid);
	a2sel_t tsel;
	bool ok2 = a2sel_parse (&tsel, instr, strlen (instr));
	log_debug ("%s - %s", ok2 ? "OK" : "KO", instr);
	log_debug ("   = %s", tsel.txt);
	if (ok2) a2sel_detail ("   ", &tsel);
	bool ok3 = (sizeof (tid) == sizeof (tsel));
	ok3 = ok3 && (memcmp (&tid, &tsel, sizeof (tid)) == 0);
	log_debug ("%s - same data from a2id_parse() and a2sel_parse()",
			ok3 ? "OK" : "KO");
	if (!ok3) {
		log_data ("a2id__parse() output", (uint8_t *) &tid,  sizeof (tid ), 75);
		log_data ("a2sel_parse() output", (uint8_t *) &tsel, sizeof (tsel), 75);
	}
	return (ok1 && ok2 && ok3);
}

int main (int argc, char *argv []) {
	bool ok = true;
	a2id_init ();
	//
	// Test arguments
	if (argc < 2) {
		fprintf (stderr, "Usage: %s <user>@<domain>...\n", argv [0]);
		exit (1);
	}
	//
	// Iterate over all identities supplied
	for (int argi = 1; argi < argc; argi++) {
		if (!test (argv [argi])) {
			ok = false;
		}
	}
	//
	// Exit value
	return (ok ? 0 : 1);
}
