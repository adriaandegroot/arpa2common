/* Verify prepared Signatures in ARPA2 Identities.
 *
 * This command is called with an Identity and any arguments #4 and up.
 * The signature is verified instead of generated.
 *
 * Arguments up to #4 are derived from the local and remote identity.
 * To this end, the remote identity is also provided.
 *
 * Variations on the signature are made, based on scrambled data, which
 * ought to invalidate the signature *if* it is provided with the input
 * Selector.
 *
 * SPDX-License-Identifier: BSD-2-Clause 
 * SPDX-FileCopyrightText: Copyright 2020 Rick van Rein <rick@openfortress.nl> 
 */


#include <stdlib.h>
#include <stdbool.h>
#include <stdio.h>
#include <string.h>

#include <unistd.h>
#include <time.h>

#include <arpa2/com_err.h>

#include <arpa2/except.h>
#include <arpa2/identity.h>

#include "testkey.h"



bool parse_local_selector (char *instr, a2id_t *lid, bool *vary) {
	bool ok = true;
	//
	// Parse the local Selector and report the result
	ok = a2id_parse (lid, instr, 0);
	log_debug ("%s - %s", ok ? "OK" : "KO", instr);
	log_debug ("   = %s", lid->txt);
	if (ok) a2sel_detail ("   ", lid);
	//
	// Indicate that variations should be tried _if_ there is a
	// signature value in this local Selector
	*vary = ok && (lid->ofs [A2ID_OFS_SIGVALUE] < lid->ofs [A2ID_OFS_SIGPLUS]);
	//
	// Return success or failure
	return ok;
}


bool parse_remote_identity (char *instr, a2id_t *rid) {
	//
	// Parse the remote Identity and report the result
	bool ok = a2id_parse (rid, instr, 0);
	log_debug ("%s - %s", ok ? "OK" : "KO", instr);
	log_debug ("   = %s", rid->txt);
	if (ok) a2sel_detail ("   ", rid);
	return ok;
}


struct sigdata {
	int argc;
	char **argv;
	a2id_t *remote;
};


bool sign_callback (a2id_sigdata_t sd,
			void *cbdata, const a2id_t *id,
			uint8_t *buf, uint16_t *buflen) {
	//
	// Uncloak the signature data
	struct sigdata *sigdata = cbdata;
	//
	// Only arguments 4 and up are locally considered, rebase
	int argi = ((int) sd) - 4;
	//
	// Items before #4 are base inquiries
	if (argi < 0) {
		log_debug ("Signature Callback delegates inquiry for signature data #%d", (int) sd);
		if (a2id_sigdata_base (sd,
				sigdata->remote, id,
				buf, buflen)) {
			log_data ("Far-fetched data", buf, *buflen, 0);
			return true;
		}
		return false;
	}
	//
	// Test if we can provide #4 and up from argc, argv
	if (argi >= sigdata->argc) {
		*buflen = 0;
		log_error ("Signature Callback has no data #%d", (int) sd);
		return false;
	}
	//
	// Cut short if the data does not fit in the buffer
	int outlen = *buflen;
	int arglen = strlen (sigdata->argv [argi]);
	*buflen = arglen;
	if (arglen > *buflen) {
		memcpy (buf, sigdata->argv [argi], outlen);
		log_warning ("Signature Callback provides only the start of data #%d", (int) sd);
		log_data ("Cut-short data", buf, *buflen, 0);
		return false;
	}
	//
	// Copy the data to the buffer (which is large enough)
	memcpy (buf, sigdata->argv [argi], arglen);
	//
	// Return success
	log_info ("Signature Callback provides the data #%d", (int) sd);
	log_data ("Full-sized data", buf, *buflen, 0);
	return true;
}


int main (int argc, char *argv []) {
	bool ok = true;
	a2id_init ();
	//
	// Test arguments
	if (argc < 3) {
		fprintf (stderr, "Usage: %s <localid> <remoteid> [<opts>...]\n"
			"where <opts> is a beginning of the following:\n"
			" * <session-id>    under an application's interpretation\n"
			" * <canon-subject> entered by a human but canonicalised\n"
			" * <topic>         such as a \"[topic]\" in an email subject\n"
			" * <enc-local>     is the encrypted localpart (TODO)\n",
				argv [0]);
		exit (1);
	}
	//
	// Install the test secret
	int keyfd = keyfd_test ();
	ok = ok && a2id_addkey (keyfd);
	close (keyfd);
	//
	// Parse the identities
	a2id_t lid;
	a2id_t rid;
	bool vary = true;
	ok = ok && parse_local_selector  (argv [1], &lid, &vary);
	ok = ok && parse_remote_identity (argv [2], &rid );
	if (!ok) {
		fprintf (stderr, "Invalid identity grammar, cannot proceed\n");
		exit (1);
	}
	//
	// Prepare callback data
	struct sigdata sigdata;
	sigdata.argc = argc - 3;
	sigdata.argv = argv + 3;
	sigdata.remote = &rid;
	//
	// Construct the signed identity
	a2id_t siglid;
	memcpy (&siglid, &lid, sizeof (siglid));
	siglid.expiration = time (NULL);
	siglid.sigflags = A2ID_SIGFLAG_EXPIRATION       |
	                  A2ID_SIGFLAG_REMOTE_DOMAIN    |
	                  A2ID_SIGFLAG_REMOTE_USERID    |
	                  A2ID_SIGFLAG_LOCAL_ALIASES    |
	                  A2ID_SIGFLAG_SESSIONID        |
	                  A2ID_SIGFLAG_SUBJECT          |
	                  A2ID_SIGFLAG_TOPIC            ;
	// if (ok && !a2id_verify (&siglid, sign_callback, &sigdata))
	if (ok && !a2id_verify (&lid, sign_callback, &sigdata)) {
		log_errno ("Signature verification failure in a2id_verify()");
		ok = false;
	}
	time_t expts = (18000 + lid.expireday) * 86400;
	struct tm expdy;
	char expdystr [105];
	gmtime_r (&expts, &expdy);
	strftime (expdystr, 102, "%F", &expdy);
	struct tm exptm;
	char exptmstr [105];
	gmtime_r (&lid.expiration, &exptm);
	strftime (exptmstr, 102, "%F", &exptm);
	log_debug ("   @ %s due to expireday=%d", expdystr, lid.expireday);
	log_debug ("   @ %s due to expiration (filled in by a2id_verify()", exptmstr);
	//
	// Print the identity information
	if (ok) {
		printf ("Address signature added, %d bit strong:\n%s\n",
				5 * (siglid.ofs [A2ID_OFS_SIGPLUS] - siglid.ofs [A2ID_OFS_SIGVALUE]),
				siglid.txt);
		a2sel_detail ("   ", &siglid);
	}
	//
	// Compare the signature to the originally provided Selector
	if (ok && !a2id_equal (&siglid, &lid)) {
		log_error ("Signature does not match the local Identity");
		ok = false;
	} else {
		log_info ("Signature matches the local Identity");
	}
	//
	// Exit value
	a2id_fini ();
	return (ok ? 0 : 1);
}

