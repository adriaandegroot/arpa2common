/* Iterate ARPA2 Selectors compared as partially ordered sets.
 *
 * Generally said, ARPA2 Selectors form a partially ordered set;
 * although we have a "maximum value" with the "@." catch-all.
 *
 * This command allows you to specify inheritance sequences,
 * and validate them.  In addition, it will invalidate all other
 * relations.  It is helpful to compare an identity to a selector,
 * but can also validate much more complex networks.  Have a ball!
 *
 * Short runs of words specify a sequence from abstract to concrete,
 * and will be compared to be (1) superseteq, (2) not subseteq.  This
 * implies (3) not equal, which is additionally tested to be sure.
 * This is done transitively.
 *
 * Runs can be separated by "breaks", or sequences of dots, much like
 * UNIX path conventions.  One dot means adding another value as a
 * peer to the previous one.  Two dots move one level up, and so on.
 * You can even go up to a point before the first run to start from
 * scratch.  When runs "jump" on the bandwaggon of earlier runs, then
 * (1) the superseteq is tested, (2) subseteq invalidated and (3) not
 * equal is also tested explicitly.  This is also done transitively.
 *
 * Finally, different runs are assumed to be independent, otherwise
 * they would have jumped.  This is explicitly tested, as (1) not
 * superseteq, (2) not subseteq and (3) not equal.
 *
 * This program works as an array of (argc-1) * (argc-1) and it
 * knows the expected output for every node, and validates them.
 * Since partial orders are, well... partial, there are so many
 * things to compare.  Transitivity follows from partial orders,
 * makes sense for subsetting semantics, but is still good to
 * have tested.
 *
 * TODO: There is no current relation to have multiple supersets on
 * a more specific one -- over the domain and/or over the username.
 * Perhaps a combination of dots and commas could do this later.
 *
 * SPDX-License-Identifier: BSD-2-Clause 
 * SPDX-FileCopyrightText: Copyright 2020 Rick van Rein <rick@openfortress.nl> 
 */


#include <stdlib.h>
#include <stdbool.h>
#include <stdio.h>
#include <string.h>

#include <arpa2/identity.h>
#include <arpa2/except.h>


static const int NO_ARGI = -1;

typedef enum {
	POREL_UNSPECIFIED = 0,	/* we test _no_ relation for these */
	POREL_EQUAL = 1,	/* subseteq in both orientations */
	POREL_SUBSET = 2,	/* lower argi is subset   of higher argi */
	POREL_SUPERSET = 3,	/* lower argi is superset of higher argi */
	POREL_JUMPED = 255,	/* comparison is meaningless due to jump */
} poset_relation;


#define UTF8STR_UNSPECIFIED    "\xe2\x89\xa0"
#define UTF8STR_EQUAL          "="
#define UTF8STR_NOT_EQUAL      "\xe2\x89\xa0"
#define UTF8STR_SUBSETEQ       "\xe2\x8a\x86"
#define UTF8STR_SUPERSETEQ     "\xe2\x8a\x87"
#define UTF8STR_NOT_SUBSETEQ   "\xe2\x8a\x88"
#define UTF8STR_NOT_SUPERSETEQ "\xe2\x8a\x89"

bool expect (a2sel_t *left, a2sel_t *right, poset_relation infixop) {
	bool ok = true;
	char *op = "UNDEFINED_OPERATOR";
	//
	// First test left \subseteq right
	bool subseteq = a2sel_subseteq (left, right);
	bool supseteq = a2sel_subseteq (right, left);
	bool notequal = (0 != strcmp (left->txt, right->txt));
	//
	// Decide if what we found is ok
	switch (infixop) {
	case POREL_UNSPECIFIED:
		op = UTF8STR_UNSPECIFIED;
		ok = ok && (!subseteq) && (!supseteq) &&   notequal ;
		break;
	case POREL_EQUAL:
		op = UTF8STR_EQUAL;
		ok = ok && (!subseteq) && (!supseteq) && (!notequal);
		break;
	case POREL_SUBSET:
		op = UTF8STR_SUBSETEQ;
		ok = ok &&   subseteq  && (!supseteq) &&   notequal ;
		break;
	case POREL_SUPERSET:
		op = UTF8STR_SUPERSETEQ;
		ok = ok && (!subseteq) &&   supseteq  &&   notequal ;
		break;
	case POREL_JUMPED:
		/* Cannot compare, quietly accept */
		break;
	default:
		op = "UNKONWN_OPERATOR";
		ok = false;
	}
	//
	// Report an error, if any
	printf ("%s -- Relation got %s, %s, %s -- Expected \"%s\" %s \"%s\"\n",
		ok ? "OK" : "KO",
		subseteq ? UTF8STR_SUBSETEQ   : UTF8STR_NOT_SUBSETEQ,
		supseteq ? UTF8STR_SUPERSETEQ : UTF8STR_NOT_SUPERSETEQ,
		notequal ? UTF8STR_NOT_EQUAL  : UTF8STR_EQUAL,
		left->txt, op, right->txt);
	//USELESS// log_data ("L-operand", left,  sizeof (*left ), 75);
	//USELESS// log_data ("R-operand", right, sizeof (*right), 75);
	// Return the verdict
	return ok;
}


int main (int argc, char *argv []) {
	bool ok = true;
	//
	// The matrix of poset_relations.  We waste [0][*] and [*][0].
	poset_relation porel [argc][argc];
	memset (&porel, 0, sizeof (porel));
	//
	// The array of parsed selectors.  We waste [0].
	int argn = 0;
	a2sel_t selv [argc];
	//
	// The array of arguments' parents.  Initially no relations.
	int parentv [argc];
	//
	// Parse arguments.  Selectors have an '@' and jumps are all '.'
	int parent_argn = NO_ARGI;
	for (int argi = 1; argi < argc; argi++) {
		if (strchr (argv [argi], '@') != NULL) {
			//
			// Selector found.  Parse it.
			assertxt (a2sel_parse (&selv [argn], argv [argi], 0),
					"Failed to parse selector %s", argv [argi]);
			//
			// Set our parent in the linear vector.
			parentv [argn] = parent_argn;
			//
			// Setup SUPERSET relations with transitive parents.
			int pargn = parent_argn;
			while (pargn != NO_ARGI) {
				porel [pargn][argn] = POREL_SUPERSET;
				porel [argn][pargn] = POREL_SUBSET;
				pargn = parentv [pargn];
			}
			//
			// Offer parental guidance to following words.
			parent_argn = argn;
			//
			// We filled a position, extend the count
			argn++;
		} else {
			//
			// Jump found.  Count dots.
			int dots = 0;
			while (argv [argi][dots] != '\0') {
				assertxt (argv [argi][dots] == '.',
					"Word is neither a Selector nor does it consist of only dots: %s", argv [argi]);
				dots++;
			}
			//
			// Starting from our parent, go back several positions.
			while (dots-- > 0) {
				assertxt (parentv [parent_argn] != NO_ARGI,
					"Dotted word aims beyond the absolute beginning");
				//
				// Move to the parent's parent's .... parent
				parent_argn = parentv [parent_argn];
			}
		}
	}
	//
	// We now have a matrix full of relations.  Most are unrelated.
	// Test the various relations, and be strict about them.
	for (int argi = 0; argi < argn; argi++) {
		for (int argj = 0; argj < argi; argj++) {
			//
			// Test subsetting -- continue to collect after failure.
			ok = expect (&selv [argi], &selv [argj], porel [argi][argj])
				&& ok;
		}
	}
	//
	// Exit value
	return (ok ? 0 : 1);
}

