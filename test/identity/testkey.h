/* Generate a test keyfd.  Once.
 *
 * SPDX-License-Identifier: BSD-2-Clause
 * SPDX-FileCopyrightText: Copyright 2020 Rick van Rein <rick@openfortress.nl>
 */


/* Generate a keyfd.  The key is fixed, ideal for test purposes.
 * Can be fed directly into a2id_addkey().  Remember to close afterwards.
 */
int keyfd_test (void);

